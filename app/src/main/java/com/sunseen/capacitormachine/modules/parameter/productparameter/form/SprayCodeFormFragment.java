package com.sunseen.capacitormachine.modules.parameter.productparameter.form;

import com.bin.david.form.core.SmartTable;
import com.bin.david.form.data.table.FormTableData;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.MethodUtil;
import com.sunseen.capacitormachine.databinding.FragmentSprayCodeFormBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.parameter.bean.ProcessBean;
import com.sunseen.capacitormachine.common.Form;
import com.sunseen.capacitormachine.modules.parameter.productparameter.ProcessQueryEnd;

import androidx.databinding.ViewDataBinding;

/**
 * @author zest
 */
public class SprayCodeFormFragment extends BaseFragment implements ProcessQueryEnd {
    @Override
    protected int setLayout() {
        return R.layout.fragment_spray_code_form;
    }

    private SmartTable<Form> sprayCodeTable;
    private SmartTable<Form> implosionTable;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        final FragmentSprayCodeFormBinding binding = (FragmentSprayCodeFormBinding) viewDataBinding;
        sprayCodeTable = binding.tableSprayCode;
        sprayCodeTable.getConfig().setShowTableTitle(false);
        sprayCodeTable.getConfig().setShowXSequence(false);
        sprayCodeTable.getConfig().setShowYSequence(false);
        sprayCodeTable.getConfig().getContentGridStyle()
                .setColor(getResources().getColor(R.color.table_content_grid_color));
        sprayCodeTable.getConfig().getContentStyle().
                setTextColor(getResources().getColor(R.color.table_content_text_color));
        initSprayCodeForms();
        FormTableData<Form> tableData = FormTableData.create(binding.tableSprayCode, "", 12, sprayCodeForms);
        tableData.setFormat((t) -> {
            if (t != null) {
                return t.getContent();
            } else {
                return "";
            }
        });
        sprayCodeTable.setTableData(tableData);

        implosionTable = binding.tableImplosion;
        implosionTable.getConfig().setShowXSequence(false);
        implosionTable.getConfig().setShowYSequence(false);
        implosionTable.getConfig().setShowTableTitle(false);
        implosionTable.getConfig().getContentGridStyle()
                .setColor(getResources().getColor(R.color.table_content_grid_color));
        implosionTable.getConfig().getContentStyle().
                setTextColor(getResources().getColor(R.color.table_content_text_color));
        initImplosionForms();
        FormTableData<Form> implosionData = FormTableData.create(binding.tableImplosion, "", 12, implosionForms);
        implosionData.setFormat((t) -> {
            if (t != null) {
                return t.getContent();
            } else {
                return "";
            }
        });
        implosionTable.setTableData(implosionData);
    }

    private Form[][] sprayCodeForms;

    private void initSprayCodeForms() {
        sprayCodeForms = new Form[][]{
                {
                        new Form(getString(R.string.prefix_mark), 2),
                        new Form("", 10)
                },
                {
                        new Form(getString(R.string.start_number), 2),
                        new Form("", 10)
                }
        };
    }

    private Form[][] implosionForms;

    private void initImplosionForms() {
        implosionForms = new Form[][]{
                {
                        new Form(getString(R.string.implosion_current_up_limit), 2),
                        new Form("", 10)
                },
                {
                        new Form(getString(R.string.implosion_current_low_limit), 2),
                        new Form("", 10)
                },
                {
                        new Form(getString(R.string.implosion_voltage_up_limit), 2),
                        new Form("", 10)
                },
                {
                        new Form(getString(R.string.implosion_voltage_low_limit), 2),
                        new Form("", 10)
                },

                {
                        new Form(getString(R.string.implosion_duration), 2),
                        new Form("", 10)
                },
        };
    }

    @Override
    public void onProcessQueryEnd(ProcessBean bean) {
        if (isAdded() && getActivity() != null) {
            sprayCodeForms[0][1].setContent(MethodUtil.getNoNullString(bean.getCodingMark()));
            sprayCodeForms[1][1].setContent(MethodUtil.getNoNullString(bean.getCodingStartNumber()));
            sprayCodeTable.invalidate();

            implosionForms[0][1].setContent(MethodUtil.createStringUnit(bean.getImplosionCurrentUpperLimit(),0,"μA"));
            implosionForms[1][1].setContent(MethodUtil.createStringUnit(bean.getImplosionCurrentLowerLimit(),0,"μA"));
            implosionForms[2][1].setContent(MethodUtil.createStringUnit(bean.getImplosionVoltageUpperLimit(),0,"V"));
            implosionForms[3][1].setContent(MethodUtil.createStringUnit(bean.getImplosionVoltageLowerLimit(),0,"V"));
            implosionForms[4][1].setContent(MethodUtil.createStringUnit(bean.getImplosionTime(), 0, getString(R.string.ms)));
            implosionTable.invalidate();
        }
    }
}
