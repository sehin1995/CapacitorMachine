package com.sunseen.capacitormachine.commumication.serialport.bean;

/**
 * 当链路运动一步时，PlcService将解析该数据
 */
public class OneStepInfo {

    private boolean feedIn;
    private String feedCapacitorId;
    private boolean openCircuitResult;
    private boolean shortCircuitResult;
    private boolean unAgeResult;
    private boolean convexResult;
    private boolean temperateResult;
    private byte leakCurrentResult1;
    private byte leakCurrentResult2;
    private byte capacityResult1;
    private boolean lossAngleResult1;
    private boolean impedanceResult1;
    private byte capacityResult2;
    private boolean lossAngleResult2;
    private boolean impedanceResult2;
    private byte capacitorGrade;
    private boolean surge1;
    private boolean surge2;
    private boolean acuteMaladyCount;

    private int openCircuit;
    private int shortCircuit;
    private int unAge;
    private int temperature;
    private String leakCurrent1;
    private String leakCurrent2;
    private String capacity1;
    private String lossAngle1;
    private String impedance1;
    private String capacity2;
    private String lossAngle2;
    private String impedance2;
    private String dischargeCapacitorId;
    private boolean checkEmpty;//开路检测工位上的对射光纤检测结果

    public OneStepInfo(boolean feedIn, String feedCapacitorId, boolean openCircuitResult,
                       boolean shortCircuitResult, boolean unAgeResult, boolean convexResult,
                       boolean temperateResult, byte leakCurrentResult1, byte leakCurrentResult2,
                       byte capacityResult1, boolean lossAngleResult1, boolean impedanceResult1,
                       byte capacityResult2, boolean lossAngleResult2, boolean impedanceResult2,
                       byte capacitorGrade, boolean surge1, boolean surge2, int openCircuit, int shortCircuit, int unAge,
                       int temperature, String leakCurrent1, String leakCurrent2,
                       String capacity1, String lossAngle1, String impedance1,
                       String capacity2, String lossAngle2, String impedance2,
                       String dischargeCapacitorId, boolean checkEmpty, boolean acuteMaladyCount) {
        this.feedIn = feedIn;
        this.feedCapacitorId = feedCapacitorId;
        this.openCircuitResult = openCircuitResult;
        this.shortCircuitResult = shortCircuitResult;
        this.unAgeResult = unAgeResult;
        this.convexResult = convexResult;
        this.temperateResult = temperateResult;
        this.leakCurrentResult1 = leakCurrentResult1;
        this.leakCurrentResult2 = leakCurrentResult2;
        this.capacityResult1 = capacityResult1;
        this.lossAngleResult1 = lossAngleResult1;
        this.impedanceResult1 = impedanceResult1;
        this.capacityResult2 = capacityResult2;
        this.lossAngleResult2 = lossAngleResult2;
        this.impedanceResult2 = impedanceResult2;
        this.capacitorGrade = capacitorGrade;
        this.surge1 = surge1;
        this.surge2 = surge2;
        this.openCircuit = openCircuit;
        this.shortCircuit = shortCircuit;
        this.unAge = unAge;
        this.temperature = temperature;
        this.leakCurrent1 = leakCurrent1;
        this.leakCurrent2 = leakCurrent2;
        this.capacity1 = capacity1;
        this.lossAngle1 = lossAngle1;
        this.impedance1 = impedance1;
        this.capacity2 = capacity2;
        this.lossAngle2 = lossAngle2;
        this.impedance2 = impedance2;
        this.dischargeCapacitorId = dischargeCapacitorId;
        this.checkEmpty = checkEmpty;
        this.acuteMaladyCount = acuteMaladyCount;
    }

    public boolean isFeedIn() {
        return feedIn;
    }

    public String getFeedCapacitorId() {
        return feedCapacitorId;
    }

    public boolean isOpenCircuitResult() {
        return openCircuitResult;
    }

    public boolean isShortCircuitResult() {
        return shortCircuitResult;
    }

    public boolean isUnAgeResult() {
        return unAgeResult;
    }

    public boolean isConvexResult() {
        return convexResult;
    }

    public boolean isTemperateResult() {
        return temperateResult;
    }

    public byte getLeakCurrentResult1() {
        return leakCurrentResult1;
    }

    public byte getLeakCurrentResult2() {
        return leakCurrentResult2;
    }

    public byte getCapacityResult1() {
        return capacityResult1;
    }

    public boolean isLossAngleResult1() {
        return lossAngleResult1;
    }

    public boolean isImpedanceResult1() {
        return impedanceResult1;
    }

    public byte getCapacityResult2() {
        return capacityResult2;
    }

    public boolean isLossAngleResult2() {
        return lossAngleResult2;
    }

    public boolean isImpedanceResult2() {
        return impedanceResult2;
    }

    public byte getCapacitorGrade() {
        return capacitorGrade;
    }

    public boolean isSurge1() {
        return surge1;
    }

    public boolean isSurge2() {
        return surge2;
    }

    public int getOpenCircuit() {
        return openCircuit;
    }

    public int getShortCircuit() {
        return shortCircuit;
    }

    public int getUnAge() {
        return unAge;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getLeakCurrent1() {
        return leakCurrent1;
    }

    public String getLeakCurrent2() {
        return leakCurrent2;
    }

    public String getCapacity1() {
        return capacity1;
    }

    public String getLossAngle1() {
        return lossAngle1;
    }

    public String getImpedance1() {
        return impedance1;
    }

    public String getCapacity2() {
        return capacity2;
    }

    public String getLossAngle2() {
        return lossAngle2;
    }

    public String getImpedance2() {
        return impedance2;
    }

    public String getDischargeCapacitorId() {
        return dischargeCapacitorId;
    }

    public boolean isCheckEmpty() {
        return checkEmpty;
    }

    @Override
    public String toString() {
        return
                "feedCapacitorId='" + feedCapacitorId + '\'' +
                        "\n, openCircuitResult=" + openCircuitResult +
                        "\n, shortCircuitResult=" + shortCircuitResult +
                        "\n, unAgeResult=" + unAgeResult +
                        "\n, convexResult=" + convexResult +
                        "\n, temperateResult=" + temperateResult +
                        "\n, leakCurrentResult1=" + leakCurrentResult1 +
                        "\n, leakCurrentResult2=" + leakCurrentResult2 +
                        "\n, capacityResult1=" + capacityResult1 +
                        "\n, lossAngleResult1=" + lossAngleResult1 +
                        "\n, impedanceResult1=" + impedanceResult1 +
                        "\n, capacityResult2=" + capacityResult2 +
                        "\n, lossAngleResult2=" + lossAngleResult2 +
                        "\n, impedanceResult2=" + impedanceResult2 +
                        "\n, capacitorGrade=" + capacitorGrade +
                        "\n, openCircuit=" + openCircuit +
                        "\n, shortCircuit=" + shortCircuit +
                        "\n, unAge=" + unAge +
                        "\n, temperature=" + temperature +
                        "\n, leakCurrent1='" + leakCurrent1 + '\'' +
                        "\n, leakCurrent2='" + leakCurrent2 + '\'' +
                        "\n, capacity1='" + capacity1 + '\'' +
                        "\n, lossAngle1='" + lossAngle1 + '\'' +
                        "\n, impedance1='" + impedance1 + '\'' +
                        "\n, capacity2='" + capacity2 + '\'' +
                        "\n, lossAngle2='" + lossAngle2 + '\'' +
                        "\n, impedance2='" + impedance2 + '\'' +
                        "\n, dischargeCapacitorId='" + dischargeCapacitorId +
                        "\n, checkEmpty = " + checkEmpty;
    }

    public boolean isAcuteMaladyCount() {
        return acuteMaladyCount;
    }

    public void setAcuteMaladyCount(boolean acuteMaladyCount) {
        this.acuteMaladyCount = acuteMaladyCount;
    }
}
