package com.sunseen.capacitormachine.commumication.http;

import java.util.WeakHashMap;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface RestService {

    @GET
    @Headers({"Content-Type: text/plain",
            "Accept:application/json",
            "Content-Type:application/x-www-form-urlencoded"})
    Call<String> get(@Url String url, @QueryMap WeakHashMap<String, Object> params);

    @FormUrlEncoded
    @POST
    @Headers({"Content-Type: text/plain",
            "Accept:application/json",
            "Content-Type:application/x-www-form-urlencoded"})
    Call<String> post(@Url String url, @FieldMap WeakHashMap<String, Object> params);

    @POST
    @Headers({"Content-Type: text/plain",
            "Accept:application/json",
            "Content-Type:application/x-www-form-urlencoded"})
    Call<String> postRaw(@Url String url, @Body RequestBody body);

    @FormUrlEncoded
    @PATCH
    @Headers({"Content-Type: text/plain",
            "Accept:application/json",
            "Content-Type:application/x-www-form-urlencoded"})
    Call<String> patch(@Url String url, @FieldMap WeakHashMap<String, Object> params);

    @FormUrlEncoded
    @PUT
    @Headers({"Content-Type: text/plain",
            "Accept:application/json",
            "Content-Type:application/x-www-form-urlencoded"})
    Call<String> put(@Url String url, @FieldMap WeakHashMap<String, Object> params);

    @PUT
    @Headers({"Content-Type: text/plain",
            "Accept:application/json",
            "Content-Type:application/x-www-form-urlencoded"})
    Call<String> putRaw(@Url String url, @Body RequestBody body);

    @DELETE
    Call<String> delete(@Url String url, @QueryMap WeakHashMap<String, Object> params);

    @Streaming
    @GET
    Call<ResponseBody> download(@Url String url, @QueryMap WeakHashMap<String, Object> params);

    @Multipart
    @POST
//    Call<String> upload(@Url String url, @Part MultipartBody.Part file);
    Call<String> upload(@Url String url, @PartMap WeakHashMap<String, RequestBody> params);
}
