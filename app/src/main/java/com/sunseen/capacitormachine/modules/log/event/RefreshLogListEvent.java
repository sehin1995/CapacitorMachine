package com.sunseen.capacitormachine.modules.log.event;

public class RefreshLogListEvent {

    /**
     * pos == 0: 刷新今日操作日志列表
     * pos == 1: 刷新今日报警日志列表
     * pos == 2: 刷新操作日志查询列表
     * pos == 3: 刷新报警日志查询列表
     */
    private int pos;

    public RefreshLogListEvent(int pos) {
        this.pos = pos;
    }

    public int getPos() {
        return pos;
    }
}
