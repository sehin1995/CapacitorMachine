package com.sunseen.capacitormachine.modules.home.bean;

public class CapacitorAdapterBean {
    private String uid;
    private int sequence;
    private int pos;
    private int voltage;
    private int current;
    private boolean empty;
    private boolean normal;


    public CapacitorAdapterBean(String uid, int sequence, int pos, int voltage, int current, boolean empty, boolean normal) {
        this.uid = uid;
        this.sequence = sequence;
        this.pos = pos;
        this.voltage = voltage;
        this.current = current;
        this.empty = empty;
        this.normal = normal;
    }

    public String getUid() {
        return uid;
    }

    public int getVoltage() {
        return voltage;
    }

    public int getCurrent() {
        return current;
    }

    public boolean isNormal() {
        return normal;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public void setNormal(boolean normal) {
        this.normal = normal;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }
}
