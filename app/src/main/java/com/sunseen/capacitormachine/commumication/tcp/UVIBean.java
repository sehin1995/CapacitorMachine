package com.sunseen.capacitormachine.commumication.tcp;

public class UVIBean {
    private String uid;
    private int sequence;
    private int i;
    private int v;

    public UVIBean(String uid, int sequence,int i, int v) {
        this.uid = uid;
        this.i = i;
        this.v = v;
        this.sequence = sequence;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }
}
