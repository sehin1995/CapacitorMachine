package com.sunseen.capacitormachine.modules.parameter.productivetask.event;

public class ModifySameIVEvent {
    private boolean sameI;
    private int value;

    public ModifySameIVEvent(boolean sameI, int value) {
        this.sameI = sameI;
        this.value = value;
    }

    public boolean isSameI() {
        return sameI;
    }

    public void setSameI(boolean sameI) {
        this.sameI = sameI;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
