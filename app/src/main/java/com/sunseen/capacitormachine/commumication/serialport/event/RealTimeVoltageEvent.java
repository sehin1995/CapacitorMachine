package com.sunseen.capacitormachine.commumication.serialport.event;

public class RealTimeVoltageEvent {
    private int surgePowerState;
    private int surgeVoltage;
    private int surgeCurrent;
    private int testPowerState;
    private int testVoltage;
    private int testCurrent;
    private int unAgePowerState;
    private int unAgeVoltage;
    private int unAgeCurrent;

    public RealTimeVoltageEvent(int surgePowerState, int surgeVoltage, int surgeCurrent,
                                int testPowerState, int testVoltage, int testCurrent,
                                int unAgePowerState, int unAgeVoltage, int unAgeCurrent) {
        this.surgePowerState = surgePowerState;
        this.surgeVoltage = surgeVoltage;
        this.surgeCurrent = surgeCurrent;
        this.testPowerState = testPowerState;
        this.testVoltage = testVoltage;
        this.testCurrent = testCurrent;
        this.unAgePowerState = unAgePowerState;
        this.unAgeVoltage = unAgeVoltage;
        this.unAgeCurrent = unAgeCurrent;
    }

    public int getSurgeCurrent() {
        return surgeCurrent;
    }

    public void setSurgeCurrent(int surgeCurrent) {
        this.surgeCurrent = surgeCurrent;
    }

    public int getTestCurrent() {
        return testCurrent;
    }

    public void setTestCurrent(int testCurrent) {
        this.testCurrent = testCurrent;
    }

    public int getUnAgeCurrent() {
        return unAgeCurrent;
    }

    public void setUnAgeCurrent(int unAgeCurrent) {
        this.unAgeCurrent = unAgeCurrent;
    }

    public int getSurgeVoltage() {
        return surgeVoltage;
    }

    public void setSurgeVoltage(int surgeVoltage) {
        this.surgeVoltage = surgeVoltage;
    }

    public int getTestVoltage() {
        return testVoltage;
    }

    public void setTestVoltage(int testVoltage) {
        this.testVoltage = testVoltage;
    }

    public int getUnAgeVoltage() {
        return unAgeVoltage;
    }

    public void setUnAgeVoltage(int unAgeVoltage) {
        this.unAgeVoltage = unAgeVoltage;
    }

    public int getSurgePowerState() {
        return surgePowerState;
    }

    public void setSurgePowerState(int surgePowerState) {
        this.surgePowerState = surgePowerState;
    }

    public int getTestPowerState() {
        return testPowerState;
    }

    public void setTestPowerState(int testPowerState) {
        this.testPowerState = testPowerState;
    }

    public int getUnAgePowerState() {
        return unAgePowerState;
    }

    public void setUnAgePowerState(int unAgePowerState) {
        this.unAgePowerState = unAgePowerState;
    }
}
