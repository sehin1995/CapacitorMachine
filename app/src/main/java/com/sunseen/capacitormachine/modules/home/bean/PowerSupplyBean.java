package com.sunseen.capacitormachine.modules.home.bean;

public class PowerSupplyBean {
    /**
     * 电源状态一个寄存器（16位）
     * BIT1：1 程控，0 近控；
     * BIT2：1 恒流，0 恒压；
     * BIT3：过压故障；
     * BIT4：短路故障；
     * BIT5：基准源故障
     * 小于0b00000100为正常状态
     */
    private int state;

    // 电压值/10=实际电压
    private int voltage;

    //电流值/100=实际电流
    private int current;

    public PowerSupplyBean(int state, int voltage, int current) {
        this.state = state;
        this.voltage = voltage;
        this.current = current;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getVoltage() {
        return voltage;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }
}
