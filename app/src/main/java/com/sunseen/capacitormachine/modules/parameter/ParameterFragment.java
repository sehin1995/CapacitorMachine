package com.sunseen.capacitormachine.modules.parameter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.MethodUtil;
import com.sunseen.capacitormachine.common.event.BatchIdStatusUpdateEvent;
import com.sunseen.capacitormachine.databinding.FragmentParameterBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.parameter.event.ModifyBatchIdStatusEvent;
import com.sunseen.capacitormachine.modules.parameter.productivetask.ProducedTaskFragment;
import com.sunseen.capacitormachine.modules.parameter.productivetask.ProducingTaskFragment;
import com.sunseen.capacitormachine.modules.parameter.productivetask.ToBeProduceTaskFragment;
import com.sunseen.capacitormachine.modules.parameter.productivetask.FlagSwitchFragment;
import com.sunseen.capacitormachine.modules.parameter.productparameter.edit.EditFragment;
import com.sunseen.capacitormachine.modules.parameter.productivetask.event.RefreshListEvent;

import androidx.annotation.LayoutRes;
import androidx.databinding.ViewDataBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @author zest
 */
public class ParameterFragment extends BaseFragment {
    @Override
    protected @LayoutRes
    int setLayout() {
        return R.layout.fragment_parameter;
    }

    private BaseFragment[] fragments = new BaseFragment[]{
            new ProducingTaskFragment(),
            new ToBeProduceTaskFragment(),
            new ProducedTaskFragment(),
            new FlagSwitchFragment()
    };

    private Button refreshListBtn;
    private TextView titleTv;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        final FragmentParameterBinding binding = (FragmentParameterBinding) viewDataBinding;
        titleTv = binding.titleTv;
        binding.rgParameterBg.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.current_produce_rb:
                    titleTv.setText(R.string.current_produce_list);
                    showHideFragmentAction(0);
                    break;
                case R.id.to_be_produce_rb:
                    titleTv.setText(R.string.to_be_produce_list);
                    showHideFragmentAction(1);
                    break;
                case R.id.produced_rb:
                    titleTv.setText(R.string.produced_list);
                    showHideFragmentAction(2);
                    break;
                case R.id.detect_switch_rb: {
                    titleTv.setText(R.string.detect_switch);
                    showHideFragmentAction(3);
                }
                break;
                default:
                    break;
            }
        });
        binding.btnAddTask.setOnClickListener(v -> {
            getParent().start(new EditFragment());
        });
        refreshListBtn = binding.listRefreshBtn;
        binding.listRefreshBtn.setOnClickListener((View v) -> {
            EventBus.getDefault().post(new RefreshListEvent(fragmentPos));
        });
        loadMultipleRootFragment(R.id.fragment_container, 0,
                fragments[0], fragments[1], fragments[2], fragments[3]);
        EventBus.getDefault().register(this);
    }

    private int fragmentPos = 0;

    private void showHideFragmentAction(int pos) {
        showHideFragment(fragments[pos]);
        if (pos == 3) {
            refreshListBtn.setEnabled(false);
        } else {
            refreshListBtn.setEnabled(true);
        }
        fragmentPos = pos;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onModifyBatchIdStatus(ModifyBatchIdStatusEvent event) {
        chooseStatus = event.getStatus();
        showDialog(event.getBatchId(), event.getStatus());
    }

    private final String[] produceStatusStrings = new String[]{"待生产状态", "生产中状态", "已生产状态"};

    private int chooseStatus;

    private void showDialog(String batchId, int status) {
        AlertDialog dialog = new AlertDialog.Builder(_mActivity)
                .setTitle("修改批号 " + batchId + "生产状态")
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.e("test", "positive onClick: " + which);
                        if (chooseStatus >= 0) {
                            curBatchId = batchId;
                            MethodUtil.updateBatchProduceStatus(batchId, chooseStatus);
                        } else {
                            Toast.makeText(_mActivity, "没有修改状态", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).setSingleChoiceItems(produceStatusStrings, status, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.e("test", "onClick: " + which);
                        chooseStatus = which;
                    }
                })
                .create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                chooseStatus = -1;
            }
        });
        dialog.show();
    }

    private String curBatchId = "";

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBatchIdStatusUpdate(BatchIdStatusUpdateEvent event) {
        if (curBatchId.equals(event.getBatchId())) {
            if (event.isSuccess()) {
                Toast.makeText(_mActivity, "修改成功", Toast.LENGTH_SHORT).show();
            }
        }
        curBatchId = "";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }
}
