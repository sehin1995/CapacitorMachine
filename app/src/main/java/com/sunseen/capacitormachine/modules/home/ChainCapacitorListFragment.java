package com.sunseen.capacitormachine.modules.home;

import android.util.Log;

import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.GridLayoutManager;

import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.commumication.serialport.event.ChainCapacitorUpdateEvent;
import com.sunseen.capacitormachine.data.bean.CapacitorBean;
import com.sunseen.capacitormachine.data.bean.HolderBean;
import com.sunseen.capacitormachine.databinding.FragmentChainCapacitorListBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.home.adapter.ChainCapacitorItemAdapter;
import com.sunseen.capacitormachine.modules.home.bean.ChainCapacitorAdapterBean;
import com.sunseen.capacitormachine.modules.home.event.ChainHolderIntoOvenEvent;
import com.sunseen.capacitormachine.data.DataUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zest
 */
public class ChainCapacitorListFragment extends BaseFragment {

    private int holderId;
    private int pos;

    public void setData(int holderId, int pos) {
        Log.e("test", "holderId = " + holderId);
        this.holderId = holderId;
        this.pos = pos;
    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_chain_capacitor_list;
    }

    private ChainCapacitorItemAdapter adapter;
    private FragmentChainCapacitorListBinding binding;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        binding = (FragmentChainCapacitorListBinding) viewDataBinding;
        binding.tvHolderName.setText(String.format(getString(R.string.holder_format), holderId));
        binding.setOnBackClick((view) -> pop());
        binding.rvCapacitor.setLayoutManager(new GridLayoutManager(getContext(), 5));
        initList();
        adapter = new ChainCapacitorItemAdapter(R.layout.layout_chain_item_capacitor, list);
        binding.rvCapacitor.setAdapter(adapter);
        EventBus.getDefault().register(this);
    }

    private final List<ChainCapacitorAdapterBean> list = new ArrayList<>();

    private void initList() {
        list.clear();
        if (pos < DataUtil.chainHolderList.size()) {
            HolderBean holderBean = DataUtil.chainHolderList.get(pos);
            for (int i = 1; i < 21; i++) {
                CapacitorBean capacitorBean = holderBean.capacitorList.get(i);
                ChainCapacitorAdapterBean adapterBean = new ChainCapacitorAdapterBean(
                        capacitorBean.getUid(), i, capacitorBean.isEmpty(), !capacitorBean.isNormal());
                list.add(adapterBean);
            }
        } else {
            for (int i = 1; i < 21; i++) {
                ChainCapacitorAdapterBean adapterBean = new ChainCapacitorAdapterBean(
                        "", i, true);
                list.add(adapterBean);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChainCapacitorUpdate(ChainCapacitorUpdateEvent event) {
        if (pos == event.getHolderIndex()) {
            int changePos = event.getCapacitorPos() - 1;
            ChainCapacitorAdapterBean adapterBean = list.get(changePos);
            adapterBean.setUid(event.getUid());
            adapterBean.setEmpty(event.isEmpty());
            adapter.notifyItemChanged(changePos);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChainHolderOutIn(ChainHolderIntoOvenEvent event) {
        pos = pos - 1;
        if (pos < 0) {
            pos = 0;
        }
        initList();
        adapter.notifyDataSetChanged();
        binding.tvHolderName.setText(String.format(getString(R.string.holder_format), DataUtil.chainHolderList.get(pos).getHolderId()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }
}
