package com.sunseen.capacitormachine.modules.query.bean;

public class CapacitorUidBean {
    private String uid;
    private String batchId;

    public CapacitorUidBean() {
    }

    public CapacitorUidBean(String uid, String batchId) {
        this.uid = uid;
        this.batchId = batchId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }
}
