package com.sunseen.capacitormachine.commumication.http;

enum HttpMethod {
    GET,
    POST,
    POST_RAW,
    PATCH,
    PUT,
    PUT_RAW,
    DELETE,
    UPLOAD
}
