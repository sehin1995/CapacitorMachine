package com.sunseen.capacitormachine.commumication.serialport.event;

import android.util.SparseIntArray;

public class AlarmEvent {

    private final SparseIntArray intArray;

    public AlarmEvent(SparseIntArray intArray) {
        this.intArray = intArray;
    }

    public SparseIntArray getIntArray() {
        return intArray;
    }

}
