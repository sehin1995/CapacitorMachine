package com.sunseen.capacitormachine.commumication.tcp.event;

public class ViEvent {
    private int v;
    private int i;

    public ViEvent(int v, int i) {
        this.v = v;
        this.i = i;
    }

    public int getV() {
        return v;
    }

    public int getI() {
        return i;
    }
}
