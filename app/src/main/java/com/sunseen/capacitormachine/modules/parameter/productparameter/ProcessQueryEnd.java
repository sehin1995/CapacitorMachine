package com.sunseen.capacitormachine.modules.parameter.productparameter;

import com.sunseen.capacitormachine.modules.parameter.bean.ProcessBean;

public interface ProcessQueryEnd {
    void onProcessQueryEnd(ProcessBean processBean);
}
