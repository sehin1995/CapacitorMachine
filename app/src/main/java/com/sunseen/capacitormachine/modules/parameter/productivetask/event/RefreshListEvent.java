package com.sunseen.capacitormachine.modules.parameter.productivetask.event;

public class RefreshListEvent {
    private int index;

    public RefreshListEvent(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
