package com.sunseen.capacitormachine.commumication.mqtt.event;

public class SendJsonObjEvent {

    private Object obj;

    public SendJsonObjEvent(Object obj) {

        this.obj = obj;
    }

    public Object getObj() {
        return obj;
    }
}
