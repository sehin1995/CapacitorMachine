package com.sunseen.capacitormachine.commumication.serialport.event;

public class PlcReplyEvent {

    private boolean init;

    public PlcReplyEvent(boolean init) {
        this.init = init;
    }

    public boolean isInit() {
        return init;
    }
}
