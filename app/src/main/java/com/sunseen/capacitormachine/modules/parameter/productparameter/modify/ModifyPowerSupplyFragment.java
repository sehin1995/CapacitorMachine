package com.sunseen.capacitormachine.modules.parameter.productparameter.modify;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.widget.Toast;

import androidx.databinding.ViewDataBinding;

import com.bin.david.form.core.SmartTable;
import com.bin.david.form.data.column.Column;
import com.bin.david.form.data.format.selected.BaseSelectFormat;
import com.bin.david.form.data.table.FormTableData;
import com.bin.david.form.data.table.TableData;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.MethodUtil;
import com.sunseen.capacitormachine.databinding.FragmentModifyPowerSupplyBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.parameter.bean.ProcessBean;
import com.sunseen.capacitormachine.common.Form;
import com.sunseen.capacitormachine.modules.parameter.productivetask.event.ModifyParameterEvent;
import com.sunseen.capacitormachine.modules.parameter.productivetask.event.ModifySameIVEvent;
import com.sunseen.capacitormachine.modules.parameter.productparameter.ProcessQueryEnd;

import org.greenrobot.eventbus.EventBus;

/**
 * @author zest
 */
public class ModifyPowerSupplyFragment extends BaseFragment implements ProcessQueryEnd {

    private TextWatcher textWatcher1;
    private TextWatcher textWatcher2;

    private Form selectedForm;
    private int curCol = -1;
    private int curRow = -1;

    @Override
    protected int setLayout() {
        return R.layout.fragment_modify_power_supply;
    }

    private SmartTable<Form> table;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        initForm();
        final FragmentModifyPowerSupplyBinding binding = (FragmentModifyPowerSupplyBinding) viewDataBinding;
        table = binding.table;
        table.getConfig().setShowTableTitle(false);
        table.getConfig().setShowYSequence(false);
        table.getConfig().setShowXSequence(false);
        table.getConfig().getContentGridStyle()
                .setColor(getResources().getColor(R.color.table_content_grid_color));
        table.getConfig().getContentStyle()
                .setTextColor(getResources().getColor(R.color.table_content_text_color));
        table.setSelectFormat(new BaseSelectFormat());

        FormTableData<Form> tableData = FormTableData.create(table, "", 12, forms);
        tableData.setFormat((t) -> {
            if (t != null) {
                return t.getContent();
            } else {
                return "";
            }
        });

        textWatcher1 = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                if (str.contains(".")) {
                    int pointIndex = str.indexOf(".");
                    if (str.length() - pointIndex > 2) {
                        binding.editInput.setText(str.substring(0, pointIndex + 2));
                    }
                }
            }
        };

        textWatcher2 = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                if (str.contains(".")) {
                    int pointIndex = str.indexOf(".");
                    if (str.length() - pointIndex > 3) {
                        binding.editInput.setText(str.substring(0, pointIndex + 3));
                    }
                }
            }
        };


        tableData.setOnItemClickListener(new TableData.OnItemClickListener<Form>() {
            @Override
            public void onClick(Column column, String value, Form form, int col, int row) {
                if (row != 0) {
                    if (col % 8 == 0) {
                        binding.btnInput.setEnabled(false);
                    } else {
                        curCol = col;
                        curRow = row;
                        binding.btnInput.setEnabled(true);
                        selectedForm = form;
                        binding.editInput.setText("");
                        if (col % 8 == 1) {
                            binding.editInput.removeTextChangedListener(textWatcher1);
                            binding.editInput.removeTextChangedListener(textWatcher2);
                            binding.editInput.addTextChangedListener(textWatcher1);
                        } else {
                            binding.editInput.removeTextChangedListener(textWatcher1);
                            binding.editInput.removeTextChangedListener(textWatcher2);
                            binding.editInput.addTextChangedListener(textWatcher2);

                        }
                    }
                } else {
                    binding.btnInput.setEnabled(false);
                }
            }
        });
        binding.table.setTableData(tableData);
        binding.editInput.setKeyListener(new DigitsKeyListener(false, true));
        binding.btnInput.setOnClickListener((view) -> {
            switch (binding.inputTypeRg.getCheckedRadioButtonId()) {
                case R.id.edit_one_by_one_rb: {
                    if (selectedForm != null) {
                        String content = binding.editInput.getText().toString();
                        Log.e("test", "onBindView: " + content);
                        //用户未输入，或只输入了一个小数点，不做处理，提示用户输入错误
                        if (content.length() == 0 || (content.length() == 1 && ".".equals(content))) {
                            Toast.makeText(_mActivity, getString(R.string.number_input_error), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        switch (curCol) {
                            case 1: {
                                if (vInvalid(content)) {
                                    return;
                                }
                                selectedForm.setContent(content + getString(R.string.volt));
                                binding.table.invalidate();
                                switch (curRow) {
                                    case 1: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V1", value));
                                    }
                                    break;
                                    case 2: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V4", value));
                                    }
                                    break;
                                    case 3: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V7", value));
                                    }
                                    break;
                                    case 4: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V10", value));
                                    }
                                    break;
                                    case 5: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V13", value));
                                    }
                                    break;
                                    case 6: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V16", value));
                                    }
                                    break;
                                    case 7: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "chargeV", value));
                                    }
                                    break;
                                }
                            }
                            break;
                            case 3: {
                                if (iInvalid(content)) {
                                    return;
                                }
                                selectedForm.setContent(content + getString(R.string.ampere));
                                binding.table.invalidate();
                                switch (curRow) {
                                    case 1: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I1", value));
                                    }
                                    break;
                                    case 2: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I4", value));
                                    }
                                    break;
                                    case 3: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I7", value));
                                    }
                                    break;
                                    case 4: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I10", value));
                                    }
                                    break;
                                    case 5: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I13", value));
                                    }
                                    break;
                                    case 6: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I16", value));
                                    }
                                    break;
                                    case 7: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "chargeI", value));
                                    }
                                    break;
                                }
                            }
                            break;
                            case 5: {
                                if (vInvalid(content)) {
                                    return;
                                }
                                selectedForm.setContent(content + getString(R.string.volt));
                                binding.table.invalidate();
                                switch (curRow) {
                                    case 1: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V2", value));
                                    }
                                    break;
                                    case 2: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V5", value));
                                    }
                                    break;
                                    case 3: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V8", value));
                                    }
                                    break;
                                    case 4: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V11", value));
                                    }
                                    break;
                                    case 5: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V14", value));
                                    }
                                    break;
                                    case 6: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V17", value));
                                    }
                                    break;
                                    case 7: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "surgeV", value));
                                    }
                                    break;
                                }
                            }
                            break;
                            case 7: {
                                if (iInvalid(content)) {
                                    return;
                                }
                                selectedForm.setContent(content + getString(R.string.ampere));
                                binding.table.invalidate();
                                switch (curRow) {
                                    case 1: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I2", value));
                                    }
                                    break;
                                    case 2: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I5", value));
                                    }
                                    break;
                                    case 3: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I8", value));
                                    }
                                    break;
                                    case 4: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I11", value));
                                    }
                                    break;
                                    case 5: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I14", value));
                                    }
                                    break;
                                    case 6: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I17", value));
                                    }
                                    break;
                                    case 7: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "surgeI", value));
                                    }
                                    break;
                                }
                            }
                            break;
                            case 9: {
                                if (vInvalid(content)) {
                                    return;
                                }
                                selectedForm.setContent(content + getString(R.string.volt));
                                binding.table.invalidate();
                                switch (curRow) {
                                    case 1: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V3", value));
                                    }
                                    break;
                                    case 2: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V6", value));
                                    }
                                    break;
                                    case 3: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V9", value));
                                    }
                                    break;
                                    case 4: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V12", value));
                                    }
                                    break;
                                    case 5: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V15", value));
                                    }
                                    break;
                                    case 6: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "V18", value));
                                    }
                                    break;
                                    case 7: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "testV", value));
                                    }
                                    break;
                                }
                            }
                            break;
                            case 11: {
                                if (iInvalid(content)) {
                                    return;
                                }
                                selectedForm.setContent(content + getString(R.string.ampere));
                                binding.table.invalidate();
                                switch (curRow) {
                                    case 1: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I3", value));
                                    }
                                    break;
                                    case 2: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I6", value));
                                    }
                                    break;
                                    case 3: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I9", value));
                                    }
                                    break;
                                    case 4: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I12", value));
                                    }
                                    break;
                                    case 5: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I15", value));
                                    }
                                    break;
                                    case 6: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "I18", value));
                                    }
                                    break;
                                    case 7: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new ModifyParameterEvent(true, "testI", value));
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                break;
                case R.id.same_voltage_rb: {
                    String content = binding.editInput.getText().toString();
                    //用户未输入，或只输入了一个小数点，不做处理，提示用户输入错误
                    if (content.length() == 0 || (content.length() == 1 && ".".equals(content))) {
                        Toast.makeText(_mActivity, getString(R.string.number_input_error), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (vInvalid(content)) {
                        return;
                    }
                    int sameV = getV(content);
                    EventBus.getDefault().post(new ModifySameIVEvent(false, sameV));
                    for (int i = 1; i < 8; i++) {
                        for (int j = 0; j < 9; j++) {
                            if (j == 1 || j == 4 || j == 7) {
                                forms[i][j].setContent(content + getString(R.string.volt));
                            }
                        }
                    }
                    binding.table.invalidate();
                }
                break;
                case R.id.same_current_rb: {
                    String content = binding.editInput.getText().toString();
                    //用户未输入，或只输入了一个小数点，不做处理，提示用户输入错误
                    if (content.length() == 0 || (content.length() == 1 && ".".equals(content))) {
                        Toast.makeText(_mActivity, getString(R.string.number_input_error), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (iInvalid(content)) {
                        return;
                    }
                    int sameI = getI(content);
                    EventBus.getDefault().post(new ModifySameIVEvent(true, sameI));
                    for (int i = 1; i < 8; i++) {
                        for (int j = 0; j < 9; j++) {
                            if (j == 2 || j == 5 || j == 8) {
                                forms[i][j].setContent(content + getString(R.string.ampere));
                            }
                        }
                    }
                    binding.table.invalidate();
                }
                break;
            }


        });
    }

    private boolean vInvalid(String content) {
        boolean flag = Integer.valueOf(content) >= 600;
        if (flag) {
            Toast.makeText(_mActivity, getString(R.string.voltage_limit_hint), Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    private boolean iInvalid(String content) {
        boolean flag = Double.valueOf(content) >= 3.0;
        if (flag) {
            Toast.makeText(_mActivity, getString(R.string.current_limit_hint), Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    private Form[][] forms;

    private void initForm() {
        forms = new Form[][]{
                {
                        new Form(getString(R.string.power_number)), new Form(getString(R.string.voltage), 2), new Form(getString(R.string.electric)),
                        new Form(getString(R.string.power_number)), new Form(getString(R.string.voltage), 2), new Form(getString(R.string.electric)),
                        new Form(getString(R.string.power_number)), new Form(getString(R.string.voltage), 2), new Form(getString(R.string.electric))
                },
                {
                        new Form(getString(R.string.power1)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power2)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power3)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                },
                {
                        new Form(getString(R.string.power4)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power5)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power6)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                },
                {
                        new Form(getString(R.string.power7)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power8)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power9)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                },
                {
                        new Form(getString(R.string.power10)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power11)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power12)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                },
                {
                        new Form(getString(R.string.power13)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power14)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power15)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                },
                {
                        new Form(getString(R.string.power16)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power17)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power18)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                },
                {
                        new Form(getString(R.string.power19)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power20)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power21)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                }
        };
    }

    @Override
    public void onProcessQueryEnd(ProcessBean bean) {
        if (isAdded() && getActivity() != null) {
            forms[1][1].setContent(MethodUtil.createVUnit(bean.getV1()));
            forms[1][2].setContent(MethodUtil.createIUnit(bean.getI1()));
            forms[1][4].setContent(MethodUtil.createVUnit(bean.getV2()));
            forms[1][5].setContent(MethodUtil.createIUnit(bean.getI2()));
            forms[1][7].setContent(MethodUtil.createVUnit(bean.getV3()));
            forms[1][8].setContent(MethodUtil.createIUnit(bean.getI3()));

            forms[2][1].setContent(MethodUtil.createVUnit(bean.getV4()));
            forms[2][2].setContent(MethodUtil.createIUnit(bean.getI4()));
            forms[2][4].setContent(MethodUtil.createVUnit(bean.getV5()));
            forms[2][5].setContent(MethodUtil.createIUnit(bean.getI5()));
            forms[2][7].setContent(MethodUtil.createVUnit(bean.getV6()));
            forms[2][8].setContent(MethodUtil.createIUnit(bean.getI6()));

            forms[3][1].setContent(MethodUtil.createVUnit(bean.getV7()));
            forms[3][2].setContent(MethodUtil.createIUnit(bean.getI7()));
            forms[3][4].setContent(MethodUtil.createVUnit(bean.getV8()));
            forms[3][5].setContent(MethodUtil.createIUnit(bean.getI8()));
            forms[3][7].setContent(MethodUtil.createVUnit(bean.getV9()));
            forms[3][8].setContent(MethodUtil.createIUnit(bean.getI9()));

            forms[4][1].setContent(MethodUtil.createVUnit(bean.getV10()));
            forms[4][2].setContent(MethodUtil.createIUnit(bean.getI10()));
            forms[4][4].setContent(MethodUtil.createVUnit(bean.getV11()));
            forms[4][5].setContent(MethodUtil.createIUnit(bean.getI11()));
            forms[4][7].setContent(MethodUtil.createVUnit(bean.getV12()));
            forms[4][8].setContent(MethodUtil.createIUnit(bean.getI12()));

            forms[5][1].setContent(MethodUtil.createVUnit(bean.getV13()));
            forms[5][2].setContent(MethodUtil.createIUnit(bean.getI13()));
            forms[5][4].setContent(MethodUtil.createVUnit(bean.getV14()));
            forms[5][5].setContent(MethodUtil.createIUnit(bean.getI14()));
            forms[5][7].setContent(MethodUtil.createVUnit(bean.getV15()));
            forms[5][8].setContent(MethodUtil.createIUnit(bean.getI15()));

            forms[6][1].setContent(MethodUtil.createVUnit(bean.getV16()));
            forms[6][2].setContent(MethodUtil.createIUnit(bean.getI16()));
            forms[6][4].setContent(MethodUtil.createVUnit(bean.getV17()));
            forms[6][5].setContent(MethodUtil.createIUnit(bean.getI17()));
            forms[6][7].setContent(MethodUtil.createVUnit(bean.getV18()));
            forms[6][8].setContent(MethodUtil.createIUnit(bean.getI18()));

            forms[7][1].setContent(MethodUtil.createVUnit(bean.getChargeV()));
            forms[7][2].setContent(MethodUtil.createIUnit(bean.getChargeI()));
            forms[7][4].setContent(MethodUtil.createVUnit(bean.getSurgeV()));
            forms[7][5].setContent(MethodUtil.createIUnit(bean.getSurgeI()));
            forms[7][7].setContent(MethodUtil.createVUnit(bean.getTestV()));
            forms[7][8].setContent(MethodUtil.createIUnit(bean.getTestI()));
            table.invalidate();
        }
    }

    private int getV(String content) {
        try {
            return (int) (Float.valueOf(content) * 10);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private int getI(String content) {
        try {
            return (int) (Float.valueOf(content) * 100);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
