package com.sunseen.capacitormachine.commumication.serialport.event;

public class SurgeUidEvent {
    /**
     * type == 1 浪涌1的前一个uid
     * type == 2 浪涌2的前一个uid
     */
    private final int type;
    private final String uid;

    public SurgeUidEvent(int type, String uid) {
        this.type = type;
        this.uid = uid;
    }

    public int getType() {
        return type;
    }

    public String getUid() {
        return uid;
    }
}
