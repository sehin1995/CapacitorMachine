package com.sunseen.capacitormachine.modules.home.bean;

/**
 * @author zest
 */
public class PowerSupplyAdapterBean {
    private int number;
    private int voltage;
    private int current;
    private int startPos;
    private int endPos;
    private boolean normal;

    public PowerSupplyAdapterBean(int number, int startPos, int endPos) {
        this.number = number;
        this.startPos = startPos;
        this.endPos = endPos;
        this.voltage = 0;
        this.current = 0;
        this.normal = true;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getVoltage() {
        return voltage;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public boolean isNormal() {
        return normal;
    }

    public void setNormal(boolean normal) {
        this.normal = normal;
    }

    public int getStartPos() {
        return startPos;
    }

    public void setStartPos(int startPos) {
        this.startPos = startPos;
    }

    public int getEndPos() {
        return endPos;
    }

    public void setEndPos(int endPos) {
        this.endPos = endPos;
    }
}
