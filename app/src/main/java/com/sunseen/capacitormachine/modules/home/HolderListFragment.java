package com.sunseen.capacitormachine.modules.home;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.commumication.serialport.event.OvenChainUpdateEvent;
import com.sunseen.capacitormachine.commumication.serialport.event.UpdatePowerSupplyEvent;
import com.sunseen.capacitormachine.databinding.FragmentHolderListBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.home.adapter.HolderItemAdapter;
import com.sunseen.capacitormachine.modules.home.bean.HolderAdapterBean;
import com.sunseen.capacitormachine.data.DataUtil;
import com.sunseen.capacitormachine.data.bean.HolderBean;
import com.sunseen.capacitormachine.modules.home.bean.PowerSupplyBean;

import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class HolderListFragment extends BaseFragment {
    @Override
    protected int setLayout() {
        return R.layout.fragment_holder_list;
    }

    private int powerPos = 0;
    private float voltage = 0;
    private float current = 0;
    private int startPos = 0;
    private int endPos = 0;
    private int errorCount = 0;

    void setData(int powerPos, int voltage, int current, int startPos, int endPos) {
        this.powerPos = powerPos;
        this.voltage = voltage;
        this.current = current;
        this.startPos = startPos;
        this.endPos = endPos;
    }

    private TextView tvVoltageValue;
    private TextView tvCurrentValue;

    private HolderItemAdapter adapter;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        final FragmentHolderListBinding binding = (FragmentHolderListBinding) viewDataBinding;
        tvVoltageValue = binding.tvVoltageValue;
        tvCurrentValue = binding.tvElectricValue;
        binding.setOnBackClick((view) -> pop());
        binding.tvZoneName.setText(String.format(getString(R.string.power_format), powerPos));
        tvVoltageValue.setText(String.format(getString(R.string.voltage_format), voltage / 10.0f));
        tvCurrentValue.setText(String.format(getString(R.string.current_format), current / 100.0f));
        binding.tvExceptionCount.setText(String.format(getString(R.string.error_format), errorCount));

        binding.rvHolder.setLayoutManager(new GridLayoutManager(getContext(), 6,
                RecyclerView.VERTICAL, false));
        adapter = new HolderItemAdapter(R.layout.layout_item_holder, getHolderData());
        binding.rvHolder.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                HolderAdapterBean bean = (HolderAdapterBean) adapter.getData().get(position);
                if (bean.getHolderId() >= 0) {
                    HolderBean holderBean = DataUtil.holderList.get(bean.getPos());
                    CapacitorListFragment fragment = new CapacitorListFragment();
                    fragment.setData(holderBean);
                    start(fragment);
                } else {
                    Toast.makeText(getContext(), "该工位暂无夹具", Toast.LENGTH_SHORT).show();
                }
            }
        });
        EventBus.getDefault().register(this);
    }

    private List<HolderAdapterBean> getHolderData() {
        ArrayList<HolderAdapterBean> holderBeans = new ArrayList<>();
        final int holderSize = DataUtil.holderList.size();
        Log.e("test", "holderSize = " + holderSize);
        for (int i = startPos; i >= endPos; i--) {
            if (i < holderSize) {
                HolderBean holderBean = DataUtil.holderList.get(i);
                HolderAdapterBean bean = new HolderAdapterBean(holderBean.getHolderId(), i, true);
                holderBeans.add(bean);
            } else {
                HolderAdapterBean bean = new HolderAdapterBean(-1, i, true);
                holderBeans.add(bean);
            }
        }
        return holderBeans;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPowerUpdate(UpdatePowerSupplyEvent event) {
        if (powerPos < event.getSupplyBeanList().size()) {
            PowerSupplyBean powerSupplyBean = event.getSupplyBeanList().get(powerPos);
            voltage = powerSupplyBean.getVoltage();
            current = powerSupplyBean.getCurrent();
            tvVoltageValue.setText(String.format(getString(R.string.voltage_format), voltage / 10.0f));
            tvCurrentValue.setText(String.format(getString(R.string.current_format), current / 100.0f));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOvenMoveOneStep(OvenChainUpdateEvent event) {
        Log.e("PlcService","OvenChainUpdateEvent");
        adapter.setNewData(getHolderData());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }
}
