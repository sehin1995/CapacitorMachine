package com.sunseen.capacitormachine.modules.log.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.modules.log.bean.AlarmInfoBean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AlarmLogAdapter extends BaseQuickAdapter<AlarmInfoBean, BaseViewHolder> {
    private DateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");

    public AlarmLogAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, AlarmInfoBean item) {
        helper.setText(R.id.log_time_tv, dateFormat.format(new Date(item.getTime() * 1000)))
                .setText(R.id.alarm_info_tv, item.getAlarmInfo());
    }
}
