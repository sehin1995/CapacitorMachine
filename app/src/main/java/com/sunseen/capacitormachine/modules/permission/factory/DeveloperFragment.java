package com.sunseen.capacitormachine.modules.permission.factory;

import android.view.View;
import android.widget.RadioGroup;

import androidx.databinding.ViewDataBinding;

import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.databinding.FragmentDeveloperBinding;
import com.sunseen.capacitormachine.common.BaseFragment;


import me.yokeyword.fragmentation.ISupportFragment;


public class DeveloperFragment extends BaseFragment {
    @Override
    protected int setLayout() {
        return R.layout.fragment_developer;
    }

    ISupportFragment[] fragments = new ISupportFragment[]{
            new SystemInfoFragment(),
            new SystemUpdateFragment(),
            new ExtractLogFragment()
    };

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        FragmentDeveloperBinding binding = (FragmentDeveloperBinding) viewDataBinding;

        binding.toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pop();
            }
        });
        binding.rgDebugMenu.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_system_info: {
                        showHideFragment(fragments[0]);
                    }
                    break;
                    case R.id.rb_system_update: {
                        showHideFragment(fragments[1]);


                    }
                    break;
                    case R.id.rb_extract_log: {
                        showHideFragment(fragments[2]);
                    }
                    break;
                }
            }
        });

        loadMultipleRootFragment(R.id.fragment_container, 0, fragments);

    }


}
