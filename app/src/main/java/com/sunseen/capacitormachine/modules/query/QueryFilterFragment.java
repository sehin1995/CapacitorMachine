package com.sunseen.capacitormachine.modules.query;

import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.MethodUtil;
import com.sunseen.capacitormachine.commumication.http.HttpUtil;
import com.sunseen.capacitormachine.commumication.http.RestClient;
import com.sunseen.capacitormachine.databinding.FragmentQueryFilterBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.query.bean.CapacitorUidBean;

import androidx.databinding.ViewDataBinding;

import java.util.Calendar;
import java.util.List;
import java.util.WeakHashMap;

public class QueryFilterFragment extends BaseFragment {

    private static final String CapacitorId = "capacitorId";
    private static final String StartTime = "start_time";
    private static final String EndTime = "end_time";
    private static final String BatchId = "batchId";

    static final String QueryUniqueId = "uid";
    static final String QueryPageCount = "per_page";
    static final String QueryType = "types";//查询类别
    static final String QueryAll = "openCircuit,shortCircuit,unAge,surge1,surge2,convex,temperature,leakCurrent,capacitor1,capacitor2,implosion";

    public static final String QuerySurge = "surgeData1,surgeData2";
    public static final String QueryOven = "ovenCapacitorData";
    public static final String QueryFlowCard = "flowCard";
    public static final String QueryProcess = "process";

    @Override
    protected int setLayout() {
        return R.layout.fragment_query_filter;
    }

    private boolean hidden = true;

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        this.hidden = hidden;
    }

    private Calendar calendar = Calendar.getInstance();

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        final FragmentQueryFilterBinding binding = (FragmentQueryFilterBinding) viewDataBinding;
        binding.toolBar.setNavigationOnClickListener((view) -> {
            hideSoftInput();
            pop();
        });
        binding.etYear.setText(String.valueOf(calendar.get(Calendar.YEAR)));
        binding.etMonth.setText(String.valueOf(calendar.get(Calendar.MONTH) + 1));
        binding.etDay.setText(String.valueOf(calendar.get(Calendar.DATE)));
        binding.btnGotoSearch.setOnClickListener(v -> {
            WeakHashMap<String, Object> params = new WeakHashMap<>();
            if (binding.cbCapacitorId.isChecked()) {
                String capacitanceIdStr = binding.idEditText.getText().toString();
                if (capacitanceIdStr.length() > 0) {
                    params.put(CapacitorId, capacitanceIdStr);
                }
            }
            if (binding.cbBatchId.isChecked()) {
                String batchIdStr = binding.batchEditText.getText().toString();
                if (batchIdStr.length() > 0) {
                    params.put(BatchId, batchIdStr);
                }
            }
            if (binding.cbDateId.isChecked()) {
                String year = binding.etYear.getText().toString();
                String month = binding.etMonth.getText().toString();
                String day = binding.etDay.getText().toString();
                if (year.length() > 0 && month.length() > 0 && day.length() > 0) {
                    if (MethodUtil.isDateCorrect(_mActivity, year, month, day)) {
                        long startTime = MethodUtil.getStartTimeStamp(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
                        long endTime = MethodUtil.getEndTimeStamp(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
                        params.put(StartTime, String.valueOf(startTime));
                        params.put(EndTime, String.valueOf(endTime));
                    } else {
                        Toast.makeText(_mActivity, "请输入正确的年、月、日值", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    Toast.makeText(_mActivity, "请输入年、月、日 值", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            if (!params.isEmpty()) {
                queryUidList(params);
            } else {
                Toast.makeText(_mActivity, "请输入查询参数", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void queryUidList(WeakHashMap<String, Object> params) {
        RestClient
                .builder()
                .url(HttpUtil.UidList)
                .params(params)
                .success(response -> {
                    JSONObject rootObj = JSON.parseObject(response);
                    if (rootObj.getInteger("status") == 1) {
                        JSONObject jsonObject = rootObj.getJSONObject("data");
                        JSONObject listObject = jsonObject.getJSONObject("list");
                        List<CapacitorUidBean> beanList = JSON.parseArray(listObject.getString("data"), CapacitorUidBean.class);
                        if (beanList != null && beanList.size() > 0) {
                            hideSoftInput();
                            if (!hidden) {
                                UidListFragment fragment = new UidListFragment();
                                fragment.setList(beanList);
                                getParent().start(fragment);
                            }
                        } else {
                            Toast.makeText(_mActivity, "无查询结果", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(_mActivity, "查询失败", Toast.LENGTH_SHORT).show();
                    }
                })
                .error(((code, msg) -> {
                }))
                .failure(() -> {
                })
                .build()
                .post();

    }
}
