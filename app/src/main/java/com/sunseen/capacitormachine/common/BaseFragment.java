package com.sunseen.capacitormachine.common;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import me.yokeyword.fragmentation.SupportFragment;

public abstract class BaseFragment extends SupportFragment {
    protected abstract @LayoutRes
    int setLayout();

    private static Handler superHandler = new Handler();

    private Runnable recoverPopPressRun = new Runnable() {
        @Override
        public void run() {
            popPressed = false;
        }
    };

    protected abstract void onBindView(ViewDataBinding viewDataBinding);

    @SuppressWarnings("unchecked")
    public <T extends BaseFragment> T getParent() {
        return (T) getParentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final ViewDataBinding viewDataBinding = DataBindingUtil.inflate(inflater, setLayout(), container, false);
        onBindView(viewDataBinding);
        return viewDataBinding.getRoot();
    }

    protected boolean popPressed = false;

    @Override
    public void pop() {
        if (!popPressed) {
            super.pop();
            popPressed = true;
            superHandler.postDelayed(recoverPopPressRun, 2000);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        superHandler.removeCallbacks(recoverPopPressRun);
    }
}
