package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class LeakCurrentData {
    public String uid;
    public int leakCurrent;
    public int result;


    public LeakCurrentData(String uid, int leakCurrent, int result) {
        this.uid = uid;
        this.leakCurrent = leakCurrent;
        this.result = result;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getLeakCurrent() {
        return leakCurrent;
    }

    public void setLeakCurrent(int leakCurrent) {
        this.leakCurrent = leakCurrent;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
