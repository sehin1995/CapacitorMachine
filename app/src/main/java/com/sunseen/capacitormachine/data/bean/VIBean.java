package com.sunseen.capacitormachine.data.bean;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Index;
import io.objectbox.annotation.IndexType;

@Entity
public class VIBean {
    @Id
    public long id;
    @Index(type = IndexType.VALUE)
    private int sequence;
    private int v;
    private int i;

    public VIBean() {
    }

    public VIBean(int sequence, int v, int i) {
        this.sequence = sequence;
        this.v = v;
        this.i = i;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
}
