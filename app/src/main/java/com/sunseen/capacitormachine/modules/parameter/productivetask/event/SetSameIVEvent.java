package com.sunseen.capacitormachine.modules.parameter.productivetask.event;

public class SetSameIVEvent {
    private boolean sameI; //true : 设置相同的电流  false：设置相同的电压
    private int value;

    public SetSameIVEvent(boolean sameI, int value) {
        this.sameI = sameI;
        this.value = value;
    }

    public boolean isSameI() {
        return sameI;
    }

    public int getValue() {
        return value;
    }
}
