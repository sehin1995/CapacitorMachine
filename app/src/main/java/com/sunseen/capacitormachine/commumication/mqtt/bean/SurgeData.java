package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class SurgeData {
    public String uid;
    public int timeGap;
    public int baseVoltage;
    public int[] data;

    public SurgeData() {
    }

    public SurgeData(String uid, int timeGap, int baseVoltage, int[] data) {
        this.uid = uid;
        this.timeGap = timeGap;
        this.baseVoltage = baseVoltage;
        this.data = data;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }


    public int getTimeGap() {
        return timeGap;
    }

    public void setTimeGap(int timeGap) {
        this.timeGap = timeGap;
    }

    public int getBaseVoltage() {
        return baseVoltage;
    }

    public void setBaseVoltage(int baseVoltage) {
        this.baseVoltage = baseVoltage;
    }

    public int[] getData() {
        return data;
    }

    public void setData(int[] data) {
        this.data = data;
    }
}
