package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class UnAgeJsonObj {
    public String type = "unAge";
    public int time;
    public UnAgeData data;

    public UnAgeJsonObj(String type, int time, UnAgeData data) {
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public UnAgeData getData() {
        return data;
    }

    public void setData(UnAgeData data) {
        this.data = data;
    }
}
