package com.sunseen.capacitormachine.commumication.http.callback;

public interface IError {

    void onError(int code, String msg);
}
