package com.sunseen.capacitormachine.commumication.http;

public class HttpUtil {
    static final String HOST = "http://192.168.0.100";
    public static final String UidList = "api/android/v1/capacitorList";
    public static final String CapacitorInfo = "/api/android/v1/capacitorInfo";
    public static final String SurgeData = "/api/android/v1/surgeData";
    public static final String FlowCard = "/api/android/v1/flowcard";
    public static final String Process = "/api/android/v1/process";

    public static final String ivData = "/api/common/v1/iv";
    public static final String OperateLog = "api/common/v1/userOperationLog";//用户操作日志查询,get
    public static final String AlarmLog = "api/common/v1/alarm";//机器报警信息信息查询,get
    public static final String UpdateBatchIdStatus = "api/android/v1/modifyProcessAndFlowCardStatus";//更新process和flowcard状态,post
    public static final String OvenTemperature = "/api/android/v1/ovenTemperature";//烤箱内实时检测温度数据查询接口,post or get
    //public static final String batchIdList = "/api/android/v1/batchIdList";
    public static final String batchStatistics = "/api/common/v1/batchStatistics";//批次统计信息查询,get

}
