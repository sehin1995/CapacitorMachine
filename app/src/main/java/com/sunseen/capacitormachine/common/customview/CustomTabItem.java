package com.sunseen.capacitormachine.common.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunseen.capacitormachine.R;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

/**
 * @author zest
 */
public class CustomTabItem extends LinearLayout {

    private ImageView tabImg;
    private TextView tabTv;

    public CustomTabItem(Context context) {
        this(context, null);
    }

    public CustomTabItem(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomTabItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.layout_tab, this, true);
        tabImg = findViewById(R.id.img_tab);
        tabTv = findViewById(R.id.tv_tab);
    }

    public CustomTabItem(Context context, @DrawableRes int drawable, @StringRes int id) {
        this(context);
        tabImg.setImageResource(drawable);
        tabTv.setTextSize(18f);
        tabTv.setText(id);
    }
}
