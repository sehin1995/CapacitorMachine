package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class CapacityData {
    public String uid;

    public int capacity;
    public int capacityResult;
    public int lossAngle;
    public int lossAngleResult;
    public int impedance;
    public int impedanceResult;

    public CapacityData(String uid, int capacity, int capacityResult, int lossAngle, int lossAngleResult, int impedance, int impedanceResult) {
        this.uid = uid;
        this.capacity = capacity;
        this.capacityResult = capacityResult;
        this.lossAngle = lossAngle;
        this.lossAngleResult = lossAngleResult;
        this.impedance = impedance;
        this.impedanceResult = impedanceResult;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacityResult() {
        return capacityResult;
    }

    public void setCapacityResult(int capacityResult) {
        this.capacityResult = capacityResult;
    }

    public int getLossAngle() {
        return lossAngle;
    }

    public void setLossAngle(int lossAngle) {
        this.lossAngle = lossAngle;
    }

    public int getLossAngleResult() {
        return lossAngleResult;
    }

    public void setLossAngleResult(int lossAngleResult) {
        this.lossAngleResult = lossAngleResult;
    }

    public int getImpedance() {
        return impedance;
    }

    public void setImpedance(int impedance) {
        this.impedance = impedance;
    }

    public int getImpedanceResult() {
        return impedanceResult;
    }

    public void setImpedanceResult(int impedanceResult) {
        this.impedanceResult = impedanceResult;
    }
}
