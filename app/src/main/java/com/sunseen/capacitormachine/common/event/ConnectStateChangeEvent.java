package com.sunseen.capacitormachine.common.event;

public class ConnectStateChangeEvent {
    /**
     * 掉线的类型
     * 0：与红外模块1连接  1: 与红外模块2连接 2：与本地服务器连接 3：与PLC连接  4：与浪涌连接
     */
    private int type;
    private boolean disconnect;

    public ConnectStateChangeEvent(int type, boolean disconnect) {
        this.type = type;
        this.disconnect = disconnect;
    }

    public int getType() {
        return type;
    }

    public boolean isDisconnect() {
        return disconnect;
    }
}
