package com.sunseen.capacitormachine.modules.permission.factory.adapter;

import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.modules.permission.factory.FileCheckBean;


public class LogFileAdapter extends BaseQuickAdapter<FileCheckBean, BaseViewHolder> {
    public LogFileAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, FileCheckBean item) {
        helper.setText(R.id.tv_file_name, item.getFile().getName());
        CheckBox cb = helper.getView(R.id.cb_select);
        cb.setChecked(item.isCheck());
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                item.setCheck(isChecked);
            }
        });
    }
}
