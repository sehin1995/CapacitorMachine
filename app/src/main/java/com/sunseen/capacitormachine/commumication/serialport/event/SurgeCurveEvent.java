package com.sunseen.capacitormachine.commumication.serialport.event;

import android.util.SparseIntArray;

public class SurgeCurveEvent {
    /**
     * surgeType == 1 浪涌1
     * surgeType == 2 浪涌2
     */
    private int surgeType;

    /**
     * 浪涌结果 02通过，01失败，00接触不良或空位
     */
    private int surgeResult;

    private SparseIntArray surgeDatas;

    public SurgeCurveEvent(int surgeType, int surgeResult, SparseIntArray surgeDatas) {
        this.surgeType = surgeType;
        this.surgeResult = surgeResult;
        this.surgeDatas = surgeDatas;
    }

    public SparseIntArray getSurgeDatas() {
        return surgeDatas;
    }

    public int getSurgeType() {
        return surgeType;
    }

    public int getSurgeResult() {
        return surgeResult;
    }
}
