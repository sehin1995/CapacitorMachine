package com.sunseen.capacitormachine.modules.parameter.productparameter.form;

import com.bin.david.form.core.SmartTable;
import com.bin.david.form.data.table.FormTableData;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.MethodUtil;
import com.sunseen.capacitormachine.databinding.FragmentElectricLeakageFormBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.parameter.bean.ProcessBean;
import com.sunseen.capacitormachine.common.Form;
import com.sunseen.capacitormachine.modules.parameter.productparameter.ProcessQueryEnd;

import androidx.databinding.ViewDataBinding;

/**
 * @author zest
 */
public class SortingParameterFormFragment extends BaseFragment implements ProcessQueryEnd {
    @Override
    protected int setLayout() {
        return R.layout.fragment_electric_leakage_form;
    }

    private SmartTable<Form> table;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        final FragmentElectricLeakageFormBinding binding = (FragmentElectricLeakageFormBinding) viewDataBinding;

        table = binding.tableElectricLeakage;
        table.getConfig().setShowXSequence(false);
        table.getConfig().setShowYSequence(false);
        table.getConfig().setShowTableTitle(false);
        table.getConfig().getContentGridStyle()
                .setColor(getResources().getColor(R.color.table_content_grid_color));
        table.getConfig().getContentStyle()
                .setTextColor(getResources().getColor(R.color.table_content_text_color));
        initForm();
        FormTableData<Form> electricLeakageData = FormTableData
                .create(table, "", 10, forms);
        electricLeakageData.setFormat((itemForm) -> {
            if (itemForm != null) {
                return itemForm.getContent();
            } else {
                return "";
            }
        });
        table.setTableData(electricLeakageData);
    }

    private Form[][] forms;

    private void initForm() {
        forms = new Form[][]{
                {
                        new Form(getString(R.string.short_circuit_voltage), 3),
                        new Form(getString(R.string.string_4_tab), 2),
                        new Form(getString(R.string.open_circuit_voltage), 3),
                        new Form(getString(R.string.string_4_tab), 2)
                },
                {
                        new Form(getString(R.string.leak_current_up_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2),
                        new Form(getString(R.string.leak_current_middle_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2)
                },
                {
                        new Form(getString(R.string.leak_current_low_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2),
                        new Form(getString(R.string.un_age_voltage), 3),
                        new Form(getString(R.string.string_4_tab), 2),

                },
                {
                        new Form(getString(R.string.loss_angle_up_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2),
                        new Form(getString(R.string.impedance_up_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2)
                },
                {
                        new Form(getString(R.string.temperature_up_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2),
                        new Form(getString(R.string.surge_up_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2),

                },
                {
                        new Form(getString(R.string.capacitor_nominal), 3),
                        new Form(getString(R.string.string_4_tab), 2),
                        new Form(getString(R.string.string_4_tab), 3),
                        new Form(getString(R.string.string_4_tab), 2)
                },
                {
                        new Form(getString(R.string.excellent_product_up_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2),
                        new Form(getString(R.string.excellent_product_low_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2)
                },
                {
                        new Form(getString(R.string.good_product_up_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2),
                        new Form(getString(R.string.good_product_low_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2)
                },
                {
                        new Form(getString(R.string.good_product_2_up_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2),
                        new Form(getString(R.string.good_product_2_low_limit), 3),
                        new Form(getString(R.string.string_4_tab), 2)
                }
        };
    }

    @Override
    public void onProcessQueryEnd(ProcessBean bean) {
        if (isAdded() && getActivity() != null) {
            forms[0][1].setContent(MethodUtil.createVUnit(bean.getShortCircuitV()));
            forms[0][3].setContent(MethodUtil.createVUnit(bean.getOpenCircuitV()));
            forms[1][1].setContent(MethodUtil.createIUnit(bean.getLeakageUpperLimit()));
            forms[1][3].setContent(MethodUtil.createIUnit(bean.getLeakageMiddleLimit()));
            forms[2][1].setContent(MethodUtil.createIUnit(bean.getLeakageLowerLimit()));
            forms[2][3].setContent(MethodUtil.createVUnit(bean.getUnAgedVoltage()));
            forms[3][1].setContent(MethodUtil.createStringUnit(bean.getLossAngelUpperLimit(), 1, getString(R.string.percent)));
            forms[3][3].setContent(MethodUtil.createStringUnit(bean.getUpperImpedanceLimit(), 1, getString(R.string.mo)));
            forms[4][1].setContent(MethodUtil.createStringUnit(bean.getUpperTemperatureLimit(), 1, getString(R.string.temp_unit_flag)));
            forms[4][3].setContent(MethodUtil.createVUnit(bean.getSurgeUpperLimit()));
            forms[5][1].setContent(MethodUtil.createCapacityUnit(bean.getCapacitorNominal()));
            forms[6][1].setContent(MethodUtil.createCapacityUnit(bean.getPremiumLimit()));
            forms[6][3].setContent(MethodUtil.createCapacityUnit(bean.getSuperiorLowerLimit()));
            forms[7][1].setContent(MethodUtil.createCapacityUnit(bean.getGoodProductUpperLimit()));
            forms[7][3].setContent(MethodUtil.createCapacityUnit(bean.getGoodProductLowerLimit()));
            forms[8][1].setContent(MethodUtil.createCapacityUnit(bean.getGoodProduct2UpperLimit()));
            forms[8][3].setContent(MethodUtil.createCapacityUnit(bean.getGoodProduct2LowerLimit()));
            table.invalidate();
        }
    }
}
