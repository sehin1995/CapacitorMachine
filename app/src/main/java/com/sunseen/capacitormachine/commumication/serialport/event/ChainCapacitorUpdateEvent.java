package com.sunseen.capacitormachine.commumication.serialport.event;

public class ChainCapacitorUpdateEvent {
    private String uid;
    private boolean empty;
    private int holderIndex;
    private int capacitorPos;

    public ChainCapacitorUpdateEvent(String uid, boolean empty, int holderIndex, int capacitorPos) {
        this.uid = uid;
        this.empty = empty;
        this.holderIndex = holderIndex;
        this.capacitorPos = capacitorPos;
    }

    public int getHolderIndex() {
        return holderIndex;
    }

    public int getCapacitorPos() {
        return capacitorPos;
    }

    public String getUid() {
        return uid;
    }

    public boolean isEmpty() {
        return empty;
    }
}
