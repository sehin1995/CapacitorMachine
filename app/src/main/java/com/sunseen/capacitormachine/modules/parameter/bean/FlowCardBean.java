package com.sunseen.capacitormachine.modules.parameter.bean;

public class FlowCardBean {

    private String batchId;
    private String deviceNo;
    private String productCode;
    private String customerCode;
    private String date;
    private String specification;
    private String size;
    private String amount;
    private String capacityRange;
    private String positiveFoil;
    private String positiveFoilModel;
    private String positiveFoilSupplier;
    private String positiveFoilSize;
    private String negativeFoilModel;
    private String negativeFoilSupplier;
    private String negativeFoilSize;
    private String guildPinModel;
    private String guildPinSupplier;
    private String guildPinRemark;
    private String electrolyticPaper1;
    private String electrolyticPaper1Supplier;
    private String electrolyticPaper1Remark;
    private String electrolyticPaper2;
    private String electrolyticPaper2Supplier;
    private String electrolyticPaper2Remark;
    private String electrolyte;
    private String electrolyteSupplier;
    private String electrolyteRemark;
    private String cover;
    private String coverSupplier;
    private String coverRemark;
    private String aluminumShell;
    private String aluminumShellSupplier;
    private String aluminumShellRemark;
    private String casing;
    private String casingSupplier;
    private String casingRemark;
    private String time;
    private String status;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCapacityRange() {
        return capacityRange;
    }

    public void setCapacityRange(String capacityRange) {
        this.capacityRange = capacityRange;
    }

    public String getPositiveFoil() {
        return positiveFoil;
    }

    public void setPositiveFoil(String positiveFoil) {
        this.positiveFoil = positiveFoil;
    }

    public String getPositiveFoilModel() {
        return positiveFoilModel;
    }

    public void setPositiveFoilModel(String positiveFoilModel) {
        this.positiveFoilModel = positiveFoilModel;
    }

    public String getPositiveFoilSupplier() {
        return positiveFoilSupplier;
    }

    public void setPositiveFoilSupplier(String positiveFoilSupplier) {
        this.positiveFoilSupplier = positiveFoilSupplier;
    }

    public String getPositiveFoilSize() {
        return positiveFoilSize;
    }

    public void setPositiveFoilSize(String positiveFoilSize) {
        this.positiveFoilSize = positiveFoilSize;
    }

    public String getNegativeFoilModel() {
        return negativeFoilModel;
    }

    public void setNegativeFoilModel(String negativeFoilModel) {
        this.negativeFoilModel = negativeFoilModel;
    }

    public String getNegativeFoilSupplier() {
        return negativeFoilSupplier;
    }

    public void setNegativeFoilSupplier(String negativeFoilSupplier) {
        this.negativeFoilSupplier = negativeFoilSupplier;
    }

    public String getNegativeFoilSize() {
        return negativeFoilSize;
    }

    public void setNegativeFoilSize(String negativeFoilSize) {
        this.negativeFoilSize = negativeFoilSize;
    }

    public String getGuildPinModel() {
        return guildPinModel;
    }

    public void setGuildPinModel(String guildPinModel) {
        this.guildPinModel = guildPinModel;
    }

    public String getGuildPinSupplier() {
        return guildPinSupplier;
    }

    public void setGuildPinSupplier(String guildPinSupplier) {
        this.guildPinSupplier = guildPinSupplier;
    }

    public String getGuildPinRemark() {
        return guildPinRemark;
    }

    public void setGuildPinRemark(String guildPinRemark) {
        this.guildPinRemark = guildPinRemark;
    }

    public String getElectrolyticPaper1() {
        return electrolyticPaper1;
    }

    public void setElectrolyticPaper1(String electrolyticPaper1) {
        this.electrolyticPaper1 = electrolyticPaper1;
    }

    public String getElectrolyticPaper1Supplier() {
        return electrolyticPaper1Supplier;
    }

    public void setElectrolyticPaper1Supplier(String electrolyticPaper1Supplier) {
        this.electrolyticPaper1Supplier = electrolyticPaper1Supplier;
    }

    public String getElectrolyticPaper1Remark() {
        return electrolyticPaper1Remark;
    }

    public void setElectrolyticPaper1Remark(String electrolyticPaper1Remark) {
        this.electrolyticPaper1Remark = electrolyticPaper1Remark;
    }

    public String getElectrolyticPaper2() {
        return electrolyticPaper2;
    }

    public void setElectrolyticPaper2(String electrolyticPaper2) {
        this.electrolyticPaper2 = electrolyticPaper2;
    }

    public String getElectrolyticPaper2Supplier() {
        return electrolyticPaper2Supplier;
    }

    public void setElectrolyticPaper2Supplier(String electrolyticPaper2Supplier) {
        this.electrolyticPaper2Supplier = electrolyticPaper2Supplier;
    }

    public String getElectrolyticPaper2Remark() {
        return electrolyticPaper2Remark;
    }

    public void setElectrolyticPaper2Remark(String electrolyticPaper2Remark) {
        this.electrolyticPaper2Remark = electrolyticPaper2Remark;
    }

    public String getElectrolyte() {
        return electrolyte;
    }

    public void setElectrolyte(String electrolyte) {
        this.electrolyte = electrolyte;
    }

    public String getElectrolyteSupplier() {
        return electrolyteSupplier;
    }

    public void setElectrolyteSupplier(String electrolyteSupplier) {
        this.electrolyteSupplier = electrolyteSupplier;
    }

    public String getElectrolyteRemark() {
        return electrolyteRemark;
    }

    public void setElectrolyteRemark(String electrolyteRemark) {
        this.electrolyteRemark = electrolyteRemark;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getCoverSupplier() {
        return coverSupplier;
    }

    public void setCoverSupplier(String coverSupplier) {
        this.coverSupplier = coverSupplier;
    }

    public String getCoverRemark() {
        return coverRemark;
    }

    public void setCoverRemark(String coverRemark) {
        this.coverRemark = coverRemark;
    }

    public String getAluminumShell() {
        return aluminumShell;
    }

    public void setAluminumShell(String aluminumShell) {
        this.aluminumShell = aluminumShell;
    }

    public String getAluminumShellSupplier() {
        return aluminumShellSupplier;
    }

    public void setAluminumShellSupplier(String aluminumShellSupplier) {
        this.aluminumShellSupplier = aluminumShellSupplier;
    }

    public String getAluminumShellRemark() {
        return aluminumShellRemark;
    }

    public void setAluminumShellRemark(String aluminumShellRemark) {
        this.aluminumShellRemark = aluminumShellRemark;
    }

    public String getCasing() {
        return casing;
    }

    public void setCasing(String casing) {
        this.casing = casing;
    }

    public String getCasingSupplier() {
        return casingSupplier;
    }

    public void setCasingSupplier(String casingSupplier) {
        this.casingSupplier = casingSupplier;
    }

    public String getCasingRemark() {
        return casingRemark;
    }

    public void setCasingRemark(String casingRemark) {
        this.casingRemark = casingRemark;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
