package com.sunseen.capacitormachine.modules.log;

import android.view.KeyEvent;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.MethodUtil;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.databinding.FragmentLogMenuBinding;
import com.sunseen.capacitormachine.modules.log.event.QueryLogEvent;
import com.sunseen.capacitormachine.modules.log.event.RefreshLogListEvent;

import java.util.Calendar;

import androidx.annotation.LayoutRes;
import androidx.annotation.StringRes;
import androidx.databinding.ViewDataBinding;

import org.greenrobot.eventbus.EventBus;


public class LogMenuFragment extends BaseFragment implements View.OnKeyListener {
    @Override
    protected @LayoutRes
    int setLayout() {
        return R.layout.fragment_log_menu;
    }

    private BaseFragment[] fragments = new BaseFragment[]{
            new ToDayOperateLogListFragment(),
            new ToDayAlarmLogListFragment(),
            new QueryOperateLogListFragment(),
            new QueryAlarmLogListFragment()
    };

    private FragmentLogMenuBinding binding;

    private int curPos = 0;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        binding = (FragmentLogMenuBinding) viewDataBinding;
        setTitleDate(R.string.operate_log_date_format);
        binding.logMenuRg.setOnCheckedChangeListener((RadioGroup group, int checkedId) -> {
            switch (checkedId) {
                case R.id.today_operate_log_rb: {
                    setTitleDate(R.string.operate_log_date_format);
                    showHideFragment(fragments[0]);
                    showDateEdit(false);
                    curPos = 0;
                }
                break;
                case R.id.today_alarm_log_rb: {
                    setTitleDate(R.string.alarm_log_date_format);
                    showHideFragment(fragments[1]);
                    showDateEdit(false);
                    curPos = 1;
                }
                break;
                case R.id.operate_log_query_rb: {
                    binding.titleTv.setText(R.string.operate_log_query);
                    showHideFragment(fragments[2]);
                    showDateEdit(true);
                    curPos = 2;
                }
                break;
                case R.id.alarm_log_query_rb: {
                    binding.titleTv.setText(R.string.alarm_log_query);
                    showHideFragment(fragments[3]);
                    showDateEdit(true);
                    curPos = 3;
                }
                break;
            }

        });
        binding.etYear.setOnKeyListener(this);
        binding.etMonth.setOnKeyListener(this);
        binding.etDay.setOnKeyListener(this);
        binding.btnGotoSearch.setOnClickListener((v) -> {
            String year = binding.etYear.getText().toString();
            String month = binding.etMonth.getText().toString();
            String day = binding.etDay.getText().toString();
            if (year.length() > 0 && month.length() > 0 && day.length() > 0) {
                if (MethodUtil.isDateCorrect(_mActivity, year, month, day)) {
                    long startTime = MethodUtil.getStartTimeStamp(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
                    long endTime = MethodUtil.getEndTimeStamp(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
                    int queryType = curPos == 2 ? 0 : 1;
                    EventBus.getDefault().post(new QueryLogEvent(queryType, startTime, endTime));
                    hideSoftInput();
                }
            } else {
                Toast.makeText(_mActivity, "请输入年、月、日 值", Toast.LENGTH_SHORT).show();
            }
        });
        binding.listRefreshBtn.setOnClickListener((v) -> {
            EventBus.getDefault().post(new RefreshLogListEvent(curPos));
        });
        loadMultipleRootFragment(R.id.fragment_container, 0,
                fragments[0], fragments[1], fragments[2], fragments[3]);
    }

    private void setTitleDate(@StringRes int id) {
        Calendar calendar = Calendar.getInstance();
        binding.titleTv.setText(String.format(getString(id),
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)));
    }

    private void showDateEdit(boolean show) {
        if (show) {
            binding.etYear.setVisibility(View.VISIBLE);
            binding.tvYear.setVisibility(View.VISIBLE);
            binding.etMonth.setVisibility(View.VISIBLE);
            binding.tvMonth.setVisibility(View.VISIBLE);
            binding.etDay.setVisibility(View.VISIBLE);
            binding.tvDay.setVisibility(View.VISIBLE);
            binding.btnGotoSearch.setVisibility(View.VISIBLE);
        } else {
            binding.etYear.setVisibility(View.INVISIBLE);
            binding.tvYear.setVisibility(View.INVISIBLE);
            binding.etMonth.setVisibility(View.INVISIBLE);
            binding.tvMonth.setVisibility(View.INVISIBLE);
            binding.etDay.setVisibility(View.INVISIBLE);
            binding.tvDay.setVisibility(View.INVISIBLE);
            binding.btnGotoSearch.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
            hideSoftInput();
            return true;
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
