package com.sunseen.capacitormachine.common.event;

public class BatchIdStatusUpdateEvent {
    private String batchId;
    private int status;
    private boolean success;

    public BatchIdStatusUpdateEvent(String batchId, int status, boolean success) {
        this.batchId = batchId;
        this.status = status;
        this.success = success;
    }

    public String getBatchId() {
        return batchId;
    }

    public int getStatus() {
        return status;
    }

    public boolean isSuccess() {
        return success;
    }
}
