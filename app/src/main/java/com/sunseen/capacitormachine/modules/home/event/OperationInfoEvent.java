package com.sunseen.capacitormachine.modules.home.event;

public class OperationInfoEvent {
    private String info;

    public OperationInfoEvent(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}
