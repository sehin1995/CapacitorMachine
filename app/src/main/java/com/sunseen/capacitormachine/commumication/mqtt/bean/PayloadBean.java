package com.sunseen.capacitormachine.commumication.mqtt.bean;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class PayloadBean {
    @Id
    long id;

    private byte[] payloadBytes;

    public PayloadBean(byte[] payloadBytes) {
        this.payloadBytes = payloadBytes;
    }

    public byte[] getPayloadBytes() {
        return payloadBytes;
    }

    public void setPayloadBytes(byte[] payloadBytes) {
        this.payloadBytes = payloadBytes;
    }
}
