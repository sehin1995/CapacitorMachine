package com.sunseen.capacitormachine.modules.query.bean;

public class UniqueBean {
    private String uniqueId;
    private String batchId;
    private String cardId;

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    @Override
    public String toString() {
        return "UniqueBean{" +
                "batchId='" + batchId + '\'' +
                ", cardId='" + cardId + '\'' +
                '}';
    }
}
