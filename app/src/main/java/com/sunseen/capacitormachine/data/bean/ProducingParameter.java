package com.sunseen.capacitormachine.data.bean;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class ProducingParameter {
    @Id
    public long id;

    private String batchId;
    /**
     * 内爆电流上限 单位为uA
     */
    private int implosionUpperLimitCurrent;
    /**
     * 内爆电流下限 单位为uA
     */
    private int implosionLowerLimitCurrent;
    /**
     * 内爆电压上限 单位为V
     */
    private int implosionUpperLimitVoltage;
    /**
     * 内爆电压下限 单位为V
     */
    private int implosionLowerLimitVoltage;

    /**
     * 内爆采集间隔时间 单位为0.02ms, 实际时间为 implosionTime * 0.02 ms；
     */
    private int implosionTime;

    /**
     * 浪涌上限电压
     * */
    private int surgeUpperLimitVoltage;

    public ProducingParameter() {
    }

    public ProducingParameter(String batchId, int implosionUpperLimitCurrent,
                              int implosionLowerLimitCurrent, int implosionUpperVoltage,
                              int implosionLowerLimitVoltage, int implosionTime,
                              int surgeUpperLimitVoltage) {
        this.batchId = batchId;
        this.implosionUpperLimitCurrent = implosionUpperLimitCurrent;
        this.implosionLowerLimitCurrent = implosionLowerLimitCurrent;
        this.implosionUpperLimitVoltage = implosionUpperVoltage;
        this.implosionLowerLimitVoltage = implosionLowerLimitVoltage;
        this.implosionTime = implosionTime;
        this.surgeUpperLimitVoltage = surgeUpperLimitVoltage;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public int getImplosionUpperLimitCurrent() {
        return implosionUpperLimitCurrent;
    }

    public void setImplosionUpperLimitCurrent(int implosionUpperLimitCurrent) {
        this.implosionUpperLimitCurrent = implosionUpperLimitCurrent;
    }

    public int getImplosionLowerLimitCurrent() {
        return implosionLowerLimitCurrent;
    }

    public void setImplosionLowerLimitCurrent(int implosionLowerLimitCurrent) {
        this.implosionLowerLimitCurrent = implosionLowerLimitCurrent;
    }

    public int getImplosionUpperLimitVoltage() {
        return implosionUpperLimitVoltage;
    }

    public void setImplosionUpperLimitVoltage(int implosionUpperLimitVoltage) {
        this.implosionUpperLimitVoltage = implosionUpperLimitVoltage;
    }

    public int getImplosionLowerLimitVoltage() {
        return implosionLowerLimitVoltage;
    }

    public void setImplosionLowerLimitVoltage(int implosionLowerLimitVoltage) {
        this.implosionLowerLimitVoltage = implosionLowerLimitVoltage;
    }

    public int getImplosionTime() {
        return implosionTime;
    }

    public void setImplosionTime(int implosionTime) {
        this.implosionTime = implosionTime;
    }

    public int getSurgeUpperLimitVoltage() {
        return surgeUpperLimitVoltage;
    }

    public void setSurgeUpperLimitVoltage(int surgeUpperLimitVoltage) {
        this.surgeUpperLimitVoltage = surgeUpperLimitVoltage;
    }
}
