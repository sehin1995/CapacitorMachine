package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class UnAgeData {
    public String uid;
    public int unAgeValue;
    public int result;

    public UnAgeData(String uid, int unAgeValue, int result) {
        this.uid = uid;
        this.unAgeValue = unAgeValue;
        this.result = result;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getUnAgeValue() {
        return unAgeValue;
    }

    public void setUnAgeValue(int unAgeValue) {
        this.unAgeValue = unAgeValue;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
