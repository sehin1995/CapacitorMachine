package com.sunseen.capacitormachine.commumication.tcp.event;

/**
 * 发出此事件让红外模块暂停或继续采集数据
 */
public class ChangeDetectStateEvent {
    /**
     * start = true : 开启数据采集
     * start = false: 暂停数据采集
     */
    private boolean start;

    public ChangeDetectStateEvent(boolean start) {
        this.start = start;
    }

    public boolean isStart() {
        return start;
    }
}
