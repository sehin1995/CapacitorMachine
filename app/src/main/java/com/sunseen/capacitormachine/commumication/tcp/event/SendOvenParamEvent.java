package com.sunseen.capacitormachine.commumication.tcp.event;

public class SendOvenParamEvent {
    private int implosionTime;
    private int implosionVoltageHighLimit;
    private int implosionVoltageLowLimit;
    private int implosionCurrentHighLimit;
    private int implosionCurrentLowLimit;

    public SendOvenParamEvent(int implosionTime, int implosionCurrentHighLimit,
                              int implosionCurrentLowLimit, int implosionVoltageHighLimit,
                              int implosionVoltageLowLimit) {
        this.implosionTime = implosionTime;
        this.implosionCurrentHighLimit = implosionCurrentHighLimit;
        this.implosionCurrentLowLimit = implosionCurrentLowLimit;
        this.implosionVoltageHighLimit = implosionVoltageHighLimit;
        this.implosionVoltageLowLimit = implosionVoltageLowLimit;
    }

    public int getImplosionTime() {
        return implosionTime;
    }

    public int getImplosionVoltageLowLimit() {
        return implosionVoltageLowLimit;
    }

    public int getImplosionVoltageHighLimit() {
        return implosionVoltageHighLimit;
    }

    public int getImplosionCurrentLowLimit() {
        return implosionCurrentLowLimit;
    }

    public int getImplosionCurrentHighLimit() {
        return implosionCurrentHighLimit;
    }
}
