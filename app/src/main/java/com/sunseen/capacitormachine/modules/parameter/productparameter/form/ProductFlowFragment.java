package com.sunseen.capacitormachine.modules.parameter.productparameter.form;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bin.david.form.core.SmartTable;
import com.bin.david.form.data.table.FormTableData;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.commumication.http.HttpUtil;
import com.sunseen.capacitormachine.commumication.http.RestClient;
import com.sunseen.capacitormachine.data.DataUtil;
import com.sunseen.capacitormachine.databinding.FragmentProductFlowBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.parameter.bean.FlowCardBean;
import com.sunseen.capacitormachine.common.Form;

import androidx.databinding.ViewDataBinding;

/**
 * @author zest
 */
public class ProductFlowFragment extends BaseFragment {
    @Override
    protected int setLayout() {
        return R.layout.fragment_product_flow;
    }

    private SmartTable<Form> table;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        final FragmentProductFlowBinding binding = (FragmentProductFlowBinding) viewDataBinding;

        table = binding.tableParameter;
        table.getConfig().setShowXSequence(false);
        table.getConfig().setShowYSequence(false);
        table.getConfig().setShowTableTitle(false);
        table.getConfig().getContentGridStyle()
                .setColor(getResources().getColor(R.color.table_content_grid_color));
        table.getConfig().getContentStyle()
                .setTextColor(getResources().getColor(R.color.table_content_text_color));
        initForm();
        FormTableData<Form> tableData = FormTableData.create(table,
                "", 15, form);
        tableData.setFormat((Form form) -> {
            if (form != null) {
                return form.getContent();
            } else {
                return "";
            }
        });
        table.setTableData(tableData);
        Bundle bundle = getArguments();
        if (bundle != null) {
            String batchId = bundle.getString("batchId");
            Log.e("test", "batchId = " + batchId);
            if (batchId != null) {
                queryFlowCard(batchId);
            }
        }
    }

    private Form[][] form = null;

    private void initForm() {
        form = new Form[][]{
                {

                        new Form(getString(R.string.batch_id), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.product_code), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.customer_code), 2),
                        new Form(getString(R.string.string_6_tab), 3)
                },
                {
                        new Form(getString(R.string.fill_table_date), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.specification), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.size), 2),
                        new Form(getString(R.string.string_6_tab), 3),

                },
                {
                        new Form(getString(R.string.device_number), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.order_quantity), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.range_of_capacity), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                },
                {
                        new Form(getString(R.string.positive_foil_specific_volume), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.string_6_tab), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.string_6_tab), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                },
                {
                        new Form(getString(R.string.positive_foil_type), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.process_dimension), 2),
                        new Form(getString(R.string.string_6_tab), 3)},
                {
                        new Form(getString(R.string.negative_foil_type), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.process_dimension), 2),
                        new Form(getString(R.string.string_6_tab), 3)},
                {
                        new Form(getString(R.string.guide_pin_type), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getString(R.string.string_6_tab), 3)},
                {
                        new Form(getString(R.string.electrolytic_paper), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getString(R.string.string_6_tab), 3)},
                {
                        new Form(getString(R.string.electrolytic_paper_2), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getString(R.string.string_6_tab), 3)},
                {
                        new Form(getString(R.string.electrolyte), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getString(R.string.string_6_tab), 3)},
                {
                        new Form(getString(R.string.cover), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getString(R.string.string_6_tab), 3)},
                {
                        new Form(getString(R.string.aluminum_case), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getString(R.string.string_6_tab), 3)},
                {
                        new Form(getString(R.string.drive_pipe), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getString(R.string.string_6_tab), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getString(R.string.string_6_tab), 3)
                }
        };
    }

    private String getNoNullStr(String str) {
        return str == null ? "" : str;
    }

    private void queryFlowCard(String batchId) {
        RestClient.builder()
                .url(HttpUtil.FlowCard + "/" + batchId)
                .params("deviceNo", DataUtil.DeviceNo)
                .success((String respond) -> {
                    JSONObject rootObj = JSON.parseObject(respond);
                    if (rootObj != null) {
                        int status = rootObj.getIntValue("status");
                        if (status == 1) {
                            JSONObject dataObj = rootObj.getJSONObject("data");
                            if (dataObj != null) {
                                FlowCardBean bean = JSON.parseObject(
                                        dataObj.getJSONObject("item").toJSONString(),
                                        FlowCardBean.class);
                                if (bean != null) {
                                    updateForm(bean);
                                } else {
                                    Toast.makeText(_mActivity, "流转卡查询结果为空", Toast.LENGTH_SHORT).show();
                                    Log.e("test", "流转卡查询结果为空");
                                }
                            } else {
                                Toast.makeText(_mActivity, "流转卡查询结果为空", Toast.LENGTH_SHORT).show();
                                Log.e("test", "流转卡查询结果为空:");
                            }
                        } else {
                            String msg = rootObj.getString("message");
                            if (msg == null) {
                                msg = "未知原因";
                            }
                            Toast.makeText(_mActivity, "流转卡查询失败: " + msg, Toast.LENGTH_SHORT).show();
                            Log.e("test", "流转卡查询失败: " + msg);
                        }
                    } else {
                        Toast.makeText(_mActivity, "流转卡查询失败", Toast.LENGTH_SHORT).show();
                        Log.e("test", "流转卡查询失败,根数据为空");
                    }
                })
                .failure(() -> {
                    Toast.makeText(_mActivity, "流转卡查询结果失败", Toast.LENGTH_SHORT).show();
                    Log.e("test", "流转卡查询结果失败");
                })
                .error((code, msg) -> {
                    Toast.makeText(_mActivity, "流转卡查询结果错误", Toast.LENGTH_SHORT).show();
                    Log.e("test", "流转卡查询结果错误 " + code + " " + msg);
                })
                .build()
                .get();
    }

    private void updateForm(FlowCardBean bean) {
        if (isAdded() && getActivity() != null) {
            form[0][1].setContent(bean.getBatchId());
            form[0][3].setContent(getNoNullStr(bean.getProductCode()));
            form[0][5].setContent(getNoNullStr(bean.getCustomerCode()));
            form[1][1].setContent(getNoNullStr(bean.getDate()));
            form[1][3].setContent(getNoNullStr(bean.getSpecification()));
            form[1][5].setContent(getNoNullStr(bean.getSize()));
            form[2][1].setContent(getNoNullStr(bean.getDeviceNo()));
            form[2][3].setContent(getNoNullStr(bean.getAmount()));
            form[2][5].setContent(getNoNullStr(bean.getCapacityRange()));
            form[3][1].setContent(getNoNullStr(bean.getPositiveFoil()));
//        form[3][3].setContent(getNoNullString());
//        form[3][5].setContent(getNoNullString());
            form[4][1].setContent(getNoNullStr(bean.getPositiveFoilModel()));
            form[4][3].setContent(getNoNullStr(bean.getPositiveFoilSupplier()));
            form[4][5].setContent(getNoNullStr(bean.getPositiveFoilSize()));
            form[5][1].setContent(getNoNullStr(bean.getNegativeFoilModel()));
            form[5][3].setContent(getNoNullStr(bean.getNegativeFoilSupplier()));
            form[5][5].setContent(getNoNullStr(bean.getNegativeFoilSize()));
            form[6][1].setContent(getNoNullStr(bean.getGuildPinModel()));
            form[6][3].setContent(getNoNullStr(bean.getGuildPinSupplier()));
            form[6][5].setContent(getNoNullStr(bean.getGuildPinRemark()));
            form[7][1].setContent(getNoNullStr(bean.getElectrolyticPaper1()));
            form[7][3].setContent(getNoNullStr(bean.getElectrolyticPaper1Supplier()));
            form[7][5].setContent(getNoNullStr(bean.getElectrolyticPaper1Remark()));
            form[8][1].setContent(getNoNullStr(bean.getElectrolyticPaper2()));
            form[8][3].setContent(getNoNullStr(bean.getElectrolyticPaper2Supplier()));
            form[8][5].setContent(getNoNullStr(bean.getElectrolyticPaper2Remark()));
            form[9][1].setContent(getNoNullStr(bean.getElectrolyte()));
            form[9][3].setContent(getNoNullStr(bean.getElectrolyteSupplier()));
            form[9][5].setContent(getNoNullStr(bean.getElectrolyteRemark()));
            form[10][1].setContent(getNoNullStr(bean.getCover()));
            form[10][3].setContent(getNoNullStr(bean.getCoverSupplier()));
            form[10][5].setContent(getNoNullStr(bean.getCoverRemark()));
            form[11][1].setContent(getNoNullStr(bean.getAluminumShell()));
            form[11][3].setContent(getNoNullStr(bean.getAluminumShellSupplier()));
            form[11][5].setContent(getNoNullStr(bean.getAluminumShellRemark()));
            form[12][1].setContent(getNoNullStr(bean.getCasing()));
            form[12][3].setContent(getNoNullStr(bean.getCasingSupplier()));
            form[12][5].setContent(getNoNullStr(bean.getCasingRemark()));
            table.invalidate();
        }
    }
}
