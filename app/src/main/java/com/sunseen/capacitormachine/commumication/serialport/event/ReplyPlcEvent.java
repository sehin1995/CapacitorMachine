package com.sunseen.capacitormachine.commumication.serialport.event;

public class ReplyPlcEvent {
    private boolean proper;

    public ReplyPlcEvent(boolean proper) {
        this.proper = proper;
    }

    public boolean isProper() {
        return this.proper;
    }
}
