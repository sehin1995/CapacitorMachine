package com.sunseen.capacitormachine;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.databinding.ViewDataBinding;

import com.google.android.material.tabs.TabLayout;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.common.customview.CustomTabItem;
import com.sunseen.capacitormachine.commumication.mqtt.bean.AlarmJsonObj;
import com.sunseen.capacitormachine.commumication.mqtt.bean.OperatorData;
import com.sunseen.capacitormachine.commumication.mqtt.bean.OperatorJsonObj;
import com.sunseen.capacitormachine.commumication.mqtt.event.SendJsonObjEvent;
import com.sunseen.capacitormachine.commumication.tcp.event.NodeOffLineEvent;
import com.sunseen.capacitormachine.commumication.tcp.event.PortOffLineEvent;
import com.sunseen.capacitormachine.databinding.FragmentMainBinding;
import com.sunseen.capacitormachine.modules.home.ControlFragment;
import com.sunseen.capacitormachine.modules.home.OffLineFragment;
import com.sunseen.capacitormachine.modules.log.LogMenuFragment;
import com.sunseen.capacitormachine.modules.parameter.ParameterFragment;
import com.sunseen.capacitormachine.modules.parameter.productivetask.event.ProduceStateChangeEvent;
import com.sunseen.capacitormachine.modules.permission.PermissionFragment;
import com.sunseen.capacitormachine.modules.query.QueryFilterFragment;
import com.sunseen.capacitormachine.modules.realtime.RealTimeDataFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zest
 */
public class MainFragment extends BaseFragment {

    @Override
    protected @LayoutRes
    int setLayout() {
        return R.layout.fragment_main;
    }

    private final BaseFragment[] fragments = new BaseFragment[]{
            new ControlFragment(), new ParameterFragment(),
            new RealTimeDataFragment(), new QueryFilterFragment(),
            new LogMenuFragment(), new PermissionFragment(),
    };

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        EventBus.getDefault().register(this);
        FragmentMainBinding binding = (FragmentMainBinding) viewDataBinding;
        parameterTv = binding.tvCurParameter;
        holderOffLineBtn = binding.holderOffLineTipBtn;
        nodeOffLineBtn = binding.nodeOffLineTipBtn;
        nodeOffLine2Btn = binding.nodeOffLine2TipBtn;
        holderOffLineBtn.setVisibility(View.INVISIBLE);
        holderOffLineBtn.setOnClickListener((view) -> startOffLineFragment());
        nodeOffLineBtn.setOnClickListener((view) -> startOffLineFragment());
        makeMenuTab(getContext(), binding.tabMenu);
        binding.tabMenu.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                showHideFragment(fragments[tab.getPosition()]);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0: {
                    }
                    break;
                    case 1: {
                    }
                    break;
                    case 2: {
                    }
                    break;
                    case 3: {
                    }
                    break;
                    case 4: {
                    }
                    break;
                    case 5: {
                    }
                    break;
                }
            }
        });
        loadMultipleRootFragment(R.id.fl_module_container, 0,
                fragments[0], fragments[1], fragments[2], fragments[3], fragments[4], fragments[5]);
    }

    private void startOffLineFragment() {
//        if (holderOffLineBtn.getVisibility() == View.VISIBLE) {
//            holderOffLineBtn.setVisibility(View.INVISIBLE);
//        }
        OffLineFragment fragment = new OffLineFragment();
        start(fragment);
    }

    private void makeMenuTab(Context ctx, TabLayout tabLayout) {
        TabLayout.Tab tab1 = tabLayout.newTab();
        tab1.setCustomView(new CustomTabItem(ctx, R.drawable.selector_ic_home, R.string.home_page));
        tabLayout.addTab(tab1, true);

        TabLayout.Tab tab2 = tabLayout.newTab();
        tab2.setCustomView(new CustomTabItem(ctx, R.drawable.selector_ic_parameter, R.string.parameter));
        tabLayout.addTab(tab2);

        TabLayout.Tab tab3 = tabLayout.newTab();
        tab3.setCustomView(new CustomTabItem(ctx, R.drawable.selector_ic_real_time, R.string.realtime));
        tabLayout.addTab(tab3);

        TabLayout.Tab tab4 = tabLayout.newTab();
        tab4.setCustomView(new CustomTabItem(ctx, R.drawable.selector_ic_search, R.string.query));
        tabLayout.addTab(tab4);

        TabLayout.Tab tab5 = tabLayout.newTab();
        tab5.setCustomView(new CustomTabItem(ctx, R.drawable.selector_ic_log, R.string.log));
        tabLayout.addTab(tab5);

        TabLayout.Tab tab6 = tabLayout.newTab();
        tab6.setCustomView(new CustomTabItem(ctx, R.drawable.selector_ic_permission, R.string.permission));
        tabLayout.addTab(tab6);
    }

    private TextView parameterTv;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateProduceState(ProduceStateChangeEvent event) {
//        if (event.isParameterChange()) {
//            curParameter = event.getParameter();
//        }
//        if (curParameter != null) {
//            String produceStateStr = (DataUtil.getProduceState() == DataUtil.Producing) ?
//                    getString(R.string.producing)
//                    : (DataUtil.getProduceState() == DataUtil.ProducePause) ? getString(R.string.producePause)
//                    : getString(R.string.toBeProduce);
//            parameterTv.setText(String.format(getString(R.string.current_parameter_id),
//                    curParameter.getBatchId(), produceStateStr));
//        } else {
//            parameterTv.setText(getString(R.string.no_produce_play));
//        }
    }

    private Button nodeOffLineBtn;
    private Button nodeOffLine2Btn;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNodeOffLineEvent(NodeOffLineEvent event) {
        Button btn = event.getPortId() == 0 ? nodeOffLineBtn : nodeOffLine2Btn;
        if (event.isOffLine()) {
            if (btn.getVisibility() != View.VISIBLE) {
                btn.setVisibility(View.VISIBLE);
            }
        } else {
            if (btn.getVisibility() != View.INVISIBLE) {
                btn.setVisibility(View.INVISIBLE);
            }
        }
    }

    private Button holderOffLineBtn;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPortOffLine(PortOffLineEvent event) {
        if(event.isHasOff()){
            if (holderOffLineBtn.getVisibility() != View.VISIBLE) {
                holderOffLineBtn.setVisibility(View.VISIBLE);
            }
        } else {
            if (holderOffLineBtn.getVisibility() == View.VISIBLE) {
                holderOffLineBtn.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }
}
