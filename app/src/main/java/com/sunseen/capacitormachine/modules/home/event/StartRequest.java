package com.sunseen.capacitormachine.modules.home.event;

public class StartRequest {
    /**
     * start == true : 启动请求
     * start == false : 换料请求
     */
    private boolean start;

    public StartRequest(boolean start) {
        this.start = start;
    }

    public boolean isStart() {
        return start;
    }
}
