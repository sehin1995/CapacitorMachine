package com.sunseen.capacitormachine.data.bean;

/**
 * 标记该位置上的电容对应的夹具号和在夹具中的位置
 */
public class PosBean {
    private boolean inIndex;
    private int holderPos;
    private int capacitorPos;

    public PosBean(boolean inIndex, int holderPos, int capacitorPos) {
        this.inIndex = inIndex;
        this.holderPos = holderPos;
        this.capacitorPos = capacitorPos;
    }

    public boolean isInIndex() {
        return inIndex;
    }

    public int getHolderPos() {
        return holderPos;
    }

    public int getCapacitorPos() {
        return capacitorPos;
    }
}
