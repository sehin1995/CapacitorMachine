package com.sunseen.capacitormachine.commumication.serialport.event;

public class CmdSendFailedEvent {
    /**
     * 1.数据发送三次未回复，判定为发送失败
     * 2.数据发送后，PLC回复0xBB 判定为发送失败
     */
    private int failedType;

    public CmdSendFailedEvent(int failedType) {
        this.failedType = failedType;
    }

    public int getFailedType() {
        return failedType;
    }
}
