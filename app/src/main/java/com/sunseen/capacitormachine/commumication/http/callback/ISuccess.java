package com.sunseen.capacitormachine.commumication.http.callback;

public interface ISuccess {

    void onSuccess(String response);
}
