package com.sunseen.capacitormachine.modules.log.bean;

public class UserOperateBean {
    private int userId;
    private String userName;
    private String info;
    private long time;

    public UserOperateBean() {
    }

    public UserOperateBean(int userId, String userName, String info, long time) {
        this.userId = userId;
        this.userName = userName;
        this.info = info;
        this.time = time;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
