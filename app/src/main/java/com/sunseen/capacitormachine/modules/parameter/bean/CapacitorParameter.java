package com.sunseen.capacitormachine.modules.parameter.bean;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Unique;

/**
 * @author zest
 * 数据类
 * 产品流传卡
 */
@Entity
public class CapacitorParameter {
    @Id
    long id;

    /**
     * 批号
     */
    @Unique
    private String batchId;

    /**
     * 成品编号
     * */
    private String productCode;

    /**
     * 客户代码
     * */
    private String customerCode;

    /**
     * 下卡日期
     */
    private String time;
    /**
     * 规格
     */
    private String standard;
    /**
     * 尺寸
     */
    private String size;
    /**
     * 设备编号
     */
    private String deviceId;
    /**
     * 应生产数量
     */
    private String amount;

    /**
     * 容量范围
     */
    private String capacityRange;

    /**
     * 正箔比容
     */
    private String positiveFoil;
    /**
     * 正箔型号
     */
    private String positiveFoilModel;
    /**
     * 正箔供货商
     */
    private String positiveFoilSupplier;
    /**
     * 正箔工艺尺寸
     */
    private String positiveFoilSize;
    /**
     * 负箔型号
     */
    private String negativeFoilModel;
    /**
     * 负箔供货商
     */
    private String negativeFoilSupplier;
    /**
     * 负箔工艺尺寸
     */
    private String negativeFoilSize;
    /**
     * 导箔条型号
     */
    private String guidePinModel;
    /**
     * 导箔条供货商
     */
    private String guidePinSupplier;
    /**
     * 导箔条备注
     */
    private String guidePinRemark;
    /**
     * 电解纸
     */
    private String electrolyticPaper;
    /**
     * 电解纸供货商
     */
    private String electrolyticPaperSupplier;
    /**
     * 电解纸备注
     */
    private String electrolyticPaperRemark;
    /**
     * 电解纸2
     */
    private String electrolyticPaper2;
    /**
     * 电解纸2供货商
     */
    private String electrolyticPaper2Supplier;
    /**
     * 电解纸2备注
     */
    private String electrolyticPaper2Remark;
    /**
     * 电解液
     */
    private String electrolyte;
    /**
     * 电解液供货商
     */
    private String electrolyteSupplier;
    /**
     * 电解液备注
     */
    private String electrolyteRemark;
    /**
     * 盖板
     */
    private String cover;
    /**
     * 盖板供货商
     */
    private String coverSupplier;
    /**
     * 盖板备注
     */
    private String coverRemark;
    /**
     * 铝壳
     */
    private String aluminumShell;
    /**
     * 铝壳供货商
     */
    private String aluminumShellSupplier;
    /**
     * 铝壳备注
     */
    private String aluminumShellRemark;
    /**
     * 套管
     */
    private String casing;
    /**
     * 套管供货商
     */
    private String casingSupplier;
    /**
     * 套管备注
     */
    private String casingRemark;

    public CapacitorParameter() {
    }


    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCapacityRange() {
        return capacityRange;
    }

    public void setCapacityRange(String capacityRange) {
        this.capacityRange = capacityRange;
    }

    public String getPositiveFoil() {
        return positiveFoil;
    }

    public void setPositiveFoil(String positiveFoil) {
        this.positiveFoil = positiveFoil;
    }

    public String getPositiveFoilModel() {
        return positiveFoilModel;
    }

    public void setPositiveFoilModel(String positiveFoilModel) {
        this.positiveFoilModel = positiveFoilModel;
    }

    public String getPositiveFoilSupplier() {
        return positiveFoilSupplier;
    }

    public void setPositiveFoilSupplier(String positiveFoilSupplier) {
        this.positiveFoilSupplier = positiveFoilSupplier;
    }

    public String getPositiveFoilSize() {
        return positiveFoilSize;
    }

    public void setPositiveFoilSize(String positiveFoilSize) {
        this.positiveFoilSize = positiveFoilSize;
    }

    public String getNegativeFoilModel() {
        return negativeFoilModel;
    }

    public void setNegativeFoilModel(String negativeFoilModel) {
        this.negativeFoilModel = negativeFoilModel;
    }

    public String getNegativeFoilSupplier() {
        return negativeFoilSupplier;
    }

    public void setNegativeFoilSupplier(String negativeFoilSupplier) {
        this.negativeFoilSupplier = negativeFoilSupplier;
    }

    public String getNegativeFoilSize() {
        return negativeFoilSize;
    }

    public void setNegativeFoilSize(String negativeFoilSize) {
        this.negativeFoilSize = negativeFoilSize;
    }

    public String getGuidePinModel() {
        return guidePinModel;
    }

    public void setGuidePinModel(String guidePinModel) {
        this.guidePinModel = guidePinModel;
    }

    public String getGuidePinSupplier() {
        return guidePinSupplier;
    }

    public void setGuidePinSupplier(String guidePinSupplier) {
        this.guidePinSupplier = guidePinSupplier;
    }

    public String getGuidePinRemark() {
        return guidePinRemark;
    }

    public void setGuidePinRemark(String guidePinRemark) {
        this.guidePinRemark = guidePinRemark;
    }

    public String getElectrolyticPaper() {
        return electrolyticPaper;
    }

    public void setElectrolyticPaper(String electrolyticPaper) {
        this.electrolyticPaper = electrolyticPaper;
    }

    public String getElectrolyticPaperSupplier() {
        return electrolyticPaperSupplier;
    }

    public void setElectrolyticPaperSupplier(String electrolyticPaperSupplier) {
        this.electrolyticPaperSupplier = electrolyticPaperSupplier;
    }

    public String getElectrolyticPaperRemark() {
        return electrolyticPaperRemark;
    }

    public void setElectrolyticPaperRemark(String electrolyticPaperRemark) {
        this.electrolyticPaperRemark = electrolyticPaperRemark;
    }

    public String getElectrolyte() {
        return electrolyte;
    }

    public void setElectrolyte(String electrolyte) {
        this.electrolyte = electrolyte;
    }

    public String getElectrolyteSupplier() {
        return electrolyteSupplier;
    }

    public void setElectrolyteSupplier(String electrolyteSupplier) {
        this.electrolyteSupplier = electrolyteSupplier;
    }

    public String getElectrolyteRemark() {
        return electrolyteRemark;
    }

    public void setElectrolyteRemark(String electrolyteRemark) {
        this.electrolyteRemark = electrolyteRemark;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getCoverSupplier() {
        return coverSupplier;
    }

    public void setCoverSupplier(String coverSupplier) {
        this.coverSupplier = coverSupplier;
    }

    public String getCoverRemark() {
        return coverRemark;
    }

    public void setCoverRemark(String coverRemark) {
        this.coverRemark = coverRemark;
    }

    public String getAluminumShell() {
        return aluminumShell;
    }

    public void setAluminumShell(String aluminumShell) {
        this.aluminumShell = aluminumShell;
    }

    public String getAluminumShellSupplier() {
        return aluminumShellSupplier;
    }

    public void setAluminumShellSupplier(String aluminumShellSupplier) {
        this.aluminumShellSupplier = aluminumShellSupplier;
    }

    public String getAluminumShellRemark() {
        return aluminumShellRemark;
    }

    public void setAluminumShellRemark(String aluminumShellRemark) {
        this.aluminumShellRemark = aluminumShellRemark;
    }

    public String getCasing() {
        return casing;
    }

    public void setCasing(String casing) {
        this.casing = casing;
    }

    public String getCasingSupplier() {
        return casingSupplier;
    }

    public void setCasingSupplier(String casingSupplier) {
        this.casingSupplier = casingSupplier;
    }

    public String getCasingRemark() {
        return casingRemark;
    }

    public void setCasingRemark(String casingRemark) {
        this.casingRemark = casingRemark;
    }

    public String getElectrolyticPaper2() {
        return electrolyticPaper2;
    }

    public void setElectrolyticPaper2(String electrolyticPaper2) {
        this.electrolyticPaper2 = electrolyticPaper2;
    }

    public String getElectrolyticPaper2Supplier() {
        return electrolyticPaper2Supplier;
    }

    public void setElectrolyticPaper2Supplier(String electrolyticPaper2Supplier) {
        this.electrolyticPaper2Supplier = electrolyticPaper2Supplier;
    }

    public String getElectrolyticPaper2Remark() {
        return electrolyticPaper2Remark;
    }

    public void setElectrolyticPaper2Remark(String electrolyticPaper2Remark) {
        this.electrolyticPaper2Remark = electrolyticPaper2Remark;
    }

    public boolean isSetId() {
        return batchId != null && batchId.length() > 0 && !batchId.contains(" ");
    }
}
