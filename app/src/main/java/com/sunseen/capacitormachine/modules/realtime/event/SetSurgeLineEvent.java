package com.sunseen.capacitormachine.modules.realtime.event;

public class SetSurgeLineEvent {
    /**
     * 浪涌上限
     */
    private float surgeUpperLimit;

    public SetSurgeLineEvent(float surgeUpperLimit) {
        this.surgeUpperLimit = surgeUpperLimit;
    }

    public float getSurgeUpperLimit() {
        return surgeUpperLimit;
    }
}
