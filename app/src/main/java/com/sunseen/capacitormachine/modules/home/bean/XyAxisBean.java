package com.sunseen.capacitormachine.modules.home.bean;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class XyAxisBean {

    @Id
    long id;
    private int xMax;
    private int yMax;

    public XyAxisBean() {
    }

    public XyAxisBean(int xMax, int yMax) {
        this.xMax = xMax;
        this.yMax = yMax;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getXMax() {
        return xMax;
    }

    public void setXMax(int xMax) {
        this.xMax = xMax;
    }

    public int getYMax() {
        return yMax;
    }

    public void setYMax(int yMax) {
        this.yMax = yMax;
    }
}
