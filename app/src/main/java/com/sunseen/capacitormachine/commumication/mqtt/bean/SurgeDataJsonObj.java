package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class SurgeDataJsonObj {
    public String type;
    public int time;
    public SurgeData data;

    public SurgeDataJsonObj(String type, int time, SurgeData data) {
        this.type = type;
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public SurgeData getData() {
        return data;
    }

    public void setData(SurgeData data) {
        this.data = data;
    }

}
