package com.sunseen.capacitormachine.commumication.serialport.event;

public class SurgeParameterEvent {
    /**
     * 浪涌上限
     */
    private int surgeUpperLimit;

    public SurgeParameterEvent(int surgeUpperLimit) {
        this.surgeUpperLimit = surgeUpperLimit;
    }

    public int getSurgeUpperLimit() {
        return surgeUpperLimit;
    }

}
