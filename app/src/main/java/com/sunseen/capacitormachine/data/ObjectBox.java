package com.sunseen.capacitormachine.data;

import android.content.Context;
import android.os.Environment;

import com.sunseen.capacitormachine.MyObjectBox;

import io.objectbox.BoxStore;
import io.objectbox.BoxStoreBuilder;

/**
 * @author zest
 */
public final class ObjectBox {

    private static BoxStore boxStore;

    public static void init(Context context) {
        boxStore = MyObjectBox.builder()
                .maxSizeInKByte(4 * BoxStoreBuilder.DEFAULT_MAX_DB_SIZE_KBYTE)
                .androidContext(context)
                .baseDirectory(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS))
                .build();
    }

    public static BoxStore getBoxStore() {
        return boxStore;
    }

}
