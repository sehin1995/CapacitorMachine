package com.sunseen.capacitormachine.modules.parameter.productparameter.edit;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.widget.Toast;

import androidx.databinding.ViewDataBinding;

import com.bin.david.form.data.column.Column;
import com.bin.david.form.data.format.selected.BaseSelectFormat;
import com.bin.david.form.data.table.FormTableData;
import com.bin.david.form.data.table.TableData;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.common.Form;
import com.sunseen.capacitormachine.databinding.FragmentEditPowerSupplyBinding;
import com.sunseen.capacitormachine.modules.parameter.productivetask.event.SetParameterEvent;
import com.sunseen.capacitormachine.modules.parameter.productivetask.event.SetSameIVEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * @author zest
 */
public class EditPowerSupplyFragment extends BaseFragment {

    private TextWatcher textWatcher1;
    private TextWatcher textWatcher2;

    private Form selectedForm;
    private int curCol = -1;
    private int curRow = -1;

    @Override
    protected int setLayout() {
        return R.layout.fragment_edit_power_supply;
    }

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        final FragmentEditPowerSupplyBinding binding = (FragmentEditPowerSupplyBinding) viewDataBinding;
        binding.table.getConfig().setShowTableTitle(false);
        binding.table.getConfig().setShowYSequence(false);
        binding.table.getConfig().setShowXSequence(false);
        binding.table.getConfig().getContentGridStyle()
                .setColor(getResources().getColor(R.color.table_content_grid_color));
        binding.table.getConfig().getContentStyle()
                .setTextColor(getResources().getColor(R.color.table_content_text_color));
        binding.table.setSelectFormat(new BaseSelectFormat());
        Form[][] form = new Form[][]{
                {
                        new Form(getString(R.string.power_number)), new Form(getString(R.string.voltage), 4), new Form(getString(R.string.electric), 3),
                        new Form(getString(R.string.power_number)), new Form(getString(R.string.voltage), 4), new Form(getString(R.string.electric), 3),
                        new Form(getString(R.string.power_number)), new Form(getString(R.string.voltage), 4), new Form(getString(R.string.electric), 3)
                },
                {
                        new Form(getString(R.string.power1)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power2)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power3)), new Form("V", 4), new Form("A", 3)
                },
                {
                        new Form(getString(R.string.power4)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power5)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power6)), new Form("V", 4), new Form("A", 3)
                },
                {
                        new Form(getString(R.string.power7)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power8)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power9)), new Form("V", 4), new Form("A", 3)
                },
                {
                        new Form(getString(R.string.power10)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power11)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power12)), new Form("V", 4), new Form("A", 3)
                },
                {
                        new Form(getString(R.string.power13)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power14)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power15)), new Form("V", 4), new Form("A", 3)
                },
                {
                        new Form(getString(R.string.power16)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power17)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power18)), new Form("V", 4), new Form("A", 3)
                },
                {
                        new Form(getString(R.string.power19)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power20)), new Form("V", 4), new Form("A", 3),
                        new Form(getString(R.string.power21)), new Form("V", 4), new Form("A", 3)
                }
        };

        FormTableData<Form> tableData = FormTableData.create(binding.table, "", 24, form);
        tableData.setFormat((t) -> {
            if (t != null) {
                return t.getContent();
            } else {
                return "";
            }
        });

        textWatcher1 = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                if (str.contains(".")) {
                    int pointIndex = str.indexOf(".");
                    if (str.length() - pointIndex > 2) {
                        binding.editInput.setText(str.substring(0, pointIndex + 2));
                    }
                }
            }
        };

        textWatcher2 = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                if (str.contains(".")) {
                    int pointIndex = str.indexOf(".");
                    if (str.length() - pointIndex > 3) {
                        binding.editInput.setText(str.substring(0, pointIndex + 3));
                    }
                }
            }
        };

        tableData.setOnItemClickListener(new TableData.OnItemClickListener<Form>() {
            @Override
            public void onClick(Column column, String value, Form form, int col, int row) {
                if (row != 0) {
                    if (col % 8 == 0) {
                        binding.btnInput.setEnabled(false);
                    } else {
                        curCol = col;
                        curRow = row;
                        binding.btnInput.setEnabled(true);
                        selectedForm = form;
                        binding.editInput.setText("");
                        if (col % 8 == 1) {
                            binding.editInput.removeTextChangedListener(textWatcher1);
                            binding.editInput.removeTextChangedListener(textWatcher2);
                            binding.editInput.addTextChangedListener(textWatcher1);
                        } else {
                            binding.editInput.removeTextChangedListener(textWatcher1);
                            binding.editInput.removeTextChangedListener(textWatcher2);
                            binding.editInput.addTextChangedListener(textWatcher2);
                        }
                    }
                } else {
                    binding.btnInput.setEnabled(false);
                }
            }
        });
        binding.table.setTableData(tableData);
        binding.editInput.setKeyListener(new DigitsKeyListener(false, true));
        binding.btnInput.setOnClickListener((view) -> {
            switch (binding.inputTypeRg.getCheckedRadioButtonId()) {
                case R.id.edit_one_by_one_rb: {
                    if (selectedForm != null) {
                        String content = binding.editInput.getText().toString();
                        //用户未输入，或只输入了一个小数点，不做处理，提示用户输入错误
                        if (content.length() == 0 || (content.length() == 1 && ".".equals(content))) {
                            Toast.makeText(_mActivity, getString(R.string.number_input_error), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        switch (curCol) {
                            case 1: {
                                if (vInvalid(content)) {
                                    return;
                                }
                                selectedForm.setContent(content + getString(R.string.volt));
                                binding.table.invalidate();
                                switch (curRow) {
                                    case 1: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V1", value));
                                    }
                                    break;
                                    case 2: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V4", value));
                                    }
                                    break;
                                    case 3: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V7", value));
                                    }
                                    break;
                                    case 4: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V10", value));
                                    }
                                    break;
                                    case 5: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V13", value));
                                    }
                                    break;
                                    case 6: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V16", value));
                                    }
                                    break;
                                    case 7: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "chargeV", value));
                                    }
                                    break;
                                }
                            }
                            break;
                            case 5: {
                                if (iInvalid(content)) {
                                    return;
                                }
                                selectedForm.setContent(content + getString(R.string.ampere));
                                binding.table.invalidate();
                                switch (curRow) {
                                    case 1: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I1", value));
                                    }
                                    break;
                                    case 2: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I4", value));
                                    }
                                    break;
                                    case 3: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I7", value));
                                    }
                                    break;
                                    case 4: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I10", value));
                                    }
                                    break;
                                    case 5: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I13", value));
                                    }
                                    break;
                                    case 6: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I16", value));
                                    }
                                    break;
                                    case 7: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "chargeI", value));
                                    }
                                    break;
                                }
                            }
                            break;
                            case 9: {
                                if (vInvalid(content)) {
                                    return;
                                }
                                selectedForm.setContent(content + getString(R.string.volt));
                                binding.table.invalidate();
                                switch (curRow) {
                                    case 1: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V2", value));
                                    }
                                    break;
                                    case 2: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V5", value));
                                    }
                                    break;
                                    case 3: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V8", value));
                                    }
                                    break;
                                    case 4: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V11", value));
                                    }
                                    break;
                                    case 5: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V14", value));
                                    }
                                    break;
                                    case 6: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V17", value));
                                    }
                                    break;
                                    case 7: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "surgeV", value));
                                    }
                                    break;
                                }
                            }
                            break;
                            case 13: {
                                if (iInvalid(content)) {
                                    return;
                                }
                                selectedForm.setContent(content + getString(R.string.ampere));
                                binding.table.invalidate();
                                switch (curRow) {
                                    case 1: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I2", value));
                                    }
                                    break;
                                    case 2: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I5", value));
                                    }
                                    break;
                                    case 3: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I8", value));
                                    }
                                    break;
                                    case 4: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I11", value));
                                    }
                                    break;
                                    case 5: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I14", value));
                                    }
                                    break;
                                    case 6: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I17", value));
                                    }
                                    break;
                                    case 7: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "surgeI", value));
                                    }
                                    break;
                                }
                            }
                            break;
                            case 17: {
                                if (vInvalid(content)) {
                                    return;
                                }
                                selectedForm.setContent(content + getString(R.string.volt));
                                binding.table.invalidate();
                                switch (curRow) {
                                    case 1: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V3", value));
                                    }
                                    break;
                                    case 2: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V6", value));
                                    }
                                    break;
                                    case 3: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V9", value));
                                    }
                                    break;
                                    case 4: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V12", value));
                                    }
                                    break;
                                    case 5: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V15", value));
                                    }
                                    break;
                                    case 6: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "V18", value));
                                    }
                                    break;
                                    case 7: {
                                        int value = (int) (Float.valueOf(content) * 10);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "testV", value));
                                    }
                                    break;
                                }
                            }
                            break;
                            case 21: {
                                if (iInvalid(content)) {
                                    return;
                                }
                                selectedForm.setContent(content + getString(R.string.ampere));
                                binding.table.invalidate();
                                switch (curRow) {
                                    case 1: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I3", value));
                                    }
                                    break;
                                    case 2: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I6", value));
                                    }
                                    break;
                                    case 3: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I9", value));
                                    }
                                    break;
                                    case 4: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I12", value));
                                    }
                                    break;
                                    case 5: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I15", value));
                                    }
                                    break;
                                    case 6: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "I18", value));
                                    }
                                    break;
                                    case 7: {
                                        int value = (int) (Float.valueOf(content) * 100);
                                        EventBus.getDefault().post(new SetParameterEvent(true, "testI", value));
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                break;
                case R.id.same_voltage_rb: {
                    String content = binding.editInput.getText().toString();
                    //用户未输入，或只输入了一个小数点，不做处理，提示用户输入错误
                    if (content.length() == 0 || (content.length() == 1 && ".".equals(content))) {
                        Toast.makeText(_mActivity, getString(R.string.number_input_error), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (vInvalid(content)) {
                        return;
                    }
                    int sameV = getV(content);
                    EventBus.getDefault().post(new SetSameIVEvent(false, sameV));
                    for (int i = 1; i < 8; i++) {
                        for (int j = 0; j < 9; j++) {
                            if (j == 1 || j == 4 || j == 7) {
                                form[i][j].setContent(content + getString(R.string.volt));
                            }
                        }
                    }
                    binding.table.invalidate();
                }
                break;
                case R.id.same_current_rb: {
                    String content = binding.editInput.getText().toString();
                    //用户未输入，或只输入了一个小数点，不做处理，提示用户输入错误
                    if (content.length() == 0 || (content.length() == 1 && ".".equals(content))) {
                        Toast.makeText(_mActivity, getString(R.string.number_input_error), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (iInvalid(content)) {
                        return;
                    }
                    int sameI = getI(content);
                    EventBus.getDefault().post(new SetSameIVEvent(true, sameI));
                    for (int i = 1; i < 8; i++) {
                        for (int j = 0; j < 9; j++) {
                            if (j == 2 || j == 5 || j == 8) {
                                form[i][j].setContent(content + getString(R.string.ampere));
                            }
                        }
                    }
                    binding.table.invalidate();
                }
                break;
            }
        });
    }

    private boolean vInvalid(String content) {
        boolean flag = Double.valueOf(content) >= 600.0;
        if (flag) {
            Toast.makeText(_mActivity, getString(R.string.voltage_limit_hint), Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    private boolean iInvalid(String content) {
        boolean flag = Double.valueOf(content) >= 3.0;
        if (flag) {
            Toast.makeText(_mActivity, getString(R.string.current_limit_hint), Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    private int getV(String content) {
        try {
            return (int) (Float.valueOf(content) * 10);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private int getI(String content) {
        try {
            return (int) (Float.valueOf(content) * 100);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
