package com.sunseen.capacitormachine.modules.query.bean;

public class CapacitorInfoBean {
    private BaseInfo baseInfo;
    private Capacity capacitor1;
    private Capacity capacitor2;

    public BaseInfo getBaseInfo() {
        return baseInfo;
    }

    public void setBaseInfo(BaseInfo baseInfo) {
        this.baseInfo = baseInfo;
    }

    public Capacity getCapacitor1() {
        return capacitor1;
    }

    public void setCapacitor1(Capacity capacitor1) {
        this.capacitor1 = capacitor1;
    }

    public Capacity getCapacitor2() {
        return capacitor2;
    }

    public void setCapacitor2(Capacity capacitor2) {
        this.capacitor2 = capacitor2;
    }
}
