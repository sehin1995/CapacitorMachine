package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class OperatorJsonObj {
    public String type = "userOperationLog";
    public long time;
    public OperatorData data;

    public OperatorJsonObj(String type, long time, OperatorData data) {
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public OperatorData getData() {
        return data;
    }

    public void setData(OperatorData data) {
        this.data = data;
    }
}
