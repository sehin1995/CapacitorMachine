package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

import java.util.List;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class AlarmJsonObj {
    public String type = "alarm";
    public long time;
    public List<String> data;

    public AlarmJsonObj(String type, long time, List<String> data) {
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}
