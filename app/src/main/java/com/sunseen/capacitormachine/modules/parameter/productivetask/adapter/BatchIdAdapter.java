package com.sunseen.capacitormachine.modules.parameter.productivetask.adapter;

import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.modules.parameter.bean.BatchBean;

public class BatchIdAdapter extends BaseQuickAdapter<BatchBean, BaseViewHolder> {
    public BatchIdAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, BatchBean item) {
        helper.setText(R.id.batch_id_tv, item.getBatchId());
        helper.addOnClickListener(R.id.img_btn_modify);
        if(!TextUtils.isEmpty(item.getStatus())){
            helper.setText(R.id.batch_status_tv, item.getStatus());
        }
    }
}
