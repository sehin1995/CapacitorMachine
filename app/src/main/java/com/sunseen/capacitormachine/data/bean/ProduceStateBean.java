package com.sunseen.capacitormachine.data.bean;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class ProduceStateBean {
    @Id
    public long id;

    private int produceState;

    public ProduceStateBean() {

    }

    public int getProduceState() {
        return produceState;
    }
}
