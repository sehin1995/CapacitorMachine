package com.sunseen.capacitormachine.modules.permission.operation;

import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.ViewDataBinding;

import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.common.ThreadPool;
import com.sunseen.capacitormachine.common.customview.CustomDialog;
import com.sunseen.capacitormachine.commumication.serialport.event.SurgeParameterEvent;
import com.sunseen.capacitormachine.commumication.tcp.event.ChangeDetectStateEvent;
import com.sunseen.capacitormachine.commumication.tcp.event.SendOvenParamEvent;
import com.sunseen.capacitormachine.data.DataUtil;
import com.sunseen.capacitormachine.databinding.FragmentFactoryOperationBinding;
import com.sunseen.capacitormachine.modules.permission.operation.event.IndexInitEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class FactoryOperationFragment extends BaseFragment implements View.OnClickListener {
    private static Handler handler = new Handler();

    @Override
    protected int setLayout() {
        return R.layout.fragment_factory_operation;
    }

    private FragmentFactoryOperationBinding binding;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        EventBus.getDefault().register(this);
        binding = (FragmentFactoryOperationBinding) viewDataBinding;
        binding.btnPop.setOnClickListener(this);
        binding.btnDataRecoverInit.setOnClickListener(this);
        binding.curInfraredParameter.setText("当前红外检测模块参数："
                + "内爆时间: " + (DataUtil.producingParameter.getImplosionTime()) + "ms"
                + "    内爆电流上限: " + DataUtil.producingParameter.getImplosionUpperLimitCurrent() + "uA"
                + "    内爆电流下限: " + DataUtil.producingParameter.getImplosionLowerLimitCurrent() + "uA"
                + "    内爆电压上限: " + DataUtil.producingParameter.getImplosionUpperLimitVoltage() + "V"
                + "    内爆电压下限: " + DataUtil.producingParameter.getImplosionLowerLimitVoltage() + "V");
        binding.curSurgeUpperLimitTv.setText("当前浪涌检测电压上限: "
                + DataUtil.producingParameter.getSurgeUpperLimitVoltage() + "V");
        binding.sendInfraredParameterBtn.setOnClickListener(this);
        binding.enableInfraredBtn.setOnClickListener(this);
        binding.disableInfraredBtn.setOnClickListener(this);
        binding.sendSurgeUpperLimitBtn.setOnClickListener(this);
    }

    private CustomDialog customDialog;

    private void initDialog() {
        customDialog = new CustomDialog(_mActivity);
        customDialog.setTitle("数据恢复初始化确认");
        customDialog.setContent("请确认进出料链条上的夹具位置回复到了初始位置,否则会导致数据不对位！\n烤箱内的电容ID数据将全部清除，并恢复初始化状态,请谨慎操作！");
        customDialog.setCancelClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.btnDataRecoverInit.setEnabled(true);
                customDialog.dismiss();
            }
        });
        customDialog.setConfirmClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.tvDataRecovering.setVisibility(View.VISIBLE);
                binding.dataRecoverInitProgressBar.setVisibility(View.VISIBLE);
                EventBus.getDefault().post(new ChangeDetectStateEvent(false));
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ThreadPool.executor.submit(() -> {
                            DataUtil.recoverDataToInit();
                            DataUtil.saveOffset(0);
                            EventBus.getDefault().post(new IndexInitEvent());
                        });
                    }
                }, 1000);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_pop: {
                pop();
            }
            break;
            case R.id.btn_data_recover_init: {
                if (customDialog == null) {
                    initDialog();
                }
                customDialog.show();
                v.setEnabled(false);
            }
            break;
            case R.id.send_infrared_parameter_btn: {
                EventBus.getDefault().post(new SendOvenParamEvent(
                        DataUtil.producingParameter.getImplosionTime(),
                        DataUtil.producingParameter.getImplosionUpperLimitCurrent(),
                        DataUtil.producingParameter.getImplosionLowerLimitCurrent(),
                        DataUtil.producingParameter.getImplosionUpperLimitVoltage(),
                        DataUtil.producingParameter.getImplosionLowerLimitVoltage()));
            }
            break;
            case R.id.enable_infrared_btn: {
                EventBus.getDefault().post(new ChangeDetectStateEvent(true));
            }
            break;
            case R.id.disable_infrared_btn: {
                EventBus.getDefault().post(new ChangeDetectStateEvent(false));
            }
            break;
            case R.id.send_surge_upper_limit_btn: {
                SurgeParameterEvent event = new SurgeParameterEvent(
                        DataUtil.producingParameter.getSurgeUpperLimitVoltage());
                EventBus.getDefault().post(event);
            }
            break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onIndexInit(IndexInitEvent event) {
        binding.dataRecoverInitProgressBar.setVisibility(View.INVISIBLE);
        binding.tvDataRecovering.setVisibility(View.INVISIBLE);
        binding.btnDataRecoverInit.setEnabled(true);
        Toast.makeText(_mActivity, "数据恢复初始化完成", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }
}
