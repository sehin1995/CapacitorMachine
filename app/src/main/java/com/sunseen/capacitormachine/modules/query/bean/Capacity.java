package com.sunseen.capacitormachine.modules.query.bean;

public class Capacity {
    private String capacity;
    private String capacityResult;
    private String lossAngle;
    private String lossAngleResult;
    private String impedance;
    private String impedanceResult;

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getCapacityResult() {
        return capacityResult;
    }

    public void setCapacityResult(String capacityResult) {
        this.capacityResult = capacityResult;
    }

    public String getLossAngle() {
        return lossAngle;
    }

    public void setLossAngle(String lossAngle) {
        this.lossAngle = lossAngle;
    }

    public String getLossAngleResult() {
        return lossAngleResult;
    }

    public void setLossAngleResult(String lossAngleResult) {
        this.lossAngleResult = lossAngleResult;
    }

    public String getImpedance() {
        return impedance;
    }

    public void setImpedance(String impedance) {
        this.impedance = impedance;
    }

    public String getImpedanceResult() {
        return impedanceResult;
    }

    public void setImpedanceResult(String impedanceResult) {
        this.impedanceResult = impedanceResult;
    }
}
