package com.sunseen.capacitormachine.commumication.tcp.event;

public class PortOffLineEvent {
    private int portId;//0：为 进料处集群  1： 为出料处集群
    private int machineId;//机号
    private int offLineFlag1;//电路板的端口1的状态值
    private int offLineFlag2;//电路板的端口2的状态值
    private int offLineFlag3;//电路板的端口3的状态值
    private boolean hasOff = false;

    public PortOffLineEvent(int portId, int machineId, int offLineFlag1, int offLineFlag2, int offLineFlag3, boolean hasOff) {
        this.portId = portId;
        this.machineId = machineId;
        this.offLineFlag1 = offLineFlag1;
        this.offLineFlag2 = offLineFlag2;
        this.offLineFlag3 = offLineFlag3;
        this.hasOff = hasOff;
    }

    public int getPortId() {
        return portId;
    }

    public int getMachineId() {
        return machineId;
    }

    public int getOffLineFlag1() {
        return offLineFlag1;
    }

    public int getOffLineFlag2() {
        return offLineFlag2;
    }

    public int getOffLineFlag3() {
        return offLineFlag3;
    }

    public boolean isHasOff() {
        return hasOff;
    }

    public void setHasOff(boolean hasOff) {
        this.hasOff = hasOff;
    }
}
