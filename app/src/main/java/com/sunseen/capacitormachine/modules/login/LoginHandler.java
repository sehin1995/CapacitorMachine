package com.sunseen.capacitormachine.modules.login;

import com.sunseen.capacitormachine.CapacitorApp;
import com.sunseen.capacitormachine.common.SharePreferenceUtil;

/**
 * @author zest
 */
public class LoginHandler {
    private User localUser = new User("admin", "admin", User.ADMIN);


    private ICheckLogin checkLogin;


    public interface ICheckLogin {
        void check(boolean validState);
    }

    public LoginHandler(ICheckLogin checkLogin) {
        this.checkLogin = checkLogin;
    }

    public void onLoginBtnClick(User user) {
        if (checkLogin != null) {
            boolean valid = user.getName().equals(localUser.getName())
                    && user.getPassword().equals(localUser.getPassword());
            if (valid) {
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putBoolean(SharePreferenceUtil.LOGIN_STATE, true);
            }
            checkLogin.check(valid);
        }
    }

    public User getLocalUser() {
        return localUser;
    }
}
