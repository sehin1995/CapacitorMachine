package com.sunseen.capacitormachine.data.bean;

import java.util.List;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;

@Entity
public class HolderBean {

    @Id
    long id;

    private int holderId;
    private int temperature;
    private boolean inOven;

    @Backlink(to = "holder")
    public ToMany<CapacitorBean> capacitorList;

    public HolderBean() {

    }

    public HolderBean(int holderId, boolean inOven) {
        this.holderId = holderId;
        this.inOven = inOven;
    }

    public int getHolderId() {
        return holderId;
    }

    public void setHolderId(int holderId) {
        this.holderId = holderId;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public List<CapacitorBean> getCapacitorList() {
        return capacitorList;
    }


    public boolean isInOven() {
        return inOven;
    }

    public void setInOven(boolean inOven) {
        this.inOven = inOven;
    }
}
