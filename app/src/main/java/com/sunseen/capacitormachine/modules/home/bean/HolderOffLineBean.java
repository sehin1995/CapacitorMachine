package com.sunseen.capacitormachine.modules.home.bean;

public class HolderOffLineBean {

    private int holderId;
    private int holderPos;

    /**
     * 1:夹具掉线   2:夹具故障
     */
    private int holderOffLineType;

    public HolderOffLineBean(int holderId, int holderPos, int holderOffLineType) {
        this.holderId = holderId;
        this.holderPos = holderPos;
        this.holderOffLineType = holderOffLineType;
    }

    public int getHolderId() {
        return holderId;
    }

    public int getHolderPos() {
        return holderPos;
    }

    public int getHolderOffLineType() {
        return holderOffLineType;
    }
}
