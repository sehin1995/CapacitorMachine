package com.sunseen.capacitormachine.modules.log.event;

public class QueryLogEvent {
    /**
     * logType == 0 : 查询操作日志
     * logType == 1 : 查询报警日志
     **/
    private int logType;

    private long startTimeStamp;
    private long endTimeStamp;

    public QueryLogEvent(int logType, long startTimeStamp, long endTimeStamp) {
        this.logType = logType;
        this.startTimeStamp = startTimeStamp;
        this.endTimeStamp = endTimeStamp;
    }

    public int getLogType() {
        return logType;
    }

    public long getStartTimeStamp() {
        return startTimeStamp;
    }

    public long getEndTimeStamp() {
        return endTimeStamp;
    }
}
