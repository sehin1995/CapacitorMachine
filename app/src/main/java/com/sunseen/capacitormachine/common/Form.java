package com.sunseen.capacitormachine.common;

import android.graphics.Paint;

import com.bin.david.form.data.form.IForm;

/**
 * @author zest
 */
public class Form implements IForm {
    private String content;
    private int widthSize;
    private int heightSize;
    private Paint.Align align;

    public Form(String content) {
        this.content = content;
        widthSize = 1;
        heightSize = 1;
        align = Paint.Align.CENTER;
    }

    public Form(String name, int widthSize) {
        this.content = name;
        this.widthSize = widthSize;
        heightSize = 1;
        align = Paint.Align.CENTER;
    }

    public Form(String name, int widthSize, int heightSize, Paint.Align align) {
        this.content = name;
        this.widthSize = widthSize;
        this.heightSize = heightSize;
        this.align = align;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    @Override
    public int getSpanWidthSize() {
        return widthSize;
    }

    @Override
    public int getSpanHeightSize() {
        return heightSize;
    }

    @Override
    public Paint.Align getAlign() {
        return align;
    }
}
