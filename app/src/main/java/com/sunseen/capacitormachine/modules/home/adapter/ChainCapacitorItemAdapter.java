package com.sunseen.capacitormachine.modules.home.adapter;


import android.view.View;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.MethodUtil;
import com.sunseen.capacitormachine.modules.home.bean.ChainCapacitorAdapterBean;

import java.util.List;

public class ChainCapacitorItemAdapter extends BaseQuickAdapter<ChainCapacitorAdapterBean, BaseViewHolder> {


    public ChainCapacitorItemAdapter(int layoutResId, @Nullable List<ChainCapacitorAdapterBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ChainCapacitorAdapterBean item) {
        helper.setText(R.id.tv_capacitor_id, MethodUtil.getCapacitorId(item.getUid()))
                .setText(R.id.tv_capacitor_index, String.valueOf(item.getIndex()));
        if (item.isEmpty()) {
            helper.getView(R.id.img_holder).setVisibility(View.INVISIBLE);
            helper.getView(R.id.img_implosion).setVisibility(View.INVISIBLE);
        } else {
            helper.getView(R.id.img_holder).setVisibility(View.VISIBLE);
            helper.getView(R.id.img_implosion).setVisibility(
                    item.isImplosion() ? View.VISIBLE : View.INVISIBLE);
        }
    }
}
