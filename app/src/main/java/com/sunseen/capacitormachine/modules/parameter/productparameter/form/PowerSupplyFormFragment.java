package com.sunseen.capacitormachine.modules.parameter.productparameter.form;

import com.bin.david.form.core.SmartTable;
import com.bin.david.form.data.table.FormTableData;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.MethodUtil;
import com.sunseen.capacitormachine.databinding.FragmentPowerSupplyFormBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.parameter.bean.ProcessBean;
import com.sunseen.capacitormachine.common.Form;
import com.sunseen.capacitormachine.modules.parameter.productparameter.ProcessQueryEnd;

import androidx.databinding.ViewDataBinding;

/**
 * @author zest
 */
public class PowerSupplyFormFragment extends BaseFragment implements ProcessQueryEnd {
    @Override
    protected int setLayout() {
        return R.layout.fragment_power_supply_form;
    }

    private SmartTable<Form> table;
    private Form[][] forms;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        final FragmentPowerSupplyFormBinding binding = (FragmentPowerSupplyFormBinding) viewDataBinding;
        table = binding.tablePowerSupply;
        table.getConfig().setShowTableTitle(false);
        table.getConfig().setShowYSequence(false);
        table.getConfig().setShowXSequence(false);
        initForm();
        FormTableData<Form> tableData = FormTableData.create(binding.tablePowerSupply, "", 12, forms);
        tableData.setFormat((t) -> {
            if (t != null) {
                return t.getContent();
            } else {
                return "";
            }
        });
        table.getConfig().getContentGridStyle().setColor(getResources().getColor(R.color.table_content_grid_color));
        table.getConfig().getContentStyle().setTextColor(getResources().getColor(R.color.table_content_text_color));
        table.setTableData(tableData);
    }

    private void initForm() {
        forms = new Form[][]{
                {
                        new Form(getString(R.string.power_number)), new Form(getString(R.string.voltage), 2), new Form(getString(R.string.electric)),
                        new Form(getString(R.string.power_number)), new Form(getString(R.string.voltage), 2), new Form(getString(R.string.electric)),
                        new Form(getString(R.string.power_number)), new Form(getString(R.string.voltage), 2), new Form(getString(R.string.electric))
                },
                {
                        new Form(getString(R.string.power1)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power2)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power3)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                },
                {
                        new Form(getString(R.string.power4)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power5)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power6)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                },
                {
                        new Form(getString(R.string.power7)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power8)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power9)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                },
                {
                        new Form(getString(R.string.power10)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power11)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power12)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                },
                {
                        new Form(getString(R.string.power13)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power14)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power15)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                },
                {
                        new Form(getString(R.string.power16)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power17)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power18)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                },
                {
                        new Form(getString(R.string.power19)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power20)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab)),
                        new Form(getString(R.string.power21)), new Form(getString(R.string.string_4_tab), 2), new Form(getString(R.string.string_4_tab))
                }
        };
    }

    @Override
    public void onProcessQueryEnd(ProcessBean bean) {
        if(isAdded() && getActivity() != null){
            forms[1][1].setContent(MethodUtil.createVUnit(bean.getV1()));
            forms[1][2].setContent(MethodUtil.createIUnit(bean.getI1()));
            forms[1][4].setContent(MethodUtil.createVUnit(bean.getV2()));
            forms[1][5].setContent(MethodUtil.createIUnit(bean.getI2()));
            forms[1][7].setContent(MethodUtil.createVUnit(bean.getV3()));
            forms[1][8].setContent(MethodUtil.createIUnit(bean.getI3()));

            forms[2][1].setContent(MethodUtil.createVUnit(bean.getV4()));
            forms[2][2].setContent(MethodUtil.createIUnit(bean.getI4()));
            forms[2][4].setContent(MethodUtil.createVUnit(bean.getV5()));
            forms[2][5].setContent(MethodUtil.createIUnit(bean.getI5()));
            forms[2][7].setContent(MethodUtil.createVUnit(bean.getV6()));
            forms[2][8].setContent(MethodUtil.createIUnit(bean.getI6()));

            forms[3][1].setContent(MethodUtil.createVUnit(bean.getV7()));
            forms[3][2].setContent(MethodUtil.createIUnit(bean.getI7()));
            forms[3][4].setContent(MethodUtil.createVUnit(bean.getV8()));
            forms[3][5].setContent(MethodUtil.createIUnit(bean.getI8()));
            forms[3][7].setContent(MethodUtil.createVUnit(bean.getV9()));
            forms[3][8].setContent(MethodUtil.createIUnit(bean.getI9()));

            forms[4][1].setContent(MethodUtil.createVUnit(bean.getV10()));
            forms[4][2].setContent(MethodUtil.createIUnit(bean.getI10()));
            forms[4][4].setContent(MethodUtil.createVUnit(bean.getV11()));
            forms[4][5].setContent(MethodUtil.createIUnit(bean.getI11()));
            forms[4][7].setContent(MethodUtil.createVUnit(bean.getV12()));
            forms[4][8].setContent(MethodUtil.createIUnit(bean.getI12()));

            forms[5][1].setContent(MethodUtil.createVUnit(bean.getV13()));
            forms[5][2].setContent(MethodUtil.createIUnit(bean.getI13()));
            forms[5][4].setContent(MethodUtil.createVUnit(bean.getV14()));
            forms[5][5].setContent(MethodUtil.createIUnit(bean.getI14()));
            forms[5][7].setContent(MethodUtil.createVUnit(bean.getV15()));
            forms[5][8].setContent(MethodUtil.createIUnit(bean.getI15()));

            forms[6][1].setContent(MethodUtil.createVUnit(bean.getV16()));
            forms[6][2].setContent(MethodUtil.createIUnit(bean.getI16()));
            forms[6][4].setContent(MethodUtil.createVUnit(bean.getV17()));
            forms[6][5].setContent(MethodUtil.createIUnit(bean.getI17()));
            forms[6][7].setContent(MethodUtil.createVUnit(bean.getV18()));
            forms[6][8].setContent(MethodUtil.createIUnit(bean.getI18()));

            forms[7][1].setContent(MethodUtil.createVUnit(bean.getChargeV()));
            forms[7][2].setContent(MethodUtil.createIUnit(bean.getChargeI()));
            forms[7][4].setContent(MethodUtil.createVUnit(bean.getSurgeV()));
            forms[7][5].setContent(MethodUtil.createIUnit(bean.getSurgeI()));
            forms[7][7].setContent(MethodUtil.createVUnit(bean.getTestV()));
            forms[7][8].setContent(MethodUtil.createIUnit(bean.getTestI()));
            table.invalidate();
        }
    }
}
