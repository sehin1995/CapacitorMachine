package com.sunseen.capacitormachine.modules.parameter.event;

public class ModifyBatchIdStatusEvent {
    private String batchId;
    private int status;

    public ModifyBatchIdStatusEvent(String batchId, int status) {
        this.batchId = batchId;
        this.status = status;
    }

    public String getBatchId() {
        return batchId;
    }

    public int getStatus() {
        return status;
    }
}
