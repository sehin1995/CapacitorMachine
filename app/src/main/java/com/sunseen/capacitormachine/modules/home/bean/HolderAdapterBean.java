package com.sunseen.capacitormachine.modules.home.bean;

public class HolderAdapterBean {
    private int holderId;
    private int pos;//该电刷在烤箱电刷队列的位置
    private boolean normal;

    public HolderAdapterBean(int holderId, int pos, boolean normal) {
        this.holderId = holderId;
        this.pos = pos;
        this.normal = normal;
    }

    public int getHolderId() {
        return holderId;
    }

    public void setHolderId(int holderId) {
        this.holderId = holderId;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public boolean isNormal() {
        return normal;
    }

    public void setNormal(boolean normal) {
        this.normal = normal;
    }
}
