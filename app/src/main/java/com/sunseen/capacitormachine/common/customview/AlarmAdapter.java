package com.sunseen.capacitormachine.common.customview;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;


public class AlarmAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public AlarmAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.tv_alarm_item, item);
    }
}
