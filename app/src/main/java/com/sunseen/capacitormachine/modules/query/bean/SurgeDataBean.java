package com.sunseen.capacitormachine.modules.query.bean;

public class SurgeDataBean {
    private String timeGap;
    private String data;
    private String baseVoltage;

    public String getTimeGap() {
        return timeGap;
    }

    public void setTimeGap(String timeGap) {
        this.timeGap = timeGap;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getBaseVoltage() {
        return baseVoltage;
    }

    public void setBaseVoltage(String baseVoltage) {
        this.baseVoltage = baseVoltage;
    }
}
