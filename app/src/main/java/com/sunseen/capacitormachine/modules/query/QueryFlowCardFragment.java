package com.sunseen.capacitormachine.modules.query;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.ViewDataBinding;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bin.david.form.core.SmartTable;
import com.bin.david.form.data.table.FormTableData;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.commumication.http.HttpUtil;
import com.sunseen.capacitormachine.commumication.http.RestClient;
import com.sunseen.capacitormachine.databinding.FragmentQueryFlowcardBinding;
import com.sunseen.capacitormachine.common.Form;
import com.sunseen.capacitormachine.modules.query.bean.FlowCard;

public class QueryFlowCardFragment extends BaseFragment {

    private String uid = "";
    private String batchId = "";

    private SmartTable<Form> table;

    public void setData(String uid, String batchId) {
        this.uid = uid;
        this.batchId = batchId;
    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_query_flowcard;
    }

    private FragmentQueryFlowcardBinding binding;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        binding = (FragmentQueryFlowcardBinding) viewDataBinding;
        binding.toolBar.setNavigationOnClickListener((v) -> pop());
        binding.toolBar.setTitle(uid + "的流转卡");
        table = binding.tableFlowCard;
        table.getConfig().setShowTableTitle(false);
        table.getConfig().setShowXSequence(false);
        table.getConfig().setShowYSequence(false);
        table.getConfig().getContentGridStyle().setColor(Color.parseColor("#898989"));
        table.getConfig().getContentStyle().setTextColor(Color.parseColor("#222222"));
        queryFlowCard();
    }

    private void queryFlowCard() {
        Log.e("test", "batachId = " + batchId);
        if (batchId.length() > 0) {
            RestClient restClient = RestClient.builder()
                    .url(HttpUtil.FlowCard + "/" + batchId)
                    .success((String response) -> {
                        JSONObject obj = JSON.parseObject(response);
                        if (obj.getInteger("status") == 1) {
                            JSONObject dataObj = obj.getJSONObject("data");
                            if (dataObj != null) {
                                JSONObject itemJObj = dataObj.getJSONObject("item");
                                if (itemJObj != null) {
                                    FlowCard flowCard = JSON.parseObject(itemJObj.toJSONString(), FlowCard.class);
                                    initTable(flowCard);
                                } else {
                                    Toast.makeText(_mActivity, "查询电容所在流转卡数据为空", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(_mActivity, "查询电容所在流转卡数据为空", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(_mActivity, "查询电容所在流转卡失败", Toast.LENGTH_SHORT).show();
                        }
                        hideProgressBar();
                    })
                    .failure(() -> {
                        Toast.makeText(_mActivity, "查询电容所在流转卡失败", Toast.LENGTH_SHORT).show();
                        hideProgressBar();
                    })
                    .error((int code, String msg) -> {
                        Toast.makeText(_mActivity, "查询电容所在流转卡错误", Toast.LENGTH_SHORT).show();
                        hideProgressBar();
                        Log.e("test", "code = " + code + " msg = " + msg);
                    }).build();
            Log.e("test", "url = " + restClient.getURL());
            restClient.get();

        } else {
            Toast.makeText(_mActivity, "批号参数错误", Toast.LENGTH_SHORT).show();
        }
    }

    private void initTable(FlowCard parameter) {
        Form[][] forms = new Form[][]{
                {
                        new Form(getString(R.string.batch_id), 2),
                        new Form(parameter.getBatchId(), 3),
                        new Form(getString(R.string.product_code), 2),
                        new Form(parameter.getProductCode(), 3),
                        new Form(getString(R.string.customer_code), 2),
                        new Form(getNoNullStr(parameter.getCustomerCode()), 3)
                },
                {
                        new Form(getString(R.string.fill_table_date), 2),
                        new Form(getNoNullStr(parameter.getTime()), 3),
                        new Form(getString(R.string.specification), 2),
                        new Form(getNoNullStr(parameter.getSpecification()), 3),
                        new Form(getString(R.string.size), 2),
                        new Form(getNoNullStr(parameter.getSize()), 3),

                },
                {
                        new Form(getString(R.string.device_number), 2),
                        new Form(getNoNullStr(parameter.getDeviceNo()), 3),
                        new Form(getString(R.string.order_quantity), 2),
                        new Form(getNoNullStr(parameter.getAmount()), 3),
                        new Form(getString(R.string.range_of_capacity), 2),
                        new Form(getNoNullStr(parameter.getCapacityRange()), 3),
                },
                {
                        new Form(getString(R.string.positive_foil_specific_volume), 2),
                        new Form(getNoNullStr(parameter.getPositiveFoil()), 3),
                        new Form("", 2),
                        new Form("", 3),
                        new Form("", 2),
                        new Form("", 3),
                },
                {
                        new Form(getString(R.string.positive_foil_type), 2),
                        new Form(getNoNullStr(parameter.getPositiveFoilModel()), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getNoNullStr(parameter.getPositiveFoilSupplier()), 3),
                        new Form(getString(R.string.process_dimension), 2),
                        new Form(getNoNullStr(parameter.getPositiveFoilSize()), 3)},
                {
                        new Form(getString(R.string.negative_foil_type), 2),
                        new Form(getNoNullStr(parameter.getNegativeFoilModel()), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getNoNullStr(parameter.getNegativeFoilSupplier()), 3),
                        new Form(getString(R.string.process_dimension), 2),
                        new Form(getNoNullStr(parameter.getNegativeFoilSize()), 3)},
                {
                        new Form(getString(R.string.guide_pin_type), 2),
                        new Form(getNoNullStr(parameter.getGuildPinModel()), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getNoNullStr(parameter.getGuildPinSupplier()), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getNoNullStr(parameter.getGuildPinRemark()), 3)},
                {
                        new Form(getString(R.string.electrolytic_paper), 2),
                        new Form(getNoNullStr(parameter.getElectrolyticPaper1()), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getNoNullStr(parameter.getElectrolyticPaper1Supplier()), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getNoNullStr(parameter.getElectrolyticPaper1Remark()), 3)},
                {
                        new Form(getString(R.string.electrolytic_paper_2), 2),
                        new Form(getNoNullStr(parameter.getElectrolyticPaper2()), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getNoNullStr(parameter.getElectrolyticPaper2Supplier()), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getNoNullStr(parameter.getElectrolyticPaper2Remark()), 3)},
                {
                        new Form(getString(R.string.electrolyte), 2),
                        new Form(getNoNullStr(parameter.getElectrolyte()), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getNoNullStr(parameter.getElectrolyteSupplier()), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getNoNullStr(parameter.getElectrolyteRemark()), 3)},
                {
                        new Form(getString(R.string.cover), 2),
                        new Form(getNoNullStr(parameter.getCover()), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getNoNullStr(parameter.getCoverSupplier()), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getNoNullStr(parameter.getCoverRemark()), 3)},
                {
                        new Form(getString(R.string.aluminum_case), 2),
                        new Form(getNoNullStr(parameter.getAluminumShell()), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getNoNullStr(parameter.getAluminumShellSupplier()), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getNoNullStr(parameter.getAluminumShellRemark()), 3)},
                {
                        new Form(getString(R.string.drive_pipe), 2),
                        new Form(getNoNullStr(parameter.getCasing()), 3),
                        new Form(getString(R.string.supplier), 2),
                        new Form(getNoNullStr(parameter.getCasingSupplier()), 3),
                        new Form(getString(R.string.remark), 2),
                        new Form(getNoNullStr(parameter.getCasingRemark()), 3)
                }
        };

        FormTableData<Form> tableData = FormTableData.create(table, "", 15, forms);
        tableData.setFormat((t) -> {
            if (t != null) {
                return getNoNullStr(t.getContent());
            } else {
                return "";
            }
        });
        table.setTableData(tableData);
    }

    private String getNoNullStr(String str) {
        return str == null ? "" : str;
    }

    private void hideProgressBar() {
        binding.queryProgressBar.setVisibility(View.GONE);
        binding.queryingTv.setVisibility(View.GONE);
    }


}
