package com.sunseen.capacitormachine.modules.query.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.modules.query.bean.CapacitorUidBean;

import java.util.List;

public class ResultListAdapter extends BaseAdapter {
    private List<CapacitorUidBean> aData;
    private Context mContext;
    public ResultListAdapter(Context mContext, @Nullable List<CapacitorUidBean> data) {
//        super(layoutResId, data);
        this.mContext = mContext;
        this.aData = data;
    }
//
//    @Override
//    protected void convert(BaseViewHolder helper, CapacitorUidBean item) {
//        helper.setText(R.id.tv_capacitor_id,
//                String.format(helper.itemView.getContext().getString(R.string.capacitor_uid_format),
//                        item.getUid()))
//                .setText(R.id.tv_order_id, String.format(helper.itemView.getContext()
//                                .getString(R.string.produce_order_id),
//                        item.getBatchId()));
//    }

    @Override
    public int getCount() {
        return aData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView==null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_item_result_list,parent,false);
            holder = new ViewHolder();
            holder.img_right_arrow = (ImageView)convertView.findViewById(R.id.img_right_arrow);
            holder.tv_capacitor_id = (TextView)convertView.findViewById(R.id.tv_capacitor_id);
            holder.tv_order_id = (TextView)convertView.findViewById(R.id.tv_order_id);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
//        holder.img_right_arrow.setBackgroundResource(aData.get(position).getClass());
        holder.tv_capacitor_id.setText(aData.get(position).getUid());
        holder.tv_order_id.setText(aData.get(position).getBatchId());
        return convertView;
    }
    static class ViewHolder{
        ImageView img_right_arrow;
        TextView tv_capacitor_id;
        TextView tv_order_id;
    }
}
