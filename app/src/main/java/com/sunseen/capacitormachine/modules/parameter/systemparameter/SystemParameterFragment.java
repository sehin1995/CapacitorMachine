package com.sunseen.capacitormachine.modules.parameter.systemparameter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.databinding.FragmentSystemParameterBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.common.SharePreferenceUtil;

import androidx.databinding.ViewDataBinding;

public class SystemParameterFragment extends BaseFragment {
    @Override
    protected int setLayout() {
        return R.layout.fragment_system_parameter;
    }

    private String ipStr;
    private String portStr;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        ipStr = SharePreferenceUtil.getInstance(getContext()).getString(SharePreferenceUtil.IP_STR, "192.168.1.1");
        portStr = SharePreferenceUtil.getInstance(getContext()).getString(SharePreferenceUtil.PORT_STR, "80");
        final FragmentSystemParameterBinding binding = (FragmentSystemParameterBinding) viewDataBinding;

        View.OnFocusChangeListener focusChangeListener = (View v, boolean hasFocus) -> {
            if (v instanceof EditText) {
                EditText editText = (EditText) v;
                if (hasFocus) {
                    editText.setSelection((editText.getText().length()));
                } else {
                    if (editText.getText() == null || editText.getText().length() <= 0) {
                        editText.setText("0");
                    }
                }
            }
        };
        binding.editIp01.setOnFocusChangeListener(focusChangeListener);
        binding.editIp02.setOnFocusChangeListener(focusChangeListener);
        binding.editIp03.setOnFocusChangeListener(focusChangeListener);
        binding.editIp04.setOnFocusChangeListener(focusChangeListener);
        binding.editPort.setOnFocusChangeListener(focusChangeListener);

        String[] ips = ipStr.split(".");
        if (ips.length >= 4) {
            binding.editIp01.setText(ips[0]);
            binding.editIp02.setText(ips[1]);
            binding.editIp03.setText(ips[2]);
            binding.editIp04.setText(ips[3]);
        }
        binding.editPort.setText(portStr);

        binding.editIp01.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.editIp02.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.editIp03.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.editIp04.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
