package com.sunseen.capacitormachine.modules.permission;

import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.databinding.FragmentPermissionBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.permission.factory.DeveloperFragment;
import com.sunseen.capacitormachine.modules.permission.operation.FactoryOperationFragment;

import androidx.annotation.LayoutRes;
import androidx.databinding.ViewDataBinding;

/**
 * @author zest
 */
public class PermissionFragment extends BaseFragment {
    @Override
    protected @LayoutRes
    int setLayout() {
        return R.layout.fragment_permission;
    }

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        FragmentPermissionBinding binding = (FragmentPermissionBinding) viewDataBinding;
        binding.btnFactoryDebug.setOnClickListener((v) -> {
            getParent().start(new DeveloperFragment());
        });
        binding.btnCompanyManage.setOnClickListener((v) -> {

        });
        binding.btnFactoryOperation.setOnClickListener((v) -> {
            getParent().start(new FactoryOperationFragment());
        });
    }

}
