package com.sunseen.capacitormachine.commumication.tcp.event;

public class HolderTemperatureEvent {
    private int temperature;

    public HolderTemperatureEvent(int temperature) {
        this.temperature = temperature;
    }

    public int getTemperature() {
        return temperature;
    }
}
