package com.sunseen.capacitormachine.modules.log.bean;

public class AlarmInfoBean {
    private long time;
    private String alarmInfo;

    public AlarmInfoBean() {
    }

    public AlarmInfoBean(long time, String alarmInfo) {
        this.time = time;
        this.alarmInfo = alarmInfo;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getAlarmInfo() {
        return alarmInfo;
    }

    public void setAlarmInfo(String alarmInfo) {
        this.alarmInfo = alarmInfo;
    }
}
