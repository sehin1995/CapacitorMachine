package com.sunseen.capacitormachine.modules.parameter.productparameter;

import android.os.Bundle;
import android.util.Log;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.commumication.http.HttpUtil;
import com.sunseen.capacitormachine.commumication.http.RestClient;
import com.sunseen.capacitormachine.data.DataUtil;
import com.sunseen.capacitormachine.databinding.FragmentProductParameterBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.parameter.bean.ProcessBean;
import com.sunseen.capacitormachine.modules.parameter.productparameter.form.AgingProcessParameterFormFragment;
import com.sunseen.capacitormachine.modules.parameter.productparameter.form.SortingParameterFormFragment;
import com.sunseen.capacitormachine.modules.parameter.productparameter.form.PowerSupplyFormFragment;
import com.sunseen.capacitormachine.modules.parameter.productparameter.form.ProductFlowFragment;
import com.sunseen.capacitormachine.modules.parameter.productparameter.form.SprayCodeFormFragment;

import androidx.databinding.ViewDataBinding;

/**
 * @author zest
 */
public class ProductParameterFragment extends BaseFragment {

    @Override
    protected int setLayout() {
        return R.layout.fragment_product_parameter;
    }

    private BaseFragment[] fragments;

    private int curFragmentPos = 0;
    /**
     * 当前生产的批次的标志id
     */
    private String batchId;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        FragmentProductParameterBinding binding = (FragmentProductParameterBinding) viewDataBinding;
        ProductFlowFragment fragment1 = new ProductFlowFragment();
        Bundle bundle = getArguments();
        if (bundle != null) {
            batchId = bundle.getString("batchId");
        }
        Bundle newBundle = new Bundle();
        newBundle.putString("batchId", batchId);
        fragment1.setArguments(newBundle);
        SortingParameterFormFragment fragment2 = new SortingParameterFormFragment();
        PowerSupplyFormFragment fragment3 = new PowerSupplyFormFragment();
        SprayCodeFormFragment fragment4 = new SprayCodeFormFragment();
        AgingProcessParameterFormFragment fragment5 = new AgingProcessParameterFormFragment();
        fragments = new BaseFragment[]{fragment1, fragment2, fragment3, fragment4, fragment5};
        loadMultipleRootFragment(R.id.fragment_container, 0, fragments);
        queryEndListeners = new ProcessQueryEnd[]{fragment2, fragment3, fragment4, fragment5};
        binding.toolBar.setNavigationIcon(R.drawable.svg_arrow_back_white_64dp);
        binding.toolBar.setNavigationOnClickListener((view) -> pop());
        binding.rgFormMenu.setOnCheckedChangeListener((RadioGroup group, int checkedId) -> {
            switch (checkedId) {
                case R.id.rb_product_flow: {
                    showHideFragment(fragments[0], fragments[curFragmentPos]);
                    curFragmentPos = 0;
                }
                break;
                case R.id.rb_sorting_parameter: {
                    showHideFragment(fragments[1], fragments[curFragmentPos]);
                    curFragmentPos = 1;
                }
                break;
                case R.id.rb_power_supply: {
                    showHideFragment(fragments[2], fragments[curFragmentPos]);
                    curFragmentPos = 2;
                }
                break;
                case R.id.rb_spray_code: {
                    showHideFragment(fragments[3], fragments[curFragmentPos]);
                    curFragmentPos = 3;
                }
                break;
                case R.id.rb_aging_process: {
                    showHideFragment(fragments[4], fragments[curFragmentPos]);
                    curFragmentPos = 4;
                }
                break;
                default: {

                }
                break;
            }
        });
        queryProcessParameter();
    }

    private ProcessQueryEnd[] queryEndListeners;

    private void queryProcessParameter() {
        RestClient.builder()
                .url(HttpUtil.Process + "/" + batchId)
                .params("deviceNo", DataUtil.DeviceNo)
                .success((String response) -> {
                    JSONObject rootObj = JSON.parseObject(response);
                    if (rootObj != null) {
                        int status = rootObj.getIntValue("status");
                        if (status == 1) {
                            JSONObject dataObj = rootObj.getJSONObject("data");
                            if (dataObj != null) {
                                ProcessBean bean = JSON.parseObject(
                                        dataObj.getJSONObject("item").toJSONString(),
                                        ProcessBean.class);
                                if (bean != null) {
                                    for (ProcessQueryEnd queryEnd : queryEndListeners) {
                                        queryEnd.onProcessQueryEnd(bean);
                                    }
                                } else {
                                    Toast.makeText(_mActivity, "工艺参数查询结果为空", Toast.LENGTH_SHORT).show();
                                    Log.e("test", "工艺参数查询结果为空");
                                }
                            } else {
                                Toast.makeText(_mActivity, "工艺参数查询结果为空", Toast.LENGTH_SHORT).show();
                                Log.e("test", "工艺参数查询结果为空:");
                            }
                        } else {
                            String msg = rootObj.getString("message");
                            if (msg == null) {
                                msg = "未知原因";
                            }
                            Toast.makeText(_mActivity, "工艺参数查询失败: " + msg, Toast.LENGTH_SHORT).show();
                            Log.e("test", "工艺参数查询失败: " + msg);
                        }
                    } else {
                        Toast.makeText(_mActivity, "工艺参数查询失败", Toast.LENGTH_SHORT).show();
                        Log.e("test", "工艺参数查询失败,根数据为空");
                    }
                })
                .failure(() -> {
                    Toast.makeText(_mActivity, "工艺参数查询结果失败", Toast.LENGTH_SHORT).show();
                    Log.e("test", "工艺参数查询结果失败");
                })
                .error((int code, String msg) -> {
                    Toast.makeText(_mActivity, "工艺参数查询结果错误", Toast.LENGTH_SHORT).show();
                    Log.e("test", "工艺参数查询结果错误 " + code + " " + msg);
                })
                .build()
                .get();
    }
}
