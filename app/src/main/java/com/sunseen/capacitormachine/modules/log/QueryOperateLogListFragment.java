package com.sunseen.capacitormachine.modules.log;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.BaseFragment;

import com.sunseen.capacitormachine.commumication.http.HttpUtil;
import com.sunseen.capacitormachine.commumication.http.RestClient;
import com.sunseen.capacitormachine.data.DataUtil;
import com.sunseen.capacitormachine.databinding.FragmentQueryOperateLogBinding;
import com.sunseen.capacitormachine.modules.log.adapter.OperateLogAdapter;
import com.sunseen.capacitormachine.modules.log.bean.UserOperateBean;
import com.sunseen.capacitormachine.modules.log.event.QueryLogEvent;
import com.sunseen.capacitormachine.modules.log.event.RefreshLogListEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class QueryOperateLogListFragment extends BaseFragment {

    @Override
    protected int setLayout() {
        return R.layout.fragment_query_operate_log;
    }

    private FragmentQueryOperateLogBinding binding;

    private long startTime;
    private long endTime;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        binding = (FragmentQueryOperateLogBinding) viewDataBinding;

        adapter = new OperateLogAdapter(R.layout.layout_item_operate_log);
        binding.operateRv.setLayoutManager(new LinearLayoutManager(_mActivity));
        adapter.bindToRecyclerView(binding.operateRv);
        adapter.setEnableLoadMore(true);
        adapter.setLoadMoreView(new LogDateLoadMoreView());
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                if (curPage == lastPage) {
                    if (fragmentShowing) {
                        Toast.makeText(_mActivity, "该日期的操作日志已全部加载", Toast.LENGTH_SHORT).show();
                    }
                    adapter.loadMoreEnd(true);
                } else {
                    queryLogs(startTime, endTime, curPage + 1, false);
                }
            }
        }, binding.operateRv);
        adapter.disableLoadMoreIfNotFullPage();
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

            }
        });
        EventBus.getDefault().register(this);
    }

    private OperateLogAdapter adapter;
    private int lastPage = 0;
    private int curPage = 0;

    private void queryLogs(long startTime, long endTime, int page, boolean refresh) {
        Log.e("test", "queryLogs: " + startTime + " :" + endTime);
        RestClient.builder()
                .url(HttpUtil.OperateLog)
                .params("deviceNo", DataUtil.DeviceNo)
                .params("start_time", startTime)
                .params("end_time", endTime)
                .params("platform", 1)
                .params("userId", 1)
                .params("page", page)
                .success((String response) -> {
                    Log.e("test", "queryLogs: success ");
                    JSONObject rootObj = JSON.parseObject(response);
                    int status = rootObj.getIntValue("status");
                    if (status == 1) {
                        JSONObject dataObj = rootObj.getJSONObject("data");
                        curPage = dataObj.getIntValue("current_page");
                        List<UserOperateBean> operateBeanList = dataObj.getJSONArray("data")
                                .toJavaList(UserOperateBean.class);
                        if (!operateBeanList.isEmpty()) {
                            if (refresh) {
                                adapter.setNewData(operateBeanList);
                                lastPage = dataObj.getIntValue("last_page");
                            } else {
                                adapter.addData(operateBeanList);
                                adapter.loadMoreComplete();
                            }
                        } else {
                            Toast.makeText(_mActivity, "操作日志数据返回为空", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(_mActivity, "操作日志数据返回状态码0", Toast.LENGTH_SHORT).show();
                    }
                })
                .failure(() -> {
                    Toast.makeText(_mActivity, "操作日志数据请求失败", Toast.LENGTH_SHORT).show();
                    Log.e("test", "queryLogs: failure");
                })
                .error((int code, String msg) -> {
                    Toast.makeText(_mActivity, "操作日志数据返回错误:" + msg, Toast.LENGTH_SHORT).show();
                    Log.e("test", "queryLogs: error" + msg);
                })
                .build()
                .get();
    }


    private boolean fragmentShowing = true;

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        fragmentShowing = !hidden;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onQueryLog(QueryLogEvent event) {
        if (event.getLogType() == 0) {
            this.startTime = event.getStartTimeStamp();
            this.endTime = event.getEndTimeStamp();
            queryLogs(startTime, endTime, 1, true);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefreshLogList(RefreshLogListEvent event) {
        if (event.getPos() == 2) {
            if (startTime > 0) {
                queryLogs(startTime, endTime, 1, true);
            } else {
                Toast.makeText(_mActivity, "请输入正确的查询日期", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }
}
