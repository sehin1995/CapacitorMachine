package com.sunseen.capacitormachine.modules.parameter.productivetask.event;


public class SetParameterEvent {
    private boolean process;
    private String key;
    private Object value;

    public SetParameterEvent(String key, String value) {
        this.key = key;
        this.value = value;
        this.process = false;
    }

    public SetParameterEvent(boolean process, String key, Object value) {
        this.process = process;
        this.key = key;
        this.value = value;
    }

    public boolean isProcess() {
        return process;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }
}
