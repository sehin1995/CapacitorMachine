package com.sunseen.capacitormachine.modules.parameter.productivetask.event;

import com.sunseen.capacitormachine.modules.parameter.bean.ProcessParameter;

public class ProduceStateChangeEvent {
    private boolean parameterChange;//生产参数是否变化
    private ProcessParameter parameter;

    public ProduceStateChangeEvent(ProcessParameter parameter) {
        this.parameter = parameter;
        parameterChange = true;
    }

    public ProduceStateChangeEvent() {
        parameterChange = false;
    }


    public ProcessParameter getParameter() {
        return parameter;
    }

    public boolean isParameterChange() {
        return parameterChange;
    }
}
