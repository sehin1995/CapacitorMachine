package com.sunseen.capacitormachine.modules.parameter.productivetask.adapter;

import android.view.View;

import com.chad.library.adapter.base.BaseItemDraggableAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.modules.parameter.bean.ProcessParameter;

import java.util.List;

import androidx.annotation.Nullable;

/**
 * @author zest
 */
public class ItemTaskAdapter extends BaseItemDraggableAdapter<ProcessParameter, BaseViewHolder> {
    public ItemTaskAdapter(int layoutResId, @Nullable List<ProcessParameter> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ProcessParameter item) {
        helper.setText(R.id.tv_flow_id, item.getBatchId())
                .setText(R.id.tv_batch_id, item.getBatchId())
                .setText(R.id.tv_task_state, item.getState() == 0 ? "待生产" : (item.getState() == 2 ? "已生产" : "生产中"));
        helper.getView(R.id.btn_modify).setVisibility(View.GONE);
        helper.getView(R.id.btn_delete).setVisibility(View.GONE);
        helper.addOnClickListener(R.id.btn_delete, R.id.btn_modify, R.id.img_btn_show_menu);
    }
}
