package com.sunseen.capacitormachine.modules.realtime.event;

public class CountUpdateEvent {

    private boolean qrCodeError;
    private boolean shortCircuit;
    private boolean openCircuit;
    private boolean convex;
    private boolean unAge;
    private boolean exceedTemperature;
    private boolean surge;
    private boolean surge2;
    private boolean feed;
    private boolean produce;
    private boolean acuteMaladyCount;
    /**
     * 1:优品
     * 2:良品
     * 3:高容
     * 4:低容
     * 5:损失角
     * 6:漏电流
     * 7:阻抗
     * 8:重测
     * */
    private int grade;
    private int temperature;
    private String leakCurrentValue1;
    private String capacity1;
    private String lossAngle1;
    private String impedance1;
    private String leakCurrentValue2;
    private String capacity2;
    private String lossAngle2;
    private String impedance2;

    private CountInfo countInfo;//新计数数据，直接加入到这里，省的重写

    public CountUpdateEvent(boolean qrCodeError,boolean shortCircuit, boolean openCircuit, boolean convex,
                            boolean unAge, boolean exceedTemperature, boolean surge,
                            boolean surge2, boolean feed, boolean produce, int grade,
                            int temperature, String leakCurrentValue1, String capacity1,
                            String lossAngle1, String impedance1, String leakCurrentValue2,
                            String capacity2, String lossAngle2, String impedance2, boolean acuteMaladyCount, CountInfo countInfo) {
        this.qrCodeError = qrCodeError;
        this.shortCircuit = shortCircuit;
        this.openCircuit = openCircuit;
        this.convex = convex;
        this.unAge = unAge;
        this.exceedTemperature = exceedTemperature;
        this.surge = surge;
        this.surge2 = surge2;
        this.feed = feed;
        this.produce = produce;
        this.grade = grade;
        this.temperature = temperature;
        this.leakCurrentValue1 = leakCurrentValue1;
        this.capacity1 = capacity1;
        this.lossAngle1 = lossAngle1;
        this.impedance1 = impedance1;
        this.leakCurrentValue2 = leakCurrentValue2;
        this.capacity2 = capacity2;
        this.lossAngle2 = lossAngle2;
        this.impedance2 = impedance2;
        this.acuteMaladyCount = acuteMaladyCount;
        this.countInfo = countInfo;
    }

    public String toStrings(){
        return "qrCodeError = " + qrCodeError + ", shortCircuit = " + shortCircuit
                + ", openCircuit = " + openCircuit + ", convex = " + convex + ", unAge = " + unAge
                + ", exceedTemperature = " + exceedTemperature
                + ", surge = " + surge + ", surge2 = " + surge2
                + ", feed = " + feed + ", produce = " + produce + ", grade = " + grade
                + ", temperature = " + temperature + ", leakCurrentValue1 = "
                + leakCurrentValue1 + ", capacity1 = " + capacity1
                + ", lossAngle1 = " + lossAngle1 + ", impedance1 = "
                + impedance1 + ", leakCurrentValue2 = " + leakCurrentValue2
                + ", capacity2 = " + capacity2 + ", lossAngle2 = "
                + lossAngle2 + ", impedance2 = " + impedance2;
    }

   public boolean isQrCodeError() {
        return qrCodeError;
    }

    public boolean isShortCircuit() {
        return shortCircuit;
    }

    public boolean isOpenCircuit() {
        return openCircuit;
    }

    public boolean isConvex() {
        return convex;
    }

    public boolean isUnAge() {
        return unAge;
    }

    public boolean isExceedTemperature() {
        return exceedTemperature;
    }

    public boolean isSurge() {
        return surge;
    }

    public boolean isSurge2() {
        return surge2;
    }

    public boolean isFeed() {
        return feed;
    }

    public boolean isProduce() {
        return produce;
    }

    public int getGrade() {
        return grade;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getLeakCurrentValue1() {
        return leakCurrentValue1;
    }

    public String getCapacity1() {
        return capacity1;
    }

    public String getLossAngle1() {
        return lossAngle1;
    }

    public String getImpedance1() {
        return impedance1;
    }

    public String getLeakCurrentValue2() {
        return leakCurrentValue2;
    }

    public String getCapacity2() {
        return capacity2;
    }

    public String getLossAngle2() {
        return lossAngle2;
    }

    public String getImpedance2() {
        return impedance2;
    }

    public boolean isAcuteMaladyCount() {
        return acuteMaladyCount;
    }

    public void setAcuteMaladyCount(boolean acuteMaladyCount) {
        this.acuteMaladyCount = acuteMaladyCount;
    }

    public CountInfo getCountInfo() {
        return countInfo;
    }

    public void setCountInfo(CountInfo countInfo) {
        this.countInfo = countInfo;
    }
}
