package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class CapacityJsonObj {
    public String type;
    public int time;
    public CapacityData data;

    public CapacityJsonObj(String type, int time, CapacityData data) {
        this.type = type;
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public CapacityData getData() {
        return data;
    }

    public void setData(CapacityData data) {
        this.data = data;
    }

}
