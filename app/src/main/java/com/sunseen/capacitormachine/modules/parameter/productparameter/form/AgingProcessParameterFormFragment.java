package com.sunseen.capacitormachine.modules.parameter.productparameter.form;

import com.bin.david.form.core.SmartTable;
import com.bin.david.form.data.table.FormTableData;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.MethodUtil;
import com.sunseen.capacitormachine.databinding.FragmentCapacitorFormBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.parameter.bean.ProcessBean;
import com.sunseen.capacitormachine.common.Form;
import com.sunseen.capacitormachine.modules.parameter.productparameter.ProcessQueryEnd;

import androidx.databinding.ViewDataBinding;

/**
 * @author zest
 */
public class AgingProcessParameterFormFragment extends BaseFragment implements ProcessQueryEnd {

    @Override
    protected int setLayout() {
        return R.layout.fragment_capacitor_form;
    }

    private SmartTable<Form> table;
    private Form[][] forms;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        final FragmentCapacitorFormBinding binding = (FragmentCapacitorFormBinding) viewDataBinding;
        table = binding.tableAgingProcessParameter;
        table.getConfig().setShowXSequence(false);
        table.getConfig().setShowYSequence(false);
        table.getConfig().setShowTableTitle(false);
        table.getConfig().getContentGridStyle().
                setColor(getResources().getColor(R.color.table_content_grid_color));
        table.getConfig().getContentStyle().
                setTextColor(getResources().getColor(R.color.table_content_text_color));
        initForm();
        FormTableData<Form> measureData = FormTableData.create(binding.tableAgingProcessParameter,
                "", 13, forms);
        measureData.setFormat((t) -> {
            if (t != null) {
                return t.getContent();
            } else {
                return "";
            }
        });
        table.setTableData(measureData);
    }

    private void initForm() {
        forms = new Form[][]{
                {
                        new Form(getString(R.string.instrument_frequency), 3),
                        new Form("", 10)
                },
                {
                        new Form(getString(R.string.oven_temperature), 3),
                        new Form("", 10)
                },
                {
                        new Form(getString(R.string.aging_duration), 3),
                        new Form("", 10)
                },
                {
                        new Form(getString(R.string.feed_in_table_height), 3),
                        new Form("", 10)
                },
                {
                        new Form(getString(R.string.feed_in_angle), 3),
                        new Form("", 10)
                },
                {
                        new Form(getString(R.string.discharge_angle), 3),
                        new Form("", 10)
                },
                {
                        new Form(getString(R.string.discharge_table_height), 3),
                        new Form("", 10)
                },

        };
    }

    @Override
    public void onProcessQueryEnd(ProcessBean processBean) {
        if (isAdded() && getActivity() != null) {
            forms[0][1].setContent("120Hz");
            forms[1][1].setContent(MethodUtil.createStringUnit(processBean.getOvenTemperature(),
                    1, getString(R.string.temp_unit_flag)));

            forms[2][1].setContent(MethodUtil.createStringUnit(processBean.getAgingTime(), 0,
                    getString(R.string.minute)));
            forms[3][1].setContent(MethodUtil.createStringUnit(processBean.getFeedInTableHeight(), 1, ""));
            forms[4][1].setContent(MethodUtil.createStringUnit(processBean.getFeedInAngle(), 1, "°"));
            forms[5][1].setContent(MethodUtil.createStringUnit(processBean.getDischargeAngle(), 1, "°"));
            forms[6][1].setContent(MethodUtil.createStringUnit(processBean.getDischargeTableHeight(), 1, ""));
            table.invalidate();
        }
    }
}
