package com.sunseen.capacitormachine.modules.permission.factory;

import java.io.File;

public class FileCheckBean {

    private boolean check;
    private File file;

    public FileCheckBean(File file) {
        this.check = false;
        this.file = file;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
