package com.sunseen.capacitormachine.common;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPool {
    public static ExecutorService executor =
            new ThreadPoolExecutor(5, 30, 60,
                    TimeUnit.SECONDS, new LinkedBlockingQueue<>(500));
}
