package com.sunseen.capacitormachine.modules.home.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.modules.home.bean.HolderOffLineBean;

public class HolderOffLineAdapter extends BaseQuickAdapter<HolderOffLineBean, BaseViewHolder> {
    public HolderOffLineAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, HolderOffLineBean item) {
        Context context = helper.itemView.getContext();
        helper.setText(R.id.holder_id_tv, String.format(context.getString(R.string.holder_id_format)
                , Integer.toString(item.getHolderId())))
                .setText(R.id.holder_pos_tv, String.format(context.getString(R.string.holder_pos_format),Integer.toString(item.getHolderPos())))
                .setText(R.id.holder_status_tv, item.getHolderOffLineType() == 1
                        ? context.getString(R.string.holder_off_line)
                        : context.getString(R.string.holder_fault));
    }
}
