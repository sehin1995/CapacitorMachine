package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class OvenTemperatureBatchJsonObj {
    public String type = "ovenTemperatureBatch";
    public int time;
    public OvenTemperatureBatchData data;

    public OvenTemperatureBatchJsonObj(String type, int time, OvenTemperatureBatchData data) {
        this.time = time;
        this.data = data;
    }
}
