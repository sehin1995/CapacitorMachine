package com.sunseen.capacitormachine.common;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author zest
 * */
public class SharePreferenceUtil {

    public static final String LOGIN_STATE = "login_state";
    public static final String IP_STR = "IP";
    public static final String PORT_STR = "PORT";
    public static final String CURRENT_PRODUCTION = "current_production";
    public static final String CHANGE_BATCH = "change_batch";

    /**
     * 进料计数
     */
    public static final String FEED_COUNT = "FEED_COUNT";
    /**
     * 短路计数
     */
    public static final String SHORT_CIRCUIT_COUNT = "SHORT_CIRCUIT_COUNT";
    /**
     * 开路计数
     */
    public static final String OPEN_CIRCUIT_COUNT = "OPEN_CIRCUIT_COUNT";
    /**
     * 极性不良计数
     */
    public static final String ACUTE_MALADY_COUNT = "ACUTE_MALADY_COUNT";

    /**
     * 未老化计数
     */
    public static final String UN_AGE_COUNT = "UN_AGE_COUNT";
    /**
     * 凸顶计数
     */
    public static final String CONVEX_COUNT = "CONVEX_COUNT";
    /**
     * 漏电计数
     */
    public static final String LEAK_CURRENT_COUNT = "LEAK_CURRENT_COUNT";
    /**
     * 浪涌计数
     */
    public static final String SURGE_COUNT = "SURGE_COUNT";
    /**
     * 高容计数
     */
    public static final String HIGH_CAPACITY_COUNT = "HIGH_CAPACITY_COUNT";

    /**
     * 低容计数
     */
    public static final String LOW_CAPACITY_COUNT = "LOW_CAPACITY_COUNT";
    /**
     * 阻抗计数
     */
    public static final String ESR_COUNT = "ESR_COUNT";
    /**
     * 损失计数
     */
    public static final String LOSS_ANGLE_COUNT = "LOSS_ANGLE_COUNT";
    /**
     * 优品计数
     */
    public static final String EXCELLENT_PRODUCT_COUNT = "EXCELLENT_PRODUCT_COUNT";
    /**
     * 良品计数
     */
    public static final String GOOD_PRODUCT_COUNT = "GOOD_PRODUCT_COUNT";

    /**
     * 重测计数
     */
    public static final String RE_TEST_COUNT = "RE_TEST_COUNT";

    /**
     * 内爆计数
     */
    public static final String IMPLOSION_COUNT = "IMPLOSION_COUNT";

    /**
     * 二维码
     */
    public static final String QR_CODE_ERROR_COUNT = "QR_CODE_ERROR_COUNT";


    private static final String APP_PREFER = "app_prefer";

    private static SharePreferenceUtil spUtil;


    private SharedPreferences sp;
    private Context mContext;

    private static final byte[] sLock = new byte[0];

    private SharePreferenceUtil(Context context) {
        mContext = context;
        sp = context.getSharedPreferences(APP_PREFER, Context.MODE_PRIVATE);
    }

    public static SharePreferenceUtil getInstance(Context context) {
        synchronized (sLock) {
            if (spUtil == null) {
                spUtil = new SharePreferenceUtil(context);
            }
        }
        return spUtil;
    }

    public void putString(String key, String value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void putInt(String key, int value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void putLong(String key, long value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public void putFloat(String key, Float value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public String getString(String key, String defaultValue) {
        return sp.getString(key, defaultValue);
    }

    public int getInt(String key, int defaultValue) {
        return sp.getInt(key, defaultValue);

    }

    public long getLong(String key, long defaultValue) {
        return sp.getLong(key, defaultValue);

    }

    public float getFloat(String key, float defaultValue) {
        return sp.getFloat(key, defaultValue);

    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return sp.getBoolean(key, defaultValue);

    }

}
