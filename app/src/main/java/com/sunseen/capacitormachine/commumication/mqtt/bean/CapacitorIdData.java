package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class CapacitorIdData {
    public String uid;
    public String capacitorId;
    public String batchId;

    public CapacitorIdData(String uid, String capacitorId, String batchId) {
        this.uid = uid;
        this.capacitorId = capacitorId;
        this.batchId = batchId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCapacitorId() {
        return capacitorId;
    }

    public void setCapacitorId(String capacitorId) {
        this.capacitorId = capacitorId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

}
