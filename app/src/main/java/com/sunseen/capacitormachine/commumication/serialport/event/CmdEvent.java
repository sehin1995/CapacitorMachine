package com.sunseen.capacitormachine.commumication.serialport.event;

import com.sunseen.capacitormachine.commumication.serialport.PlcService;
import com.sunseen.capacitormachine.data.bean.DetectionFlagBean;
import com.sunseen.capacitormachine.modules.parameter.bean.ProcessParameter;

/**
 * @author zest
 * 应用主界面点击各控制按钮(初始化，开始，停止，复位，换料)时，将发送此事件
 */
public class CmdEvent {
    private @PlcService.ControlCommand
    int cmdType;

    private ProcessParameter parameter;

    private DetectionFlagBean detectionFlagBean;


    public CmdEvent(@PlcService.ControlCommand int cmdType) {
        this.cmdType = cmdType;
    }

    public CmdEvent(@PlcService.ControlCommand int cmdType, ProcessParameter parameter) {
        this.cmdType = cmdType;
        this.parameter = parameter;
    }

    public CmdEvent(int cmdType, DetectionFlagBean detectionFlagBean) {
        this.cmdType = cmdType;
        this.detectionFlagBean = detectionFlagBean;
    }

    public int getCmdType() {
        return cmdType;
    }

    public ProcessParameter getParameter() {
        return parameter;
    }

    public DetectionFlagBean getDetectionFlagBean() {
        return detectionFlagBean;
    }
}
