package com.sunseen.capacitormachine.modules.home.event;

public class RecoverCmdStateEvent {
    private boolean delayRun;

    public RecoverCmdStateEvent(boolean delayRun) {
        this.delayRun = delayRun;
    }

    public boolean isDelayRun() {
        return delayRun;
    }
}
