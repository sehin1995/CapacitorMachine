package com.sunseen.capacitormachine.modules.home.adapter;


import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.modules.home.bean.CapacitorAdapterBean;

import androidx.annotation.Nullable;

import java.util.List;

public class CapacitorItemAdapter extends BaseQuickAdapter<CapacitorAdapterBean, BaseViewHolder> {

    public CapacitorItemAdapter(int layoutResId, @Nullable List<CapacitorAdapterBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, CapacitorAdapterBean item) {
        helper.setText(R.id.tv_index, String.valueOf(item.getPos()));
        helper.setText(R.id.uid_tv, item.getUid());
        if (item.isEmpty()) {
            helper.getView(R.id.img_holder).setVisibility(View.INVISIBLE);
            helper.getView(R.id.uid_tv).setVisibility(View.INVISIBLE);
        } else {
            helper.getView(R.id.img_warn).setVisibility(item.isNormal() ? View.INVISIBLE : View.VISIBLE);
            helper.getView(R.id.img_holder).setVisibility(View.VISIBLE);
            helper.getView(R.id.uid_tv).setVisibility(View.VISIBLE);
        }
    }
}
