package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class ProcessJsonObj {
    public String type = "process";
    public int time;
    public ProcessData data;

    public ProcessJsonObj(String type, int time, ProcessData data) {
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public ProcessData getData() {
        return data;
    }

    public void setData(ProcessData data) {
        this.data = data;
    }

}
