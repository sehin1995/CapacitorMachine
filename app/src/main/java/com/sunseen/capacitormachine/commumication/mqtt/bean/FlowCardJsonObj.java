package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class FlowCardJsonObj {
    public String type = "flowCard";
    public int time;

    public FlowCardData data;

    public FlowCardJsonObj() {
        data = new FlowCardData();
    }

    public FlowCardJsonObj(String type, int time, FlowCardData data) {
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public FlowCardData getData() {
        return data;
    }

    public void setData(FlowCardData data) {
        this.data = data;
    }


}
