package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class CapacitorLevelJsonObj {
    public final String type = "level";
    public int time;
    public CapacitorLevelData data;


    public CapacitorLevelJsonObj(String type, int time, CapacitorLevelData data) {
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public CapacitorLevelData getData() {
        return data;
    }

    public void setData(CapacitorLevelData data) {
        this.data = data;
    }
}
