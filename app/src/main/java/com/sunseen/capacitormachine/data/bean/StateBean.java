package com.sunseen.capacitormachine.data.bean;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

/**
 * 记录当前机器使用的各个索引的数值
 */
@Entity
public class StateBean {
    @Id
    public long id;

    /**
     * 当前机器的生产状态
     * */
//    private int machineStaus;

    /**
     * 当前使用的夹具索引号的数值
     */
    private int holderIndex;
    /**
     * 当前链条使用的索引号的数值
     */
    private int chainIndex;
    /**
     * 当前的链条上的夹具的偏移量( <= 21)
     */
    private int offset;

    private boolean headChainHolderRemoved = true;

    public StateBean() {
    }

    public StateBean(int holderIndex, int chainIndex, int offset, boolean headChainHolderRemoved) {
        this.holderIndex = holderIndex;
        this.chainIndex = chainIndex;
        this.offset = offset;
        this.headChainHolderRemoved = headChainHolderRemoved;
    }

    public int getHolderIndex() {
        return holderIndex;
    }

    public int getChainIndex() {
        return chainIndex;
    }

    public int getOffset() {
        return offset;
    }

    public void setHolderIndex(int holderIndex) {
        this.holderIndex = holderIndex;
    }

    public void setChainIndex(int chainIndex) {
        this.chainIndex = chainIndex;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public boolean isHeadChainHolderRemoved() {
        return headChainHolderRemoved;
    }

    public void setHeadChainHolderRemoved(boolean headChainHolderRemoved) {
        this.headChainHolderRemoved = headChainHolderRemoved;
    }

    public void update(int holderIndex, int chainIndex, int offset) {
        this.holderIndex = holderIndex;
        this.chainIndex = chainIndex;
        this.offset = offset;
    }
}
