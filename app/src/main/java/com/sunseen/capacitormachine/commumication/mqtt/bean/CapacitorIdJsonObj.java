package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class CapacitorIdJsonObj {
    public final String type = "uid";
    public int time;
    public CapacitorIdData data;

    public CapacitorIdJsonObj(String type, int time, CapacitorIdData data) {
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public CapacitorIdData getData() {
        return data;
    }

    public void setData(CapacitorIdData data) {
        this.data = data;
    }
}
