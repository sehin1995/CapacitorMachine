package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

import java.util.List;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class CompleteUidJsonObj {
        public final String type="completeUidList";
        public long time;
        public List<String> data;

        public CompleteUidJsonObj(String type,long time, List<String> data) {
                this.time = time;
                this.data = data;
        }

        public String getType() {
                return type;
        }

        public long getTime() {
                return time;
        }

        public void setTime(long time) {
                this.time = time;
        }

        public List<String> getData() {
                return data;
        }

        public void setData(List<String> data) {
                this.data = data;
        }
}
