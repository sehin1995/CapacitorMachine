package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class ResultData {
    public String uid;
    public int result;

    public ResultData(String uid,  int result) {
        this.uid = uid;
        this.result = result;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
