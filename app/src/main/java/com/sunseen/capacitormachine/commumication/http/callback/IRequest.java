package com.sunseen.capacitormachine.commumication.http.callback;

public interface IRequest {

    void onRequestStart();

    void onRequestEnd();
}
