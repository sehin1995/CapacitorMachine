package com.sunseen.capacitormachine;

import android.app.Application;

import com.orhanobut.logger.CsvFormatStrategy;
import com.orhanobut.logger.DiskLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.sunseen.capacitormachine.common.CrashHandler;
import com.sunseen.capacitormachine.commumication.serialport.bean.ChangeBatchBean;
import com.sunseen.capacitormachine.data.DataUtil;
import com.sunseen.capacitormachine.data.ObjectBox;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zest
 */
public class CapacitorApp extends Application {

    private static CapacitorApp application;
    private static List<ChangeBatchBean> list;

    public static List<ChangeBatchBean> getList() {
        return list;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        CrashHandler.getInstance().init(this);
        ObjectBox.init(this);
        DataUtil.initData();
        EventBus.builder().addIndex(new MyEventBusIndex()).installDefaultEventBus();
        FormatStrategy formatStrategy = CsvFormatStrategy.newBuilder().tag("logger").build();
        Logger.addLogAdapter(new DiskLogAdapter(formatStrategy));
        list = new ArrayList<>();
    }

    public static CapacitorApp getApplication() {
        return application;
    }
}
