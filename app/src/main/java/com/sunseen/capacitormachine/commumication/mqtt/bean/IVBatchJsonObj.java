package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

import java.util.List;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class IVBatchJsonObj {
    public String type = "ivBatch";
    public long time;
    public List<IVData> data;

    public IVBatchJsonObj(String type, long time, List<IVData> data) {
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public List<IVData> getData() {
        return data;
    }

    public void setData(List<IVData> data) {
        this.data = data;
    }
}
