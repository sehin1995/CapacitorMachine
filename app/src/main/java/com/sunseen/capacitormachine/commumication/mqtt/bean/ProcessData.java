package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class ProcessData {
    public String batchId;
    public int V1;
    public int I1;
    public int V2;
    public int I2;
    public int V3;
    public int I3;
    public int V4;
    public int I4;
    public int V5;
    public int I5;
    public int V6;
    public int I6;
    public int V7;
    public int I7;
    public int V8;
    public int I8;
    public int V9;
    public int I9;
    public int V10;
    public int I10;
    public int V11;
    public int I11;
    public int V12;
    public int I12;
    public int V13;
    public int I13;
    public int V14;
    public int I14;
    public int V15;
    public int I15;
    public int V16;
    public int I16;
    public int V17;
    public int I17;
    public int V18;
    public int I18;
    public int chargeV;
    public int surgeV;
    public int testV;
    public int shortCircuitV;
    public int openCircuitV;
    public int leakageUpperLimit;
    public int leakageLowerLimit;
    public float capacitorNominal;
    public float premiumLimit;
    public float superiorLowerLimit;
    public float goodProductUpperLimit;
    public float goodProductLowerLimit;
    public int unAgedVoltage;
    public int lossAngelUpperLimit;
    public int upperImpedanceLimit;
    public float goodProduct2UpperLimit;
    public float goodProduct2LowerLimit;
    public int agingTime;
    public String codingMark;
    public String codingStartNumber;
    public int surgeUpperLimit;
    public int upperTemperatureLimit;
    public int instrumentFrequency;
    public int ovenTemperature;
    public int implosionTime;
    public int implosionVoltageUpperLimit;
    public int implosionVoltageLowerLimit;
    public int implosionCurrentUpperLimit;
    public int implosionCurrentLowerLimit;

    public ProcessData() {
    }

    public ProcessData(String batchId, int V1, int I1, int V2, int I2, int V3, int I3,
                       int V4, int I4, int V5, int I5, int V6, int I6, int V7, int I7, int V8, int I8,
                       int V9, int I9, int V10, int I10, int V11, int I11, int V12, int I12, int V13,
                       int I13, int V14, int I14, int V15, int I15, int V16, int I16, int V17, int I17,
                       int V18, int I18, int chargeV, int surgeV, int testV, int shortCircuitV,
                       int openCircuitV, int leakageUpperLimit, int leakageLowerLimit,
                       float capacitorNominal, float premiumLimit, float superiorLowerLimit,
                       float goodProductUpperLimit, float goodProductLowerLimit, int unAgedVoltage,
                       int lossAngelUpperLimit, int upperImpedanceLimit, float goodProduct2UpperLimit,
                       float goodProduct2LowerLimit, int agingTime, String codingMark,
                       String codingStartNumber, int surgeUpperLimit, int upperTemperatureLimit,
                       int instrumentFrequency, int ovenTemperature, int implosionTime,
                       int implosionVoltageUpperLimit, int implosionVoltageLowerLimit,
                       int implosionCurrentUpperLimit, int implosionCurrentLowerLimit) {
        this.batchId = batchId;
        this.V1 = V1;
        this.I1 = I1;
        this.V2 = V2;
        this.I2 = I2;
        this.V3 = V3;
        this.I3 = I3;
        this.V4 = V4;
        this.I4 = I4;
        this.V5 = V5;
        this.I5 = I5;
        this.V6 = V6;
        this.I6 = I6;
        this.V7 = V7;
        this.I7 = I7;
        this.V8 = V8;
        this.I8 = I8;
        this.V9 = V9;
        this.I9 = I9;
        this.V10 = V10;
        this.I10 = I10;
        this.V11 = V11;
        this.I11 = I11;
        this.V12 = V12;
        this.I12 = I12;
        this.V13 = V13;
        this.I13 = I13;
        this.V14 = V14;
        this.I14 = I14;
        this.V15 = V15;
        this.I15 = I15;
        this.V16 = V16;
        this.I16 = I16;
        this.V17 = V17;
        this.I17 = I17;
        this.V18 = V18;
        this.I18 = I18;
        this.chargeV = chargeV;
        this.surgeV = surgeV;
        this.testV = testV;
        this.shortCircuitV = shortCircuitV;
        this.openCircuitV = openCircuitV;
        this.leakageUpperLimit = leakageUpperLimit;
        this.leakageLowerLimit = leakageLowerLimit;
        this.capacitorNominal = capacitorNominal;
        this.premiumLimit = premiumLimit;
        this.superiorLowerLimit = superiorLowerLimit;
        this.goodProductUpperLimit = goodProductUpperLimit;
        this.goodProductLowerLimit = goodProductLowerLimit;
        this.unAgedVoltage = unAgedVoltage;
        this.lossAngelUpperLimit = lossAngelUpperLimit;
        this.upperImpedanceLimit = upperImpedanceLimit;
        this.goodProduct2UpperLimit = goodProduct2UpperLimit;
        this.goodProduct2LowerLimit = goodProduct2LowerLimit;
        this.agingTime = agingTime;
        this.codingMark = codingMark;
        this.codingStartNumber = codingStartNumber;
        this.surgeUpperLimit = surgeUpperLimit;
        this.upperTemperatureLimit = upperTemperatureLimit;
        this.instrumentFrequency = instrumentFrequency;
        this.ovenTemperature = ovenTemperature;
        this.implosionTime = implosionTime;
        this.implosionVoltageUpperLimit = implosionVoltageUpperLimit;
        this.implosionVoltageLowerLimit = implosionVoltageLowerLimit;
        this.implosionCurrentUpperLimit = implosionCurrentUpperLimit;
        this.implosionCurrentLowerLimit = implosionCurrentLowerLimit;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public int getV1() {
        return V1;
    }

    public void setV1(int v1) {
        V1 = v1;
    }

    public int getI1() {
        return I1;
    }

    public void setI1(int i1) {
        I1 = i1;
    }

    public int getV2() {
        return V2;
    }

    public void setV2(int v2) {
        V2 = v2;
    }

    public int getI2() {
        return I2;
    }

    public void setI2(int i2) {
        I2 = i2;
    }

    public int getV3() {
        return V3;
    }

    public void setV3(int v3) {
        V3 = v3;
    }

    public int getI3() {
        return I3;
    }

    public void setI3(int i3) {
        I3 = i3;
    }

    public int getV4() {
        return V4;
    }

    public void setV4(int v4) {
        V4 = v4;
    }

    public int getI4() {
        return I4;
    }

    public void setI4(int i4) {
        I4 = i4;
    }

    public int getV5() {
        return V5;
    }

    public void setV5(int v5) {
        V5 = v5;
    }

    public int getI5() {
        return I5;
    }

    public void setI5(int i5) {
        I5 = i5;
    }

    public int getV6() {
        return V6;
    }

    public void setV6(int v6) {
        V6 = v6;
    }

    public int getI6() {
        return I6;
    }

    public void setI6(int i6) {
        I6 = i6;
    }

    public int getV7() {
        return V7;
    }

    public void setV7(int v7) {
        V7 = v7;
    }

    public int getI7() {
        return I7;
    }

    public void setI7(int i7) {
        I7 = i7;
    }

    public int getV8() {
        return V8;
    }

    public void setV8(int v8) {
        V8 = v8;
    }

    public int getI8() {
        return I8;
    }

    public void setI8(int i8) {
        I8 = i8;
    }

    public int getV9() {
        return V9;
    }

    public void setV9(int v9) {
        V9 = v9;
    }

    public int getI9() {
        return I9;
    }

    public void setI9(int i9) {
        I9 = i9;
    }

    public int getV10() {
        return V10;
    }

    public void setV10(int v10) {
        V10 = v10;
    }

    public int getI10() {
        return I10;
    }

    public void setI10(int i10) {
        I10 = i10;
    }

    public int getV11() {
        return V11;
    }

    public void setV11(int v11) {
        V11 = v11;
    }

    public int getI11() {
        return I11;
    }

    public void setI11(int i11) {
        I11 = i11;
    }

    public int getV12() {
        return V12;
    }

    public void setV12(int v12) {
        V12 = v12;
    }

    public int getI12() {
        return I12;
    }

    public void setI12(int i12) {
        I12 = i12;
    }

    public int getV13() {
        return V13;
    }

    public void setV13(int v13) {
        V13 = v13;
    }

    public int getI13() {
        return I13;
    }

    public void setI13(int i13) {
        I13 = i13;
    }

    public int getV14() {
        return V14;
    }

    public void setV14(int v14) {
        V14 = v14;
    }

    public int getI14() {
        return I14;
    }

    public void setI14(int i14) {
        I14 = i14;
    }

    public int getV15() {
        return V15;
    }

    public void setV15(int v15) {
        V15 = v15;
    }

    public int getI15() {
        return I15;
    }

    public void setI15(int i15) {
        I15 = i15;
    }

    public int getV16() {
        return V16;
    }

    public void setV16(int v16) {
        V16 = v16;
    }

    public int getI16() {
        return I16;
    }

    public void setI16(int i16) {
        I16 = i16;
    }

    public int getV17() {
        return V17;
    }

    public void setV17(int v17) {
        V17 = v17;
    }

    public int getI17() {
        return I17;
    }

    public void setI17(int i17) {
        I17 = i17;
    }

    public int getV18() {
        return V18;
    }

    public void setV18(int v18) {
        V18 = v18;
    }

    public int getI18() {
        return I18;
    }

    public void setI18(int i18) {
        I18 = i18;
    }

    public int getChargeV() {
        return chargeV;
    }

    public void setChargeV(int chargeV) {
        this.chargeV = chargeV;
    }

    public int getSurgeV() {
        return surgeV;
    }

    public void setSurgeV(int surgeV) {
        this.surgeV = surgeV;
    }

    public int getTestV() {
        return testV;
    }

    public void setTestV(int testV) {
        this.testV = testV;
    }

    public int getShortCircuitV() {
        return shortCircuitV;
    }

    public void setShortCircuitV(int shortCircuitV) {
        this.shortCircuitV = shortCircuitV;
    }

    public int getOpenCircuitV() {
        return openCircuitV;
    }

    public void setOpenCircuitV(int openCircuitV) {
        this.openCircuitV = openCircuitV;
    }

    public int getLeakageUpperLimit() {
        return leakageUpperLimit;
    }

    public void setLeakageUpperLimit(int leakageUpperLimit) {
        this.leakageUpperLimit = leakageUpperLimit;
    }

    public int getLeakageLowerLimit() {
        return leakageLowerLimit;
    }

    public void setLeakageLowerLimit(int leakageLowerLimit) {
        this.leakageLowerLimit = leakageLowerLimit;
    }

    public float getPremiumLimit() {
        return premiumLimit;
    }

    public void setPremiumLimit(float premiumLimit) {
        this.premiumLimit = premiumLimit;
    }

    public float getSuperiorLowerLimit() {
        return superiorLowerLimit;
    }

    public void setSuperiorLowerLimit(float superiorLowerLimit) {
        this.superiorLowerLimit = superiorLowerLimit;
    }

    public int getUnAgedVoltage() {
        return unAgedVoltage;
    }

    public void setUnAgedVoltage(int unAgedVoltage) {
        this.unAgedVoltage = unAgedVoltage;
    }

    public int getLossAngelUpperLimit() {
        return lossAngelUpperLimit;
    }

    public void setLossAngelUpperLimit(int lossAngelUpperLimit) {
        this.lossAngelUpperLimit = lossAngelUpperLimit;
    }

    public int getUpperImpedanceLimit() {
        return upperImpedanceLimit;
    }

    public void setUpperImpedanceLimit(int upperImpedanceLimit) {
        this.upperImpedanceLimit = upperImpedanceLimit;
    }

    public int getAgingTime() {
        return agingTime;
    }

    public void setAgingTime(int agingTime) {
        this.agingTime = agingTime;
    }

    public String getCodingMark() {
        return codingMark;
    }

    public void setCodingMark(String codingMark) {
        this.codingMark = codingMark;
    }

    public String getCodingStartNumber() {
        return codingStartNumber;
    }

    public void setCodingStartNumber(String codingStartNumber) {
        this.codingStartNumber = codingStartNumber;
    }

    public int getSurgeUpperLimit() {
        return surgeUpperLimit;
    }

    public void setSurgeUpperLimit(int surgeUpperLimit) {
        this.surgeUpperLimit = surgeUpperLimit;
    }

    public int getUpperTemperatureLimit() {
        return upperTemperatureLimit;
    }

    public void setUpperTemperatureLimit(int upperTemperatureLimit) {
        this.upperTemperatureLimit = upperTemperatureLimit;
    }

    public float getCapacitorNominal() {
        return capacitorNominal;
    }

    public void setCapacitorNominal(float capacitorNominal) {
        this.capacitorNominal = capacitorNominal;
    }

    public float getGoodProductUpperLimit() {
        return goodProductUpperLimit;
    }

    public void setGoodProductUpperLimit(float goodProductUpperLimit) {
        this.goodProductUpperLimit = goodProductUpperLimit;
    }

    public float getGoodProductLowerLimit() {
        return goodProductLowerLimit;
    }

    public void setGoodProductLowerLimit(float goodProductLowerLimit) {
        this.goodProductLowerLimit = goodProductLowerLimit;
    }

    public float getGoodProduct2UpperLimit() {
        return goodProduct2UpperLimit;
    }

    public void setGoodProduct2UpperLimit(float goodProduct2UpperLimit) {
        this.goodProduct2UpperLimit = goodProduct2UpperLimit;
    }

    public float getGoodProduct2LowerLimit() {
        return goodProduct2LowerLimit;
    }

    public void setGoodProduct2LowerLimit(float goodProduct2LowerLimit) {
        this.goodProduct2LowerLimit = goodProduct2LowerLimit;
    }

    public int getInstrumentFrequency() {
        return instrumentFrequency;
    }

    public void setInstrumentFrequency(int instrumentFrequency) {
        this.instrumentFrequency = instrumentFrequency;
    }

    public int getOvenTemperature() {
        return ovenTemperature;
    }

    public void setOvenTemperature(int ovenTemperature) {
        this.ovenTemperature = ovenTemperature;
    }

    public int getImplosionTime() {
        return implosionTime;
    }

    public void setImplosionTime(int implosionTime) {
        this.implosionTime = implosionTime;
    }

    public int getImplosionVoltageUpperLimit() {
        return implosionVoltageUpperLimit;
    }

    public void setImplosionVoltageUpperLimit(int implosionVoltageUpperLimit) {
        this.implosionVoltageUpperLimit = implosionVoltageUpperLimit;
    }

    public int getImplosionVoltageLowerLimit() {
        return implosionVoltageLowerLimit;
    }

    public void setImplosionVoltageLowerLimit(int implosionVoltageLowerLimit) {
        this.implosionVoltageLowerLimit = implosionVoltageLowerLimit;
    }

    public int getImplosionCurrentUpperLimit() {
        return implosionCurrentUpperLimit;
    }

    public void setImplosionCurrentUpperLimit(int implosionCurrentUpperLimit) {
        this.implosionCurrentUpperLimit = implosionCurrentUpperLimit;
    }

    public int getImplosionCurrentLowerLimit() {
        return implosionCurrentLowerLimit;
    }

    public void setImplosionCurrentLowerLimit(int implosionCurrentLowerLimit) {
        this.implosionCurrentLowerLimit = implosionCurrentLowerLimit;
    }
}
