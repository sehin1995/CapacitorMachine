package com.sunseen.capacitormachine.modules.home.event;

import com.github.mikephil.charting.data.Entry;

import java.util.List;

public class QueryVIResult {
    private int sequence;
    private List<Entry> vList;
    private List<Entry> iList;

    public QueryVIResult(int sequence, List<Entry> vList, List<Entry> iList) {
        this.sequence = sequence;
        this.vList = vList;
        this.iList = iList;

    }

    public List<Entry> getvList() {
        return vList;
    }

    public List<Entry> getiList() {
        return iList;
    }

    public int getSequence() {
        return sequence;
    }
}
