
package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class DetectionResult {
    public static final String CONVEX = "convex";
    public static final String IMPLOSION = "implosion";
    public static final String SURGE1 = "surge1";
    public static final String SURGE2 = "surge2";

    public String type;
    public int time;
    public ResultData data;

    public DetectionResult(String type, int time, ResultData data) {
        this.type = type;
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public ResultData getData() {
        return data;
    }

    public void setData(ResultData data) {
        this.data = data;
    }

}
