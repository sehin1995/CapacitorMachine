package com.sunseen.capacitormachine.commumication.tcp.event;

import android.util.SparseIntArray;

/**
 * 红外检测板掉线事件
 */
public class NodeOffLineEvent {

    private int portId;

    private boolean offLine;

    private SparseIntArray nodeArray;

    public NodeOffLineEvent(int portId, boolean offLine, SparseIntArray nodeArray) {
        this.portId = portId;
        this.offLine = offLine;
        this.nodeArray = nodeArray;
    }

    public NodeOffLineEvent(int portId, boolean offLine) {
        this.portId = portId;
        this.offLine = offLine;
    }

    public int getPortId() {
        return portId;
    }

    public boolean isOffLine() {
        return offLine;
    }

    public SparseIntArray getNodeArray() {
        return nodeArray;
    }
}
