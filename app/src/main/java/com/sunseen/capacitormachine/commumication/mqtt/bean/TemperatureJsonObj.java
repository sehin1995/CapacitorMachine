package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class TemperatureJsonObj {
    public String type = "temperature";
    public int time;
    public TemperatureData data;

    public TemperatureJsonObj(String type, int time, TemperatureData data) {
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public TemperatureData getData() {
        return data;
    }

    public void setData(TemperatureData data) {
        this.data = data;
    }
}
