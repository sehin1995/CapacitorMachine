package com.sunseen.capacitormachine.data.bean;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class DetectionFlagBean {
    @Id
    public long id;

    private boolean shortCircuitEnable = true;
    private boolean openCircuitEnable = true;
    private boolean unAgeEnable = true;
    private boolean leakCurrentEnable = true;
    private boolean capacityEnable = true;
    private boolean lossAngleEnable = true;
    private boolean esrEnable = true;
    private boolean surgeEnable = true;
    private boolean implosionEnable = true;
    private boolean powerSupplyAlarmEnable = true;
    private boolean temperatureAlarmEnable = true;
    private boolean camera1Enable = true;
    private boolean camera2Enable = true;
    private boolean camera3Enable = true;
    private boolean sprayCodeEnable = true;
    private boolean autoModeEnable = true;

    public boolean isShortCircuitEnable() {
        return shortCircuitEnable;
    }

    public void setShortCircuitEnable(boolean shortCircuitEnable) {
        this.shortCircuitEnable = shortCircuitEnable;
    }

    public boolean isOpenCircuitEnable() {
        return openCircuitEnable;
    }

    public void setOpenCircuitEnable(boolean openCircuitEnable) {
        this.openCircuitEnable = openCircuitEnable;
    }

    public boolean isUnAgeEnable() {
        return unAgeEnable;
    }

    public void setUnAgeEnable(boolean unAgeEnable) {
        this.unAgeEnable = unAgeEnable;
    }

    public boolean isLeakCurrentEnable() {
        return leakCurrentEnable;
    }

    public void setLeakCurrentEnable(boolean leakCurrentEnable) {
        this.leakCurrentEnable = leakCurrentEnable;
    }

    public boolean isCapacityEnable() {
        return capacityEnable;
    }

    public void setCapacityEnable(boolean capacityEnable) {
        this.capacityEnable = capacityEnable;
    }

    public boolean isLossAngleEnable() {
        return lossAngleEnable;
    }

    public void setLossAngleEnable(boolean lossAngleEnable) {
        this.lossAngleEnable = lossAngleEnable;
    }

    public boolean isEsrEnable() {
        return esrEnable;
    }

    public void setEsrEnable(boolean esrEnable) {
        this.esrEnable = esrEnable;
    }

    public boolean isSurgeEnable() {
        return surgeEnable;
    }

    public void setSurgeEnable(boolean surgeEnable) {
        this.surgeEnable = surgeEnable;
    }

    public boolean isImplosionEnable() {
        return implosionEnable;
    }

    public void setImplosionEnable(boolean implosionEnable) {
        this.implosionEnable = implosionEnable;
    }

    public boolean isPowerSupplyAlarmEnable() {
        return powerSupplyAlarmEnable;
    }

    public void setPowerSupplyAlarmEnable(boolean powerSupplyAlarmEnable) {
        this.powerSupplyAlarmEnable = powerSupplyAlarmEnable;
    }

    public boolean isTemperatureAlarmEnable() {
        return temperatureAlarmEnable;
    }

    public void setTemperatureAlarmEnable(boolean temperatureAlarmEnable) {
        this.temperatureAlarmEnable = temperatureAlarmEnable;
    }

    public boolean isCamera1Enable() {
        return camera1Enable;
    }

    public void setCamera1Enable(boolean camera1Enable) {
        this.camera1Enable = camera1Enable;
    }

    public boolean isCamera2Enable() {
        return camera2Enable;
    }

    public void setCamera2Enable(boolean camera2Enable) {
        this.camera2Enable = camera2Enable;
    }

    public boolean isCamera3Enable() {
        return camera3Enable;
    }

    public void setCamera3Enable(boolean camera3Enable) {
        this.camera3Enable = camera3Enable;
    }

    public boolean isSprayCodeEnable() {
        return sprayCodeEnable;
    }

    public void setSprayCodeEnable(boolean sprayCodeEnable) {
        this.sprayCodeEnable = sprayCodeEnable;
    }

    public boolean isAutoModeEnable() {
        return autoModeEnable;
    }

    public void setAutoModeEnable(boolean autoModeEnable) {
        this.autoModeEnable = autoModeEnable;
    }
}
