package com.sunseen.capacitormachine.modules.query;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.commumication.http.HttpUtil;
import com.sunseen.capacitormachine.commumication.http.RestClient;
import com.sunseen.capacitormachine.databinding.FragmentUidListBinding;
import com.sunseen.capacitormachine.modules.query.adapter.ResultListAdapter;
import com.sunseen.capacitormachine.modules.query.bean.CapacitorInfoBean;
import com.sunseen.capacitormachine.modules.query.bean.CapacitorUidBean;

import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

public class UidListFragment extends BaseFragment {

    private List<CapacitorUidBean> capacitorUidBeanList;

    public void setList(List<CapacitorUidBean> capacitorInfoList) {
        this.capacitorUidBeanList = capacitorInfoList;
    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_uid_list;
    }

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        final FragmentUidListBinding binding = (FragmentUidListBinding) viewDataBinding;
        binding.toolBar.setNavigationOnClickListener(v -> pop());
        ResultListAdapter adapter = new ResultListAdapter(getContext(), capacitorUidBeanList);
        //binding.rvQueryResult.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvQueryResult.setAdapter(adapter);
        binding.rvQueryResult.setOnItemClickListener((parent, view, position, id) -> {
            CapacitorUidBean bean = capacitorUidBeanList.get(position);
            if (querying) {
                Toast.makeText(getContext(), R.string.querying_tip, Toast.LENGTH_SHORT).show();
            } else {
                querying = true;
                queryCapacitorInfo(bean.getUid());
            }
        });
//        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                CapacitorUidBean bean = (CapacitorUidBean) adapter.getData().get(position);
//                if (querying) {
//                    Toast.makeText(getContext(), R.string.querying_tip, Toast.LENGTH_SHORT).show();
//                } else {
//                    querying = true;
//                    queryCapacitorInfo(bean.getUid());
//                }
//            }
//        });
    }

    private boolean querying = false;

    private void queryCapacitorInfo(String uid) {
        RestClient
                .builder()
                .url(HttpUtil.CapacitorInfo)
                .params("uid", uid)
                .success(response -> {
                    JSONObject rootObj = JSON.parseObject(response);
                    if (rootObj.getInteger("status") == 1) {
                        Log.e("test", "status == 1");
                        CapacitorInfoBean capacitorInfo = JSON.parseObject(rootObj.getString("data"), CapacitorInfoBean.class);
                        if (capacitorInfo != null) {
                            Log.e("test", "capacitorInfo != null");
                            if (!hidden) {
                                QueryDetailFragment detailFragment = new QueryDetailFragment();
                                detailFragment.setCapacitorInfo(capacitorInfo);
                                start(detailFragment);
                            }
                        } else {
                            Toast.makeText(_mActivity, "无查询结果", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(_mActivity, "查询失败", Toast.LENGTH_SHORT).show();
                    }
                    querying = false;
                })
                .error(((code, msg) -> {
                    querying = false;
                    Toast.makeText(_mActivity, "查询电容详情发生错误", Toast.LENGTH_SHORT).show();
                }))
                .failure(() -> {
                    querying = false;
                    Toast.makeText(_mActivity, "查询电容详情失败", Toast.LENGTH_SHORT).show();
                })
                .build()
                .post();
    }

    private volatile boolean hidden = false;

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        this.hidden = hidden;
        Log.e("test", "hidden = " + hidden);
    }
}
