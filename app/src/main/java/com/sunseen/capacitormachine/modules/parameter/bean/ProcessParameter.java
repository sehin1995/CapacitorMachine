package com.sunseen.capacitormachine.modules.parameter.bean;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Unique;

/**
 * @author zest
 * 数据类
 * 记录单个批次号的所有工艺参数
 */
@Entity
public class ProcessParameter {
    @Id
    long id;

    /**
     * 批号
     */
    @Unique
    private String batchId;

    /**
     * 生产序列号
     * 生产顺序将按照此序列号从小到大进行排序生产
     */
    private long produceSequence;

    /**
     * 生产状态
     * 0 未生产
     * 1 已生产
     * 2 生产中
     */
    private int state = 0;

    /**
     * 21个电源的电压和电流
     * 电压单位为0.1V，电流单位为0.01A
     */
    private int powerV1 = -1;
    private int powerI1 = -1;
    private int powerV2 = -1;
    private int powerI2 = -1;
    private int powerV3 = -1;
    private int powerI3 = -1;
    private int powerV4 = -1;
    private int powerI4 = -1;
    private int powerV5 = -1;
    private int powerI5 = -1;
    private int powerV6 = -1;
    private int powerI6 = -1;
    private int powerV7 = -1;
    private int powerI7 = -1;
    private int powerV8 = -1;
    private int powerI8 = -1;
    private int powerV9 = -1;
    private int powerI9 = -1;
    private int powerV10 = -1;
    private int powerI10 = -1;
    private int powerV11 = -1;
    private int powerI11 = -1;
    private int powerV12 = -1;
    private int powerI12 = -1;
    private int powerV13 = -1;
    private int powerI13 = -1;
    private int powerV14 = -1;
    private int powerI14 = -1;
    private int powerV15 = -1;
    private int powerI15 = -1;
    private int powerV16 = -1;
    private int powerI16 = -1;
    private int powerV17 = -1;
    private int powerI17 = -1;
    private int powerV18 = -1;
    private int powerI18 = -1;

    /**
     * 冲电电压,电流
     */
    private int chargeV = -1;
    private int chargeI = -1;
    /**
     * 浪涌电压,电流
     */
    private int surgeV = -1;
    private int surgeI = -1;
    /**
     * 测试电压,电流
     */
    private int testV = -1;
    private int testI = -1;

    /**
     * 检测开关标志位
     */
    private byte enableFlag1 = 0;
    private byte enableFlag2 = 0;

    /**
     * 短路电压
     * 单位0.1V
     */
    private int shortCircuitV = -1;

    /**
     * 开路电压
     * 单位0.1V
     */
    private int openCircuitV = -1;
    /**
     * 漏电上限
     * 单位0.1ua
     */
    private int leakageUpperLimit = -1;


    /**
     * 漏电中限
     * 单位0.1μa
     */
    private int leakageMiddleLimit = -1;


    /**
     * 漏电下限
     * 单位0.1ua
     */
    private int leakageLowerLimit = -1;

    /**
     * 电容标称 单位0.1uf
     */
    private float capacitorNominal = -1;

    /**
     * 优品上限 单位0.1uf
     */
    private float premiumLimit = -1;

    /**
     * 优品下限 单位0.1uf
     */
    private float superiorLowerLimit = -1;

    /**
     * 良品上限 单位0.1uf
     */
    private float goodProductUpperLimit = -1;

    /**
     * 良品下限 单位1uf
     */
    private float goodProductLowerLimit = -1;

    /**
     * 未老化上限 单位0.1V
     */
    private int unAgedVoltage = -1;

    /**
     * 损失上限 单位0.1%
     */
    private int lossAngelUpperLimit = -1;

    /**
     * 阻抗上限 单位0.1毫欧
     */
    private int upperImpedanceLimit = -1;

    /**
     * 良品2上限 单位0.1uf
     */
    private float goodProduct2UpperLimit = -1;

    /**
     * 良品2下限 单位0.1uf
     */
    private float goodProduct2LowerLimit = -1;

    /**
     * 老化时间 单位1分钟
     */
    private int agingTime = -1;

    /**
     * 喷码标识 NO123456
     */
    private String codingMark;

    /**
     * 喷码起始编号 0
     */
    private String codingStartNumber;

    /**
     * 浪涌上限 单位0.1V
     */
    private int surgeUpperLimit = -1;

    /**
     * 温度上限 单位0.1℃
     */
    private int upperTemperatureLimit = -1;

    /**
     * 仪表频率 单位1Hz
     * 智胜新设定仪表频率为120Hz
     */
    private int instrumentFrequency = 120;

    /**
     * 烤箱温度 单位0.1℃
     */
    private int ovenTemperature = -1;

    /**
     * 内爆时间 单位0.01ms
     */
    private int implosionTime = -1;

    /**
     * 内爆电压上限 单位0.1v
     */
    private int implosionVoltageUpperLimit = -1;

    /**
     * 内爆电压下限 单位0.1v
     */
    private int implosionVoltageLowerLimit = -1;

    /**
     * 内爆电流上限 单位1ua
     */
    private int implosionCurrentUpperLimit = -1;

    /**
     * 内爆电流下限 单位1ua
     */
    private int implosionCurrentLowerLimit = -1;

    /**
     * 进料中转台高度　
     */
    private int feedInTableHeight = -1;

    /**
     * 进料旋转角度
     */
    private int feedInAngle = -1;

    /**
     * 出料中转台
     */
    private int dischargeTableHeight = -1;

    /**
     * 出料旋转角度
     */
    private int dischargeAngle;

    public ProcessParameter() {
        feedInAngle = -600 + 1800;
        feedInTableHeight = 801;
        dischargeAngle = 265 + 1800;
        dischargeTableHeight = 119;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public long getProduceSequence() {
        return produceSequence;
    }

    public void setProduceSequence(long produceSequence) {
        this.produceSequence = produceSequence;
    }

    public int getPowerV1() {
        return powerV1;
    }

    public void setPowerV1(int powerV1) {
        this.powerV1 = powerV1;
    }

    public int getPowerI1() {
        return powerI1;
    }

    public void setPowerI1(int powerI1) {
        this.powerI1 = powerI1;
    }

    public int getPowerV2() {
        return powerV2;
    }

    public void setPowerV2(int powerV2) {
        this.powerV2 = powerV2;
    }

    public int getPowerI2() {
        return powerI2;
    }

    public void setPowerI2(int powerI2) {
        this.powerI2 = powerI2;
    }

    public int getPowerV3() {
        return powerV3;
    }

    public void setPowerV3(int powerV3) {
        this.powerV3 = powerV3;
    }

    public int getPowerI3() {
        return powerI3;
    }

    public void setPowerI3(int powerI3) {
        this.powerI3 = powerI3;
    }

    public int getPowerV4() {
        return powerV4;
    }

    public void setPowerV4(int powerV4) {
        this.powerV4 = powerV4;
    }

    public int getPowerI4() {
        return powerI4;
    }

    public void setPowerI4(int powerI4) {
        this.powerI4 = powerI4;
    }

    public int getPowerV5() {
        return powerV5;
    }

    public void setPowerV5(int powerV5) {
        this.powerV5 = powerV5;
    }

    public int getPowerI5() {
        return powerI5;
    }

    public void setPowerI5(int powerI5) {
        this.powerI5 = powerI5;
    }

    public int getPowerV6() {
        return powerV6;
    }

    public void setPowerV6(int powerV6) {
        this.powerV6 = powerV6;
    }

    public int getPowerI6() {
        return powerI6;
    }

    public void setPowerI6(int powerI6) {
        this.powerI6 = powerI6;
    }

    public int getPowerV7() {
        return powerV7;
    }

    public void setPowerV7(int powerV7) {
        this.powerV7 = powerV7;
    }

    public int getPowerI7() {
        return powerI7;
    }

    public void setPowerI7(int powerI7) {
        this.powerI7 = powerI7;
    }

    public int getPowerV8() {
        return powerV8;
    }

    public void setPowerV8(int powerV8) {
        this.powerV8 = powerV8;
    }

    public int getPowerI8() {
        return powerI8;
    }

    public void setPowerI8(int powerI8) {
        this.powerI8 = powerI8;
    }

    public int getPowerV9() {
        return powerV9;
    }

    public void setPowerV9(int powerV9) {
        this.powerV9 = powerV9;
    }

    public int getPowerI9() {
        return powerI9;
    }

    public void setPowerI9(int powerI9) {
        this.powerI9 = powerI9;
    }

    public int getPowerV10() {
        return powerV10;
    }

    public void setPowerV10(int powerV10) {
        this.powerV10 = powerV10;
    }

    public int getPowerI10() {
        return powerI10;
    }

    public void setPowerI10(int powerI10) {
        this.powerI10 = powerI10;
    }

    public int getPowerV11() {
        return powerV11;
    }

    public void setPowerV11(int powerV11) {
        this.powerV11 = powerV11;
    }

    public int getPowerI11() {
        return powerI11;
    }

    public void setPowerI11(int powerI11) {
        this.powerI11 = powerI11;
    }

    public int getPowerV12() {
        return powerV12;
    }

    public void setPowerV12(int powerV12) {
        this.powerV12 = powerV12;
    }

    public int getPowerI12() {
        return powerI12;
    }

    public void setPowerI12(int powerI12) {
        this.powerI12 = powerI12;
    }

    public int getPowerV13() {
        return powerV13;
    }

    public void setPowerV13(int powerV13) {
        this.powerV13 = powerV13;
    }

    public int getPowerI13() {
        return powerI13;
    }

    public void setPowerI13(int powerI13) {
        this.powerI13 = powerI13;
    }

    public int getPowerV14() {
        return powerV14;
    }

    public void setPowerV14(int powerV14) {
        this.powerV14 = powerV14;
    }

    public int getPowerI14() {
        return powerI14;
    }

    public void setPowerI14(int powerI14) {
        this.powerI14 = powerI14;
    }

    public int getPowerV15() {
        return powerV15;
    }

    public void setPowerV15(int powerV15) {
        this.powerV15 = powerV15;
    }

    public int getPowerI15() {
        return powerI15;
    }

    public void setPowerI15(int powerI15) {
        this.powerI15 = powerI15;
    }

    public int getPowerV16() {
        return powerV16;
    }

    public void setPowerV16(int powerV16) {
        this.powerV16 = powerV16;
    }

    public int getPowerI16() {
        return powerI16;
    }

    public void setPowerI16(int powerI16) {
        this.powerI16 = powerI16;
    }

    public int getPowerV17() {
        return powerV17;
    }

    public void setPowerV17(int powerV17) {
        this.powerV17 = powerV17;
    }

    public int getPowerI17() {
        return powerI17;
    }

    public void setPowerI17(int powerI17) {
        this.powerI17 = powerI17;
    }

    public int getPowerV18() {
        return powerV18;
    }

    public void setPowerV18(int powerV18) {
        this.powerV18 = powerV18;
    }

    public int getPowerI18() {
        return powerI18;
    }

    public void setPowerI18(int powerI18) {
        this.powerI18 = powerI18;
    }

    public int getChargeV() {
        return chargeV;
    }

    public void setChargeV(int chargeV) {
        this.chargeV = chargeV;
    }

    public int getChargeI() {
        return chargeI;
    }

    public void setChargeI(int chargeI) {
        this.chargeI = chargeI;
    }

    public int getSurgeI() {
        return surgeI;
    }

    public void setSurgeI(int surgeI) {
        this.surgeI = surgeI;
    }

    public int getTestI() {
        return testI;
    }

    public void setTestI(int testI) {
        this.testI = testI;
    }

    public byte getEnableFlag1() {
        return enableFlag1;
    }

    public void setEnableFlag1(byte enableFlag1) {
        this.enableFlag1 = enableFlag1;
    }

    public byte getEnableFlag2() {
        return enableFlag2;
    }

    public void setEnableFlag2(byte enableFlag2) {
        this.enableFlag2 = enableFlag2;
    }

    public int getSurgeV() {
        return surgeV;
    }

    public void setSurgeV(int surgeV) {
        this.surgeV = surgeV;
    }

    public int getTestV() {
        return testV;
    }

    public void setTestV(int testV) {
        this.testV = testV;
    }

    public int getShortCircuitV() {
        return shortCircuitV;
    }

    public void setShortCircuitV(int shortCircuitV) {
        this.shortCircuitV = shortCircuitV;
    }

    public int getOpenCircuitV() {
        return openCircuitV;
    }

    public void setOpenCircuitV(int openCircuitV) {
        this.openCircuitV = openCircuitV;
    }

    public int getLeakageUpperLimit() {
        return leakageUpperLimit;
    }

    public void setLeakageUpperLimit(int leakageUpperLimit) {
        this.leakageUpperLimit = leakageUpperLimit;
    }

    public int getLeakageMiddleLimit() {
        return leakageMiddleLimit;
    }

    public void setLeakageMiddleLimit(int leakageMiddleLimit) {
        this.leakageMiddleLimit = leakageMiddleLimit;
    }

    public int getLeakageLowerLimit() {
        return leakageLowerLimit;
    }

    public void setLeakageLowerLimit(int leakageLowerLimit) {
        this.leakageLowerLimit = leakageLowerLimit;
    }

    public float getCapacitorNominal() {
        return capacitorNominal;
    }

    public void setCapacitorNominal(float capacitorNominal) {
        this.capacitorNominal = capacitorNominal;
    }

    public float getPremiumLimit() {
        return premiumLimit;
    }

    public void setPremiumLimit(float premiumLimit) {
        this.premiumLimit = premiumLimit;
    }

    public float getSuperiorLowerLimit() {
        return superiorLowerLimit;
    }

    public void setSuperiorLowerLimit(float superiorLowerLimit) {
        this.superiorLowerLimit = superiorLowerLimit;
    }

    public float getGoodProductUpperLimit() {
        return goodProductUpperLimit;
    }

    public void setGoodProductUpperLimit(float goodProductUpperLimit) {
        this.goodProductUpperLimit = goodProductUpperLimit;
    }

    public float getGoodProductLowerLimit() {
        return goodProductLowerLimit;
    }

    public void setGoodProductLowerLimit(float goodProductLowerLimit) {
        this.goodProductLowerLimit = goodProductLowerLimit;
    }

    public int getUnAgedVoltage() {
        return unAgedVoltage;
    }

    public void setUnAgedVoltage(int unAgedVoltage) {
        this.unAgedVoltage = unAgedVoltage;
    }

    public int getLossAngelUpperLimit() {
        return lossAngelUpperLimit;
    }

    public void setLossAngelUpperLimit(int lossAngelUpperLimit) {
        this.lossAngelUpperLimit = lossAngelUpperLimit;
    }

    public int getUpperImpedanceLimit() {
        return upperImpedanceLimit;
    }

    public void setUpperImpedanceLimit(int upperImpedanceLimit) {
        this.upperImpedanceLimit = upperImpedanceLimit;
    }

    public float getGoodProduct2UpperLimit() {
        return goodProduct2UpperLimit;
    }

    public void setGoodProduct2UpperLimit(float goodProduct2UpperLimit) {
        this.goodProduct2UpperLimit = goodProduct2UpperLimit;
    }

    public float getGoodProduct2LowerLimit() {
        return goodProduct2LowerLimit;
    }

    public void setGoodProduct2LowerLimit(float goodProduct2LowerLimit) {
        this.goodProduct2LowerLimit = goodProduct2LowerLimit;
    }

    public int getAgingTime() {
        return agingTime;
    }

    public void setAgingTime(int agingTime) {
        this.agingTime = agingTime;
    }

    public String getCodingMark() {
        return codingMark;
    }

    public void setCodingMark(String codingMark) {
        this.codingMark = codingMark;
    }

    public String getCodingStartNumber() {
        return codingStartNumber;
    }

    public void setCodingStartNumber(String codingStartNumber) {
        this.codingStartNumber = codingStartNumber;
    }

    public int getSurgeUpperLimit() {
        return surgeUpperLimit;
    }

    public void setSurgeUpperLimit(int surgeUpperLimit) {
        this.surgeUpperLimit = surgeUpperLimit;
    }

    public int getUpperTemperatureLimit() {
        return upperTemperatureLimit;
    }

    public void setUpperTemperatureLimit(int upperTemperatureLimit) {
        this.upperTemperatureLimit = upperTemperatureLimit;
    }

    public int getInstrumentFrequency() {
        return instrumentFrequency;
    }

    public void setInstrumentFrequency(int instrumentFrequency) {
        this.instrumentFrequency = instrumentFrequency;
    }

    public int getOvenTemperature() {
        return ovenTemperature;
    }

    public void setOvenTemperature(int ovenTemperature) {
        this.ovenTemperature = ovenTemperature;
    }

    public int getImplosionTime() {
        return implosionTime;
    }

    public void setImplosionTime(int implosionTime) {
        this.implosionTime = implosionTime;
    }

    public int getImplosionVoltageUpperLimit() {
        return implosionVoltageUpperLimit;
    }

    public void setImplosionVoltageUpperLimit(int implosionVoltageUpperLimit) {
        this.implosionVoltageUpperLimit = implosionVoltageUpperLimit;
    }

    public int getImplosionVoltageLowerLimit() {
        return implosionVoltageLowerLimit;
    }

    public void setImplosionVoltageLowerLimit(int implosionVoltageLowerLimit) {
        this.implosionVoltageLowerLimit = implosionVoltageLowerLimit;
    }

    public int getImplosionCurrentUpperLimit() {
        return implosionCurrentUpperLimit;
    }

    public void setImplosionCurrentUpperLimit(int implosionCurrentUpperLimit) {
        this.implosionCurrentUpperLimit = implosionCurrentUpperLimit;
    }

    public int getImplosionCurrentLowerLimit() {
        return implosionCurrentLowerLimit;
    }

    public void setImplosionCurrentLowerLimit(int implosionCurrentLowerLimit) {
        this.implosionCurrentLowerLimit = implosionCurrentLowerLimit;
    }

    public int getFeedInTableHeight() {
        return feedInTableHeight;
    }

    public void setFeedInTableHeight(int feedInTableHeight) {
        this.feedInTableHeight = feedInTableHeight;
    }

    public int getFeedInAngle() {
        return feedInAngle;
    }

    public void setFeedInAngle(int feedInAngle) {
        this.feedInAngle = feedInAngle;
    }

    public int getDischargeTableHeight() {
        return dischargeTableHeight;
    }

    public void setDischargeTableHeight(int dischargeTableHeight) {
        this.dischargeTableHeight = dischargeTableHeight;
    }

    public int getDischargeAngle() {
        return dischargeAngle;
    }

    public void setDischargeAngle(int dischargeAngle) {
        this.dischargeAngle = dischargeAngle;
    }

    public boolean isSetAll() {
        return
                batchId != null
                        && batchId.length() > 0 && !batchId.contains(" ")
                        && powerV1 >= 0
                        && powerI1 >= 0
                        && powerV2 >= 0
                        && powerI2 >= 0
                        && powerV3 >= 0
                        && powerI3 >= 0
                        && powerV4 >= 0
                        && powerI4 >= 0
                        && powerV5 >= 0
                        && powerI5 >= 0
                        && powerV6 >= 0
                        && powerI6 >= 0
                        && powerV7 >= 0
                        && powerI7 >= 0
                        && powerV8 >= 0
                        && powerI8 >= 0
                        && powerV9 >= 0
                        && powerI9 >= 0
                        && powerV10 >= 0
                        && powerI10 >= 0
                        && powerV11 >= 0
                        && powerI11 >= 0
                        && powerV12 >= 0
                        && powerI12 >= 0
                        && powerV13 >= 0
                        && powerI13 >= 0
                        && powerV14 >= 0
                        && powerI14 >= 0
                        && powerV15 >= 0
                        && powerI15 >= 0
                        && powerV16 >= 0
                        && powerI16 >= 0
                        && powerV17 >= 0
                        && powerI17 >= 0
                        && powerV18 >= 0
                        && powerI18 >= 0
                        && chargeV >= 0
                        && chargeI >= 0
                        && surgeV >= 0
                        && surgeI >= 0
                        && testV >= 0
                        && testI >= 0
                        && shortCircuitV >= 0
                        && openCircuitV >= 0
                        && leakageUpperLimit >= 0
                        && leakageMiddleLimit > 0
                        && leakageLowerLimit >= 0
                        && capacitorNominal >= 0
                        && premiumLimit >= 0
                        && superiorLowerLimit >= 0
                        && goodProductUpperLimit >= 0
                        && goodProductLowerLimit >= 0
                        && unAgedVoltage >= 0
                        && lossAngelUpperLimit >= 0
                        && upperImpedanceLimit >= 0
                        && goodProduct2UpperLimit >= 0
                        && goodProduct2LowerLimit >= 0
                        && agingTime >= 0
                        && codingMark != null && codingMark.length() > 0 && !codingMark.contains(" ")
                        && codingStartNumber != null && codingStartNumber.length() > 0 && !codingStartNumber.contains(" ")
                        && surgeUpperLimit >= 0
                        && upperTemperatureLimit >= 0
                        && instrumentFrequency >= 0
                        && ovenTemperature >= 0
                        && implosionTime >= 0
                        && implosionVoltageUpperLimit >= 0
                        && implosionVoltageLowerLimit >= 0
                        && implosionCurrentUpperLimit >= 0
                        && implosionCurrentLowerLimit >= 0;
    }

    @Override
    public String toString() {
        return "ProcessParameter{" +
                "id=" + id +
                ", batchId='" + batchId + '\'' +
                ", produceSequence=" + produceSequence +
                ", state=" + state +
                ", powerV1=" + powerV1 +
                ", powerI1=" + powerI1 +
                ", powerV2=" + powerV2 +
                ", powerI2=" + powerI2 +
                ", powerV3=" + powerV3 +
                ", powerI3=" + powerI3 +
                ", powerV4=" + powerV4 +
                ", powerI4=" + powerI4 +
                ", powerV5=" + powerV5 +
                ", powerI5=" + powerI5 +
                ", powerV6=" + powerV6 +
                ", powerI6=" + powerI6 +
                ", powerV7=" + powerV7 +
                ", powerI7=" + powerI7 +
                ", powerV8=" + powerV8 +
                ", powerI8=" + powerI8 +
                ", powerV9=" + powerV9 +
                ", powerI9=" + powerI9 +
                ", powerV10=" + powerV10 +
                ", powerI10=" + powerI10 +
                ", powerV11=" + powerV11 +
                ", powerI11=" + powerI11 +
                ", powerV12=" + powerV12 +
                ", powerI12=" + powerI12 +
                ", powerV13=" + powerV13 +
                ", powerI13=" + powerI13 +
                ", powerV14=" + powerV14 +
                ", powerI14=" + powerI14 +
                ", powerV15=" + powerV15 +
                ", powerI15=" + powerI15 +
                ", powerV16=" + powerV16 +
                ", powerI16=" + powerI16 +
                ", powerV17=" + powerV17 +
                ", powerI17=" + powerI17 +
                ", powerV18=" + powerV18 +
                ", powerI18=" + powerI18 +
                ", chargeV=" + chargeV +
                ", surgeV=" + surgeV +
                ", testV=" + testV +
                ", shortCircuitV=" + shortCircuitV +
                ", openCircuitV=" + openCircuitV +
                ", leakageUpperLimit=" + leakageUpperLimit +
                ", leakageMiddleLimit=" + leakageMiddleLimit +
                ", leakageLowerLimit=" + leakageLowerLimit +
                ", capacitorNominal=" + capacitorNominal +
                ", premiumLimit=" + premiumLimit +
                ", superiorLowerLimit=" + superiorLowerLimit +
                ", goodProductUpperLimit=" + goodProductUpperLimit +
                ", goodProductLowerLimit=" + goodProductLowerLimit +
                ", unAgedVoltage=" + unAgedVoltage +
                ", lossAngelUpperLimit=" + lossAngelUpperLimit +
                ", upperImpedanceLimit=" + upperImpedanceLimit +
                ", goodProduct2UpperLimit=" + goodProduct2UpperLimit +
                ", goodProduct2LowerLimit=" + goodProduct2LowerLimit +
                ", agingTime=" + agingTime +
                ", codingMark='" + codingMark + '\'' +
                ", codingStartNumber='" + codingStartNumber + '\'' +
                ", surgeUpperLimit=" + surgeUpperLimit +
                ", upperTemperatureLimit=" + upperTemperatureLimit +
                ", instrumentFrequency=" + instrumentFrequency +
                ", ovenTemperature=" + ovenTemperature +
                ", implosionTime=" + implosionTime +
                ", implosionVoltageUpperLimit=" + implosionVoltageUpperLimit +
                ", implosionVoltageLowerLimit=" + implosionVoltageLowerLimit +
                ", implosionCurrentUpperLimit=" + implosionCurrentUpperLimit +
                ", implosionCurrentLowerLimit=" + implosionCurrentLowerLimit +
                '}';
    }
}
