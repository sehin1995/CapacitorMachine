package com.sunseen.capacitormachine.commumication.http.callback;

public interface IFailure {

    void onFailure();
}
