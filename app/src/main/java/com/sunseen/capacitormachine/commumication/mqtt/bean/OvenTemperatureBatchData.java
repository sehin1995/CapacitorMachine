package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

import java.util.List;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class OvenTemperatureBatchData {
    public int temperature;
    public List<String> uidList;

    public OvenTemperatureBatchData(int temperature, List<String> uidList) {
        this.temperature = temperature;
        this.uidList = uidList;
    }
}
