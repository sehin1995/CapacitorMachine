package com.sunseen.capacitormachine.modules.query;

import android.graphics.Color;
import android.view.View;

import androidx.databinding.ViewDataBinding;

import com.bin.david.form.data.table.FormTableData;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.MethodUtil;
import com.sunseen.capacitormachine.databinding.FragmentQueryDetailBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.common.Form;
import com.sunseen.capacitormachine.modules.query.bean.BaseInfo;
import com.sunseen.capacitormachine.modules.query.bean.CapacitorInfoBean;
import com.sunseen.capacitormachine.modules.query.bean.Capacity;

public class QueryDetailFragment extends BaseFragment implements View.OnClickListener {

    private CapacitorInfoBean infoBean = null;

    public void setCapacitorInfo(CapacitorInfoBean infoBean) {
        this.infoBean = infoBean;
    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_query_detail;
    }

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        FragmentQueryDetailBinding binding = (FragmentQueryDetailBinding) viewDataBinding;
        binding.toolBar.setNavigationOnClickListener((v) -> pop());
        FormTableData<Form> dataForm = FormTableData.create(binding.tableCapacitorDetail, "", 6, createForms());
        dataForm.setFormat((t) -> t != null ? t.getContent() : "");

        binding.tableCapacitorDetail.getConfig().setShowXSequence(false);
        binding.tableCapacitorDetail.getConfig().setShowYSequence(false);
        binding.tableCapacitorDetail.getConfig().setShowTableTitle(false);
        binding.tableCapacitorDetail.getConfig().getContentGridStyle().setColor(Color.parseColor("#898989"));
        binding.tableCapacitorDetail.getConfig().getContentStyle().setTextColor(Color.parseColor("#222222"));
        binding.tableCapacitorDetail.setTableData(dataForm);

        binding.btnViewFlowCard.setOnClickListener(this);
        binding.btnViewProcessParameter.setOnClickListener(this);
        binding.btnViewCapacitorCurve.setOnClickListener(this);
        binding.btnViewSurgeCurve.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_view_flow_card: {
                QueryFlowCardFragment fragment = new QueryFlowCardFragment();
                fragment.setData(MethodUtil.getNoNullString(infoBean.getBaseInfo().getUid()),
                        MethodUtil.getNoNullString(infoBean.getBaseInfo().getBatchId()));
                start(fragment);
            }
            break;
            case R.id.btn_view_process_parameter: {
                QueryProcessParameterFragment fragment = new QueryProcessParameterFragment();
                fragment.setData(MethodUtil.getNoNullString(infoBean.getBaseInfo().getUid()),
                        MethodUtil.getNoNullString(infoBean.getBaseInfo().getBatchId()));
                start(fragment);
            }
            break;
            case R.id.btn_view_capacitor_curve: {
                QueryCapacitorCurveFragment fragment = new QueryCapacitorCurveFragment();
                fragment.setUniqueId(MethodUtil.getNoNullString(infoBean.getBaseInfo().getUid()));
                start(fragment);
            }
            break;
            case R.id.btn_view_surge_curve: {
                QuerySurgeCurveFragment fragment = new QuerySurgeCurveFragment();
                fragment.setUniqueId(MethodUtil.getNoNullString(infoBean.getBaseInfo().getUid()));
                start(fragment);
            }
            break;
        }
    }

    private Form[][] createForms() {

        Form[][] newFrom = new Form[][]{
                {
                        new Form(getString(R.string.capacitor_id)),
                        new Form(""),
                        new Form(MethodUtil.getNoNullString(getString(R.string.capacitor_uid))),
                        new Form(""),
                        new Form(getString(R.string.batch_id)),
                        new Form(getString(R.string.string_8_tab))
                },
                {
                        new Form(getString(R.string.device_number)),
                        new Form(""),
                        new Form("分选结果"),
                        new Form(""),
                        new Form("内爆检测结果"),
                        new Form(""),
                },
                {
                        new Form("开路检测电压值"),
                        new Form(""),
                        new Form("开路检测结果"),
                        new Form(""),
                        new Form("短路检测电压值"),
                        new Form(""),
                },
                {
                        new Form("短路检测结果"),
                        new Form(""),
                        new Form("未老化检测电压值"),
                        new Form(""),
                        new Form("未老化检测结果"),
                        new Form(""),
                },
                {
                        new Form("凸顶检测结果"),
                        new Form(""),
                        new Form("漏电流检测电流值"),
                        new Form(""),
                        new Form("漏电流检测结果"),
                        new Form(""),
                },
                {
                        new Form("浪涌1检测结果"),
                        new Form(""),
                        new Form("浪涌2检测结果"),
                        new Form(""),
                        new Form("温度检测结果"),
                        new Form("")
                },
                {
                        new Form("温度检测值"),
                        new Form(""),
                        new Form("漏电流2检测电流值"),
                        new Form(""),
                        new Form("漏电流2检测结果"),
                        new Form(""),

                },
                {
                        new Form("容量检测结果"),
                        new Form(""),
                        new Form("阻抗检测结果"),
                        new Form(""),
                        new Form("损失角检测结果"),
                        new Form("")
                },
                {
                        new Form("容量检测值"),
                        new Form(""),
                        new Form("阻抗检测值"),
                        new Form(""),
                        new Form("损失角检测值"),
                        new Form(""),
                },
                {
//                        new Form("容量检测结果2"),
//                        new Form(""),
//                        new Form("阻抗测试结果2"),
//                        new Form(""),
//                        new Form("损失角检测结果2"),
//                        new Form(""),

                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                },
                {
//                        new Form("容量检测值2"),
//                        new Form(""),
//                        new Form("阻抗检测值2"),
//                        new Form(""),
//                        new Form("损失角检测值2"),
//                        new Form(""),

                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                }};
        if (infoBean != null) {
            BaseInfo baseInfo = infoBean.getBaseInfo();
            if (baseInfo != null) {
                Form[] form0 = new Form[]{
                        new Form(getString(R.string.capacitor_id)),
                        new Form(MethodUtil.getNoNullString(baseInfo.getCapacitorId())),
                        new Form(MethodUtil.getNoNullString(getString(R.string.capacitor_uid))),
                        new Form(baseInfo.getUid()),
                        new Form(getString(R.string.batch_id)),
                        new Form(MethodUtil.getNoNullString(baseInfo.getBatchId())),
                };
                Form[] form1 = new Form[]{
                        new Form(getString(R.string.device_number)),
                        new Form(MethodUtil.getNoNullString(baseInfo.getDeviceNo())),
                        new Form("分选结果"),
                        new Form(getLevelStr(baseInfo.getLevel())),
                        new Form("内爆检测结果"),
                        new Form(getCheckResult(baseInfo.getImplosionResult())),
                };
                Form[] form2 = new Form[]{
                        new Form("开路检测电压值"),
                        new Form(MethodUtil.createVUnit(baseInfo.getOpenCircuitVoltage())),
                        new Form("开路检测结果"),
                        new Form(getCheckResult(baseInfo.getOpenCircuitResult())),
                        new Form("短路检测电压值"),
                        new Form(MethodUtil.createVUnit(baseInfo.getShortCircuitVoltage())),
                };
                Form[] form3 = new Form[]{
                        new Form("凸顶检测结果"),
                        new Form(getCheckResult(baseInfo.getConvexResult())),
                        new Form("漏电流检测电流值"),
                        new Form(MethodUtil.createIUnit(baseInfo.getLeakCurrent1())),
                        new Form("漏电流检测结果"),
                        new Form(getCheckResult(baseInfo.getLeakCurrent1Result())),
                };
                Form[] form4 = new Form[]{
                        new Form("短路检测结果"),
                        new Form(getCheckResult(infoBean.getBaseInfo().getShortCircuitResult())),
                        new Form("未老化检测电压值"),
                        new Form(MethodUtil.createVUnit(infoBean.getBaseInfo().getUnAgeValue())),
                        new Form("未老化检测结果"),
                        new Form(getCheckResult(infoBean.getBaseInfo().getUnAgeResult())),
                };

                Form[] form5 = new Form[]{
                        new Form("浪涌1检测结果"),
                        new Form(getCheckResult(baseInfo.getSurge1Result())),
                        new Form("浪涌2检测结果"),
                        new Form(getCheckResult(baseInfo.getSurge2Result())),
                        new Form("温度检测结果"),
                        new Form(getCheckResult(baseInfo.getTemperatureResult()))
                };
                Form[] form6 = new Form[]{
                        new Form("温度检测值"),
                        new Form(MethodUtil.createStringUnit(baseInfo.getTemperature(), 1, "℃")),
                        new Form("漏电流2检测电流值"),
                        new Form(MethodUtil.createIUnit(baseInfo.getLeakCurrent1())),
                        new Form("漏电流2检测结果"),
                        new Form(getCheckResult(baseInfo.getLeakCurrent1Result())),
                };
                newFrom[0] = form0;
                newFrom[1] = form1;
                newFrom[2] = form2;
                newFrom[3] = form3;
                newFrom[4] = form4;
                newFrom[5] = form5;
                newFrom[6] = form6;
            }
            Capacity capacity1 = infoBean.getCapacitor1();
            if (capacity1 != null) {
                Form[] form7 = new Form[]{
//                        new Form("容量检测结果1"),
//                        new Form(getCheckResult(infoBean.getCapacitor1().getCapacityResult())),
//                        new Form("阻抗检测结果1"),
//                        new Form(getCheckResult(infoBean.getCapacitor1().getImpedanceResult())),
//                        new Form("损失角检测结果1"),
//                        new Form(getCheckResult(infoBean.getCapacitor1().getLossAngleResult())),

                        new Form("容量检测结果"),
                        new Form(getCheckResult(infoBean.getCapacitor1().getCapacityResult())),
                        new Form("阻抗检测结果"),
                        new Form(getCheckResult(infoBean.getCapacitor1().getImpedanceResult())),
                        new Form("损失角检测结果"),
                        new Form(getCheckResult(infoBean.getCapacitor1().getLossAngleResult())),
                };
                Form[] form8 = new Form[]{
//                        new Form("容量检测值1"),
//                        new Form(MethodUtil.createStringUnit(infoBean.getCapacitor1().getCapacity(), 2, getString(R.string.uf))),
//                        new Form("阻抗检测值1"),
//                        new Form(MethodUtil.createStringUnit(infoBean.getCapacitor1().getImpedance(), 2, "mΩ")),
//                        new Form("损失角检测值1"),
//                        new Form(MethodUtil.createStringUnit(infoBean.getCapacitor1().getLossAngle(), 6, "%")),
                        new Form("容量检测值"),
                        new Form(MethodUtil.createStringUnit(infoBean.getCapacitor1().getCapacity(), 2, getString(R.string.uf))),
                        new Form("阻抗检测值"),
                        new Form(MethodUtil.createStringUnit(infoBean.getCapacitor1().getImpedance(), 2, "mΩ")),
                        new Form("损失角检测值"),
                        new Form(MethodUtil.createStringUnit(infoBean.getCapacitor1().getLossAngle(), 6, "%")),
                };
                newFrom[7] = form7;
                newFrom[8] = form8;
            }
            Capacity capacity2 = infoBean.getCapacitor2();
            if (capacity2 != null) {
                Form[] form9 = new Form[]{
//                        new Form("容量检测结果2"),
//                        new Form(getCheckResult(infoBean.getCapacitor2().getCapacityResult())),
//                        new Form("阻抗测试结果2"),
//                        new Form(getCheckResult(infoBean.getCapacitor2().getImpedanceResult())),
//                        new Form("损失角检测结果2"),
//                        new Form(getCheckResult(infoBean.getCapacitor2().getLossAngleResult())),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                };
                Form[] form10 = new Form[]{
//                        new Form("容量检测值2"),
//                        new Form(MethodUtil.createStringUnit(infoBean.getCapacitor2().getCapacity(), 2, getString(R.string.uf))),
//                        new Form("阻抗检测值2"),
//                        new Form(MethodUtil.createStringUnit(infoBean.getCapacitor2().getImpedance(), 2, "mΩ")),
//                        new Form("损失角检测值2"),
//                        new Form(MethodUtil.createStringUnit(infoBean.getCapacitor2().getLossAngle(), 6, "%")),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                };

                newFrom[9] = form9;
                newFrom[10] = form10;
            }
        }
        return newFrom;
    }

    private String getCheckResult(String string) {
        if (string != null) {
            try {
                int flag = Integer.valueOf(string);
                return flag == 0 ?
                        getString(R.string.ok)
                        : flag == 1 ? getString(R.string.ng)
                        : getString(R.string.unknown_result);
            } catch (NumberFormatException e) {
                return getString(R.string.unknown_result);
            }
        }
        return getString(R.string.unknown_result);
    }

    private String getLevelStr(String level) {
        if (level != null) {
            try {
                int value = Integer.valueOf(level);
                switch (value) {
                    case 1:
                        return "优品";
                    case 2:
                        return "良品";
                    case 3:
                        return "高容不良";
                    case 4:
                        return "低容不良";
                    case 5:
                        return "损失角不良";
                    case 6:
                        return "漏电不良";
                    case 7:
                        return "阻卡抗不良";
                    case 8:
                        return "重测";
                    default:
                        return "";
                }
            } catch (NumberFormatException e) {
                return "";
            }
        } else {
            return "";
        }
    }
}
