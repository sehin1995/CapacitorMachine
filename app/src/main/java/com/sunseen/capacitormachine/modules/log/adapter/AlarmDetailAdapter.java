package com.sunseen.capacitormachine.modules.log.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;

public class AlarmDetailAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public AlarmDetailAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.alarm_log_detail_tv,item);
    }
}
