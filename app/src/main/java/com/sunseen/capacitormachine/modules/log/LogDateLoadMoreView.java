package com.sunseen.capacitormachine.modules.log;

import com.chad.library.adapter.base.loadmore.LoadMoreView;
import com.sunseen.capacitormachine.R;

public class LogDateLoadMoreView extends LoadMoreView {
    @Override
    public int getLayoutId() {
        return R.layout.view_log_date_load_more;
    }

    @Override
    protected int getLoadingViewId() {
        return R.id.load_more_loading_view;
    }

    @Override
    protected int getLoadFailViewId() {
        return R.id.load_more_load_fail_view;
    }

    @Override
    protected int getLoadEndViewId() {
        return R.id.load_more_load_end_view;
    }
}
