package com.sunseen.capacitormachine.modules.log;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sunseen.capacitormachine.modules.log.event.DateChangeEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;


public class DateChangeReceiver extends BroadcastReceiver {

    private int day;

    public DateChangeReceiver() {
        Calendar calendar = Calendar.getInstance();
        this.day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_TIME_TICK.equals(intent.getAction())) {
            int newDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            if(newDay != day){
                day = newDay;
                EventBus.getDefault().post(new DateChangeEvent());
            }
        }
    }
}
