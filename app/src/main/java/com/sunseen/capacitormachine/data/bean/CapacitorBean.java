package com.sunseen.capacitormachine.data.bean;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToOne;

@Entity
public class CapacitorBean {
    @Id
    long id;

    private String uid = "";
    private int sequence;
    private int voltage;
    private int current;
    private boolean empty;
    private boolean normal = true;

    public ToOne<HolderBean> holder;

    public CapacitorBean() {
    }

    public CapacitorBean(String uid, int sequence, boolean empty) {
        this.uid = uid;
        this.sequence = sequence;
        this.empty = empty;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getVoltage() {
        return voltage;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public boolean isNormal() {
        return normal;
    }

    public void setNormal(boolean normal) {
        this.normal = normal;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

}
