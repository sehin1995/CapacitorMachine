package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class OvenCapacitorJsonObj {
    public  String type = "iv";
    public long time;
    public OvenCapacitorData data;

    public OvenCapacitorJsonObj(){}

    public OvenCapacitorJsonObj(long time, OvenCapacitorData data) {
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public OvenCapacitorData getData() {
        return data;
    }

    public void setData(OvenCapacitorData data) {
        this.data = data;
    }

}
