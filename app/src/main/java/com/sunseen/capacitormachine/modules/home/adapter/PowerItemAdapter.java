package com.sunseen.capacitormachine.modules.home.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.modules.home.bean.PowerSupplyAdapterBean;

import java.util.List;

import androidx.annotation.Nullable;

/**
 * @author zest
 */
public class PowerItemAdapter extends BaseQuickAdapter<PowerSupplyAdapterBean, BaseViewHolder> {

    public PowerItemAdapter(int layoutResId, @Nullable List<PowerSupplyAdapterBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PowerSupplyAdapterBean item) {
        helper.setText(R.id.tv_temp_zone,
                String.format(helper.itemView.getContext().getString(R.string.power_format),
                        item.getNumber()))
                .setText(R.id.tv_voltage_value, String.valueOf(item.getVoltage() / 10f))
                .setImageResource(R.id.img_item_status,
                        item.isNormal() ? R.drawable.svg_normal_24 : R.drawable.svg_abnormal);
    }
}
