package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class CapacitorLevelData {
    public String uid;
    public int level;

    public CapacitorLevelData(String uid, int level) {
        this.uid = uid;
        this.level = level;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
