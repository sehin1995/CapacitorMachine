package com.sunseen.capacitormachine.modules.query;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.databinding.ViewDataBinding;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bin.david.form.data.table.FormTableData;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.common.MethodUtil;
import com.sunseen.capacitormachine.commumication.http.HttpUtil;
import com.sunseen.capacitormachine.commumication.http.RestClient;
import com.sunseen.capacitormachine.databinding.FragmentProcessQueryBinding;
import com.sunseen.capacitormachine.common.Form;
import com.sunseen.capacitormachine.modules.query.bean.ProcessBean;

public class QueryProcessParameterFragment extends BaseFragment {
    @Override
    protected int setLayout() {
        return R.layout.fragment_process_query;
    }

    private String uid = "";
    private String batchId = "";

    public void setData(String uid, String batchId) {
        this.uid = uid;
        this.batchId = batchId;
    }

    private FragmentProcessQueryBinding binding;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        binding = (FragmentProcessQueryBinding) viewDataBinding;
        binding.toolBar.setNavigationOnClickListener((v) -> pop());
        binding.toolBar.setTitle(uid + "的工艺参数");
        binding.table.getConfig().setShowTableTitle(false);
        binding.table.getConfig().setShowXSequence(false);
        binding.table.getConfig().setShowYSequence(false);
        binding.table.getConfig().getContentGridStyle().setColor(Color.parseColor("#898989"));
        binding.table.getConfig().getContentStyle().setTextColor(Color.parseColor("#222222"));
        queryProcessParameter();
    }

    private ProcessBean obj = null;

    private void queryProcessParameter() {
        RestClient.builder()
                .url(HttpUtil.Process + "/" + batchId)
                .success((String response) -> {
                    JSONObject jsonObject = JSON.parseObject(response);
                    if (jsonObject.getInteger("status") == 1) {
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        if (dataObj != null) {
                            JSONObject itemObj = dataObj.getJSONObject("item");
                            if (itemObj != null) {
                                obj = JSON.parseObject(itemObj.toJSONString(), ProcessBean.class);
                                if (obj != null) {
                                    updateTable(0);
                                } else {
                                    Toast.makeText(_mActivity, "查询电容所在工艺参数数据为空", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(_mActivity, "查询工艺参数失败", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(_mActivity, "查询工艺参数失败", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(_mActivity, "查询工艺参数失败", Toast.LENGTH_SHORT).show();
                    }
                    hideProgressBar();
                })
                .failure(() -> {
                    Toast.makeText(_mActivity, "查询工艺参数失败", Toast.LENGTH_SHORT).show();
                    hideProgressBar();
                })
                .error((int code, String msg) -> {
                    Toast.makeText(_mActivity, "查询工艺参数错误", Toast.LENGTH_SHORT).show();
                    Log.e("test", "code = " + code + " msg = " + msg);
                    hideProgressBar();
                })
                .build()
                .get();
    }

    private void hideProgressBar() {
        binding.queryProgressBar.setVisibility(View.GONE);
        binding.queryingTv.setVisibility(View.GONE);
        binding.rgFormMenu.setClickable(true);
        binding.rgFormMenu.setOnCheckedChangeListener((RadioGroup group, int checkedId) -> {
            switch (checkedId) {
                case R.id.rb_sorting_parameter: {
                    binding.tableTitleTv.setText(R.string.sorting_parameter);
                    if (obj != null) {
                        updateTable(0);
                    } else {
                        Toast.makeText(_mActivity, R.string.no_about_data, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
                case R.id.rb_power_supply: {
                    binding.tableTitleTv.setText(R.string.power_supply_parameter);
                    if (obj != null) {
                        updateTable(1);
                    } else {
                        Toast.makeText(_mActivity, R.string.no_about_data, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
                case R.id.rb_spray_code: {
                    binding.tableTitleTv.setText(R.string.spray_code_parameter);
                    if (obj != null) {
                        updateTable(2);
                    } else {
                        Toast.makeText(_mActivity, R.string.no_about_data, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
                case R.id.rb_implosion: {
                    binding.tableTitleTv.setText(R.string.implosion_parameter);
                    if (obj != null) {
                        updateTable(3);
                    } else {
                        Toast.makeText(_mActivity, R.string.no_about_data, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
                case R.id.rb_aging_process: {
                    binding.tableTitleTv.setText(R.string.aging_process_parameter);
                    if (obj != null) {
                        updateTable(4);
                    } else {
                        Toast.makeText(_mActivity, R.string.no_about_data, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
                default: {

                }
                break;
            }
        });
    }

    private void updateTable(int tablePos) {
        switch (tablePos) {
            case 0: {
                Form[][] sortingParameterForm = new Form[][]{
                        {
                                new Form("短路电压", 3),
                                new Form(MethodUtil.createVUnit(obj.getShortCircuitV()), 2),
                                new Form("开路电压", 3),
                                new Form(MethodUtil.createVUnit(obj.getOpenCircuitV()), 2)
                        },
                        {
                                new Form("漏电上限", 3),
                                new Form(MethodUtil.createCapacityIUnit(obj.getLeakageUpperLimit()), 2),
                                new Form("漏电下限", 3),
                                new Form(MethodUtil.createCapacityIUnit(obj.getLeakageLowerLimit()), 2)
                        },
                        {
                                new Form("未老化电压", 3),
                                new Form(MethodUtil.createVUnit(obj.getUnAgedVoltage()), 2),
                                new Form("", 3),
                                new Form("", 2)
                        },
                        {
                                new Form("损失上限", 3),
                                new Form(MethodUtil.createStringUnit(obj.getLossAngelUpperLimit(), 2, getString(R.string.percent)), 2),
                                new Form("阻抗上限", 3),
                                new Form(MethodUtil.createStringUnit(obj.getUpperImpedanceLimit(), 1, getString(R.string.mo)), 2)
                        },
                        {
                                new Form("浪涌上限", 3),
                                new Form(MethodUtil.createVUnit(obj.getSurgeUpperLimit()), 2),
                                new Form("温度上限", 3),
                                new Form(MethodUtil.createStringUnit(obj.getUpperTemperatureLimit(), 1, getString(R.string.temp_unit_flag)), 2)
                        },
                        {
                                new Form("电容标称", 3),
                                new Form(MethodUtil.createCapacityUnit(obj.getCapacitorNominal()), 2),
                                new Form("漏电中限", 3),
                                new Form(MethodUtil.createCapacityIUnit(obj.getLeakageMiddleLimit()), 2)
                        },
                        {
                                new Form("优品上限", 3),
                                new Form(MethodUtil.createCapacityUnit(obj.getPremiumLimit()), 2),
                                new Form("优品下限", 3),
                                new Form(MethodUtil.createCapacityUnit(obj.getSuperiorLowerLimit()), 2)
                        },
                        {
                                new Form("良品上限", 3),
                                new Form(MethodUtil.createCapacityUnit(obj.getGoodProductUpperLimit()), 2),
                                new Form("良品下限", 3),
                                new Form(MethodUtil.createCapacityUnit(obj.getGoodProductLowerLimit()), 2)
                        },
                        {
                                new Form("良品2上限", 3),
                                new Form(MethodUtil.createCapacityUnit(obj.getGoodProduct2UpperLimit()), 2),
                                new Form("良品2下限", 3),
                                new Form(MethodUtil.createCapacityUnit(obj.getGoodProduct2LowerLimit()), 2)
                        }
                };

                FormTableData<Form> tableData = FormTableData.create(binding.table, "", 10, sortingParameterForm);
                tableData.setFormat((t) -> {
                    if (t != null) {
                        return MethodUtil.getNoNullString(t.getContent());
                    } else {
                        return "";
                    }
                });
                binding.table.setTableData(tableData);
            }
            break;
            case 1: {
                Form[][] powerForm = new Form[][]{
                        {
                                new Form("电源1"),
                                new Form(MethodUtil.createVUnit(obj.getV1()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI1())),
                                new Form("电源2"),
                                new Form(MethodUtil.createVUnit(obj.getV2()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI2())),
                                new Form("电源3"),
                                new Form(MethodUtil.createVUnit(obj.getV3()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI3())),
                        },
                        {
                                new Form("电源4"),
                                new Form(MethodUtil.createVUnit(obj.getV4()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI4())),
                                new Form("电源5"),
                                new Form(MethodUtil.createVUnit(obj.getV5()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI5())),
                                new Form("电源6"),
                                new Form(MethodUtil.createVUnit(obj.getV6()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI6())),
                        },
                        {
                                new Form("电源7"),
                                new Form(MethodUtil.createVUnit(obj.getV7()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI7())),
                                new Form("电源8"),
                                new Form(MethodUtil.createVUnit(obj.getV8()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI8())),
                                new Form("电源9"),
                                new Form(MethodUtil.createVUnit(obj.getV9()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI9())),
                        },
                        {
                                new Form("电源10"),
                                new Form(MethodUtil.createVUnit(obj.getV10()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI10())),
                                new Form("电源11"),
                                new Form(MethodUtil.createVUnit(obj.getV11()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI11())),
                                new Form("电源12"),
                                new Form(MethodUtil.createVUnit(obj.getV12()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI12())),
                        },
                        {
                                new Form("电源13"),
                                new Form(MethodUtil.createVUnit(obj.getV13()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI13())),
                                new Form("电源14"),
                                new Form(MethodUtil.createVUnit(obj.getV14()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI14())),
                                new Form("电源15"),
                                new Form(MethodUtil.createVUnit(obj.getV15()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI15())),
                        },
                        {
                                new Form("电源16"),
                                new Form(MethodUtil.createVUnit(obj.getV16()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI16())),
                                new Form("电源17"),
                                new Form(MethodUtil.createVUnit(obj.getV17()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI17())),
                                new Form("电源18"),
                                new Form(MethodUtil.createVUnit(obj.getV18()), 2),
                                new Form(MethodUtil.createIUnit(obj.getI18())),
                        },
                        {
                                new Form("未老化电源(19)"),
                                new Form(MethodUtil.createVUnit(obj.getChargeV()), 2),
                                new Form(MethodUtil.createIUnit(obj.getChargeI())),
                                new Form("浪涌电源(20)"),
                                new Form(MethodUtil.createVUnit(obj.getSurgeV()), 2),
                                new Form(MethodUtil.createIUnit(obj.getSurgeI())),
                                new Form("测试电源(21)"),
                                new Form(MethodUtil.createVUnit(obj.getTestV()), 2),
                                new Form(MethodUtil.createIUnit(obj.getTestI()))
                        }
                };
                FormTableData<Form> powerTableData = FormTableData.create(binding.table, "", 12, powerForm);
                powerTableData.setFormat((t) -> {
                    if (t != null) {
                        return MethodUtil.getNoNullString(t.getContent());
                    } else {
                        return "";
                    }
                });
                binding.table.setTableData(powerTableData);
            }
            break;
            case 2: {
                Form[][] sprayCodeForm = new Form[][]{
                        {
                                new Form("前缀标识", 2),
                                new Form(obj.getCodingMark(), 7)
                        },
                        {
                                new Form("起始编号", 2),
                                new Form(obj.getCodingStartNumber(), 7)
                        }
                };

                FormTableData<Form> sprayCodeTableData = FormTableData.create(binding.table, "", 9, sprayCodeForm);
                sprayCodeTableData.setFormat((t) -> {
                    if (t != null) {
                        return MethodUtil.getNoNullString(t.getContent());
                    } else {
                        return "";
                    }
                });
                binding.table.setTableData(sprayCodeTableData);
            }
            break;
            case 3: {
                Form[][] implosionForm = new Form[][]{
                        {
                                new Form("内爆时间", 2),
                                new Form(MethodUtil.createStringUnit(obj.getImplosionTime(), 0, getString(R.string.ms)), 10)
                        },
                        {
                                new Form("内爆电压上限", 2),
//                                new Form(MethodUtil.createVUnit(obj.getImplosionVoltageUpperLimit()), 10)
                                new Form(MethodUtil.createStringUnit(obj.getImplosionVoltageUpperLimit(), 0, getString(R.string.volt)), 10)
                        },
                        {
                                new Form("内爆电压下限", 2),
//                                new Form(MethodUtil.createVUnit(obj.getImplosionVoltageLowerLimit()), 10)
                                new Form(MethodUtil.createStringUnit(obj.getImplosionVoltageLowerLimit(), 0, getString(R.string.volt)), 10)
                        },

                        {
                                new Form("内爆电流上限", 2),
                                new Form(MethodUtil.createCapacityIUnit(obj.getImplosionCurrentUpperLimit()), 10)
                        },
                        {
                                new Form("内爆电流下限", 2),
                                new Form(MethodUtil.createCapacityIUnit(obj.getImplosionCurrentLowerLimit()), 10)
                        }
                };


                FormTableData<Form> implosionTableData = FormTableData.create(binding.table, "", 12, implosionForm);
                implosionTableData.setFormat((t) -> {
                    if (t != null) {
                        return MethodUtil.getNoNullString(t.getContent());
                    } else {
                        return "";
                    }
                });
                binding.table.setTableData(implosionTableData);
            }
            break;
            case 4: {
                Form[][] processForm = new Form[][]{
                        {
                                new Form("仪表频率", 3),
                                new Form(MethodUtil.createStringUnit(obj.getInstrumentFrequency(), 0, getString(R.string.hz)), 10)
                        },
                        {
                                new Form("烤箱温度", 3),
                                new Form(MethodUtil.createStringUnit(obj.getOvenTemperature(), 1, getString(R.string.temp_unit_flag)), 10)
                        },
                        {
                                new Form("老化时间", 3),
                                new Form(MethodUtil.createStringUnit(obj.getAgingTime(), 0, getString(R.string.minute)), 10)
                        }
                };

                FormTableData<Form> processTableData = FormTableData.create(binding.table, "", 13, processForm);
                processTableData.setFormat((t) -> {
                    if (t != null) {
                        return MethodUtil.getNoNullString(t.getContent());
                    } else {
                        return "";
                    }
                });
                binding.table.setTableData(processTableData);
            }
            break;
            default:
                break;
        }
    }
}
