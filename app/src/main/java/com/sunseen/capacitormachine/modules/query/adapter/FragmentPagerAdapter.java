package com.sunseen.capacitormachine.modules.query.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.sunseen.capacitormachine.modules.query.bean.ProcessBean;

public class FragmentPagerAdapter extends FragmentStateAdapter {

    private ProcessBean processBean;

    public ProcessBean getProcessBean() {
        return processBean;
    }

    public void setProcessBean(ProcessBean processBean) {
        this.processBean = processBean;
    }

    public FragmentPagerAdapter(@NonNull Fragment fragment) {
        super(fragment);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return null;
    }

    @Override
    public int getItemCount() {
        return 5;
    }
}
