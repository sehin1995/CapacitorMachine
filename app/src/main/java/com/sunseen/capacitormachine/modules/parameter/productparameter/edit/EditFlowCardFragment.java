package com.sunseen.capacitormachine.modules.parameter.productparameter.edit;

import android.view.View;

import com.bin.david.form.data.column.Column;
import com.bin.david.form.data.format.selected.BaseSelectFormat;
import com.bin.david.form.data.table.FormTableData;
import com.bin.david.form.data.table.TableData;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.Form;
import com.sunseen.capacitormachine.databinding.FragmentEditProductFlowBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.modules.parameter.productivetask.event.SetParameterEvent;

import androidx.databinding.ViewDataBinding;

import org.greenrobot.eventbus.EventBus;

/**
 * @author zest
 */
public class EditFlowCardFragment extends BaseFragment {
    @Override
    protected int setLayout() {
        return R.layout.fragment_edit_product_flow;
    }

    private Form selectForm;
    private int curCol = -1;
    private int curRow = -1;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        FragmentEditProductFlowBinding binding = (FragmentEditProductFlowBinding) viewDataBinding;
        binding.tableParameter.getConfig().setShowXSequence(false);
        binding.tableParameter.getConfig().setShowYSequence(false);
        binding.tableParameter.getConfig().setShowTableTitle(false);
        binding.tableParameter.getConfig().getContentGridStyle()
                .setColor(getResources().getColor(R.color.table_content_grid_color));
        binding.tableParameter.getConfig().getContentStyle()
                .setTextColor(getResources().getColor(R.color.table_content_text_color));
        binding.tableParameter.setSelectFormat(new BaseSelectFormat());
        Form[][] forms = new Form[][]{
                {
                        new Form(getString(R.string.batch_id)),
                        new Form("", 7),
                        new Form(getString(R.string.product_code)),
                        new Form("", 7),
                        new Form(getString(R.string.customer_code)),
                        new Form("", 7),
                        new Form(getString(R.string.fill_table_date)),
                        new Form("", 7)
                },
                {
                        new Form(getString(R.string.specification)),
                        new Form("", 7),
                        new Form(getString(R.string.size)),
                        new Form("mm", 7),
                        new Form(getString(R.string.order_quantity)),
                        new Form("颗", 7),
                        new Form(getString(R.string.range_of_capacity)),
                        new Form("", 7)
                },
                {
                        new Form(getString(R.string.positive_foil_type)),
                        new Form("", 7),
                        new Form(getString(R.string.positive_foil_supplier)),
                        new Form("", 7),
                        new Form(getString(R.string.positive_foil_size)),
                        new Form("", 7),
                        new Form(getString(R.string.positive_foil_specific_volume)),
                        new Form("", 7),
                },

                {
                        new Form(getString(R.string.negative_foil_type)),
                        new Form("", 7),
                        new Form(getString(R.string.negative_foil_supplier)),
                        new Form("", 7),
                        new Form(getString(R.string.negative_foil_size)),
                        new Form("", 7),
                        new Form(getString(R.string.guide_pin_type)),
                        new Form("", 7),
                },
                {
                        new Form(getString(R.string.guide_pin_supplier)),
                        new Form("", 7),
                        new Form(getString(R.string.guide_pin_remark)),
                        new Form("", 7),
                        new Form(getString(R.string.electrolytic_paper)),
                        new Form("", 7),
                        new Form(getString(R.string.electrolytic_paper_supplier)),
                        new Form("", 7),
                },
                {
                        new Form(getString(R.string.electrolytic_paper_remark)),
                        new Form("", 7),
                        new Form(getString(R.string.electrolytic_paper_2)),
                        new Form("", 7),
                        new Form(getString(R.string.electrolytic_paper_supplier_2)),
                        new Form("", 7),
                        new Form(getString(R.string.electrolytic_paper_remark_2)),
                        new Form("", 7),
                },
                {
                        new Form(getString(R.string.electrolyte)),
                        new Form("", 7),
                        new Form(getString(R.string.electrolyte_supplier)),
                        new Form("", 7),
                        new Form(getString(R.string.electrolyte_remark)),
                        new Form("", 7),
                        new Form(getString(R.string.cover)),
                        new Form("", 7),
                },
                {
                        new Form(getString(R.string.cover_supplier)),
                        new Form("", 7),
                        new Form(getString(R.string.cover_remark)),
                        new Form("", 7),
                        new Form(getString(R.string.aluminum_case)),
                        new Form("", 7),
                        new Form(getString(R.string.aluminum_case_supplier)),
                        new Form("", 7),
                },
                {
                        new Form(getString(R.string.aluminum_case_remark)),
                        new Form("", 7),
                        new Form(getString(R.string.drive_pipe)),
                        new Form("", 7),
                        new Form(getString(R.string.drive_pipe_supplier)),
                        new Form("", 7),
                        new Form(getString(R.string.drive_pipe_remark)),
                        new Form("", 7),
                },
        };
        FormTableData<Form> tableData = FormTableData.create(binding.tableParameter,
                "", 32, forms);
        tableData.setFormat((Form form) -> {
            if (form != null) {
                return form.getContent();
            } else {
                return "";
            }
        });
        tableData.setOnItemClickListener(new TableData.OnItemClickListener<Form>() {
            @Override
            public void onClick(Column column, String value, Form form, int col, int row) {
                if (form != null) {
                    if ((col % 8) == 0) {
                        //列数从0开始，偶数列为属性名称，不可更改
                        binding.btnInput.setEnabled(false);
                        curCol = -1;
                        curRow = -1;
                    } else {
                        binding.btnInput.setEnabled(true);
                        selectForm = form;
                        curCol = col;
                        curRow = row;
                    }
                }
            }
        });
        binding.tableParameter.setTableData(tableData);
        binding.btnInput.setOnClickListener((View v) -> {
            if (selectForm != null) {
                String content = binding.editInput.getText().toString();
                switch (curCol) {
                    case 1: {
                        switch (curRow) {
                            case 0: {
                                EventBus.getDefault().post(new SetParameterEvent("batchId", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 1: {
                                EventBus.getDefault().post(new SetParameterEvent("specification", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 2: {
                                EventBus.getDefault().post(new SetParameterEvent("positiveFoilModel", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 3: {
                                EventBus.getDefault().post(new SetParameterEvent("negativeFoilModel", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 4: {
                                EventBus.getDefault().post(new SetParameterEvent("guildPinSupplier", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 5: {
                                EventBus.getDefault().post(new SetParameterEvent("electrolyticPaper1Remark", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 6: {
                                EventBus.getDefault().post(new SetParameterEvent("electrolyte", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 7: {
                                EventBus.getDefault().post(new SetParameterEvent("coverSupplier", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 8: {
                                EventBus.getDefault().post(new SetParameterEvent("aluminumShellRemark", content));
                                selectForm.setContent(content);
                            }
                            break;
                        }
                    }
                    break;
                    case 9: {
                        switch (curRow) {
                            case 0: {
                                EventBus.getDefault().post(new SetParameterEvent("productCode", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 1: {
                                EventBus.getDefault().post(new SetParameterEvent("size", content));
                                selectForm.setContent(content + "mm");
                            }
                            break;
                            case 2: {
                                EventBus.getDefault().post(new SetParameterEvent("positiveFoilSupplier", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 3: {
                                EventBus.getDefault().post(new SetParameterEvent("negativeFoilSupplier", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 4: {
                                EventBus.getDefault().post(new SetParameterEvent("guildPinRemark", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 5: {
                                EventBus.getDefault().post(new SetParameterEvent("electrolyticPaper2", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 6: {
                                EventBus.getDefault().post(new SetParameterEvent("electrolyteSupplier", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 7: {
                                EventBus.getDefault().post(new SetParameterEvent("coverRemark", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 8: {
                                EventBus.getDefault().post(new SetParameterEvent("casing", content));
                                selectForm.setContent(content);
                            }
                            break;
                        }
                    }
                    break;
                    case 17: {
                        switch (curRow) {
                            case 0: {
                                EventBus.getDefault().post(new SetParameterEvent("customerCode", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 1: {
                                EventBus.getDefault().post(new SetParameterEvent("amount", content));
                                selectForm.setContent(content + "颗");
                            }
                            break;
                            case 2: {
                                EventBus.getDefault().post(new SetParameterEvent("positiveFoilSize", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 3: {
                                EventBus.getDefault().post(new SetParameterEvent("negativeFoilSize", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 4: {
                                EventBus.getDefault().post(new SetParameterEvent("electrolyticPaper1", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 5: {
                                EventBus.getDefault().post(new SetParameterEvent("electrolyticPaper2Supplier", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 6: {
                                EventBus.getDefault().post(new SetParameterEvent("electrolyteRemark", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 7: {
                                EventBus.getDefault().post(new SetParameterEvent("aluminumShell", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 8: {
                                EventBus.getDefault().post(new SetParameterEvent("casingSupplier", content));
                                selectForm.setContent(content);
                            }
                            break;
                        }
                    }
                    break;
                    case 25: {
                        switch (curRow) {
                            case 0: {
                                EventBus.getDefault().post(new SetParameterEvent("date", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 1: {
                                EventBus.getDefault().post(new SetParameterEvent("capacityRange", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 2: {
                                EventBus.getDefault().post(new SetParameterEvent("positiveFoil", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 3: {
                                EventBus.getDefault().post(new SetParameterEvent("guildPinModel", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 4: {
                                EventBus.getDefault().post(new SetParameterEvent("electrolyticPaper1Supplier", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 5: {
                                EventBus.getDefault().post(new SetParameterEvent("electrolyticPaper2Remark", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 6: {
                                EventBus.getDefault().post(new SetParameterEvent("cover", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 7: {
                                EventBus.getDefault().post(new SetParameterEvent("aluminumShellSupplier", content));
                                selectForm.setContent(content);
                            }
                            break;
                            case 8: {
                                EventBus.getDefault().post(new SetParameterEvent("casingRemark", content));
                                selectForm.setContent(content);
                            }
                            break;
                        }
                    }
                    break;
                }
                binding.tableParameter.invalidate();
                binding.editInput.setText("");

            }
        });
    }
}
