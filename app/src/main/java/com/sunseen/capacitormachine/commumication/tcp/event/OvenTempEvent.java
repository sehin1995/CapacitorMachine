package com.sunseen.capacitormachine.commumication.tcp.event;

public class OvenTempEvent {
    private int holderIndex;
    private int temp;

    public OvenTempEvent(int holderIndex, int temp) {
        this.holderIndex = holderIndex;
        this.temp = temp;
    }

    public int getHolderIndex() {
        return holderIndex;
    }

    public int getTemp() {
        return temp;
    }
}
