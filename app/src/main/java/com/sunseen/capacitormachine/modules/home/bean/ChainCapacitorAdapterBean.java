package com.sunseen.capacitormachine.modules.home.bean;

public class ChainCapacitorAdapterBean {
    private String uid;
    private int index;
    private boolean implosion;
    private boolean empty;

    public ChainCapacitorAdapterBean(String uid, int index, boolean empty) {
        this.uid = uid;
        this.index = index;
        this.empty = empty;
    }

    public ChainCapacitorAdapterBean(String uid, int index, boolean empty, boolean implosion) {
        this.uid = uid;
        this.index = index;
        this.empty = empty;
        this.implosion = implosion;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isImplosion() {
        return implosion;
    }

    public void setImplosion(boolean implosion) {
        this.implosion = implosion;
    }
}
