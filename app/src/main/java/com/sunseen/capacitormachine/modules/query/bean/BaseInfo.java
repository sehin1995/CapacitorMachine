package com.sunseen.capacitormachine.modules.query.bean;

public class BaseInfo {
    private String uid;
    private String capacitorId;
    private String batchId;
    private String time;
    private String deviceNo;
    private String openCircuitVoltage;
    private String openCircuitResult;
    private String shortCircuitVoltage;
    private String shortCircuitResult;
    private String unAgeResult;
    private String unAgeValue;
    private String surge1Result;
    private String surge2Result;
    private String convexResult;
    private String temperatureResult;
    private String temperature;
    private String leakCurrent1;
    private String leakCurrent1Result;
    private String leakCurrent2;
    private String leakCurrent2Result;
    private String level;
    private String implosionResult;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCapacitorId() {
        return capacitorId;
    }

    public void setCapacitorId(String capacitorId) {
        this.capacitorId = capacitorId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getOpenCircuitVoltage() {
        return openCircuitVoltage;
    }

    public void setOpenCircuitVoltage(String openCircuitVoltage) {
        this.openCircuitVoltage = openCircuitVoltage;
    }

    public String getOpenCircuitResult() {
        return openCircuitResult;
    }

    public void setOpenCircuitResult(String openCircuitResult) {
        this.openCircuitResult = openCircuitResult;
    }

    public String getShortCircuitVoltage() {
        return shortCircuitVoltage;
    }

    public void setShortCircuitVoltage(String shortCircuitVoltage) {
        this.shortCircuitVoltage = shortCircuitVoltage;
    }

    public String getShortCircuitResult() {
        return shortCircuitResult;
    }

    public void setShortCircuitResult(String shortCircuitResult) {
        this.shortCircuitResult = shortCircuitResult;
    }

    public String getUnAgeResult() {
        return unAgeResult;
    }

    public void setUnAgeResult(String unAgeResult) {
        this.unAgeResult = unAgeResult;
    }

    public String getUnAgeValue() {
        return unAgeValue;
    }

    public void setUnAgeValue(String unAgeValue) {
        this.unAgeValue = unAgeValue;
    }

    public String getSurge1Result() {
        return surge1Result;
    }

    public void setSurge1Result(String surge1Result) {
        this.surge1Result = surge1Result;
    }

    public String getSurge2Result() {
        return surge2Result;
    }

    public void setSurge2Result(String surge2Result) {
        this.surge2Result = surge2Result;
    }

    public String getConvexResult() {
        return convexResult;
    }

    public void setConvexResult(String convexResult) {
        this.convexResult = convexResult;
    }

    public String getTemperatureResult() {
        return temperatureResult;
    }

    public void setTemperatureResult(String temperatureResult) {
        this.temperatureResult = temperatureResult;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getLeakCurrent1() {
        return leakCurrent1;
    }

    public void setLeakCurrent1(String leakCurrent1) {
        this.leakCurrent1 = leakCurrent1;
    }

    public String getLeakCurrent1Result() {
        return leakCurrent1Result;
    }

    public void setLeakCurrent1Result(String leakCurrent1Result) {
        this.leakCurrent1Result = leakCurrent1Result;
    }

    public String getLeakCurrent2() {
        return leakCurrent2;
    }

    public void setLeakCurrent2(String leakCurrent2) {
        this.leakCurrent2 = leakCurrent2;
    }

    public String getLeakCurrent2Result() {
        return leakCurrent2Result;
    }

    public void setLeakCurrent2Result(String leakCurrent2Result) {
        this.leakCurrent2Result = leakCurrent2Result;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getImplosionResult() {
        return implosionResult;
    }

    public void setImplosionResult(String implosionResult) {
        this.implosionResult = implosionResult;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

}
