package com.sunseen.capacitormachine.commumication.serialport.event;

import com.sunseen.capacitormachine.modules.home.bean.PowerSupplyBean;

import java.util.List;

public class UpdatePowerSupplyEvent {

    private List<PowerSupplyBean> supplyBeanList;

    public UpdatePowerSupplyEvent(List<PowerSupplyBean> supplyBeanList) {
       this.supplyBeanList = supplyBeanList;
    }

    public List<PowerSupplyBean> getSupplyBeanList() {
        return supplyBeanList;
    }
}
