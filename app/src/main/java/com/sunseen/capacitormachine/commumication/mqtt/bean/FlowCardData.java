package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class FlowCardData {
    public String batchId;
    public String productCode = "";
    public String customerCode = "";
    public String date = "";
    public String specification = "";
    public String size = "";
    public String deviceId = "";
    public String amount = "";
    public String capacityRange = "";
    public String positiveFoil = "";
    public String positiveFoilModel = "";
    public String positiveFoilSupplier = "";
    public String positiveFoilSize = "";
    public String negativeFoilModel = "";
    public String negativeFoilSupplier = "";
    public String negativeFoilSize = "";
    public String guildPinModel = "";
    public String guildPinSupplier = "";
    public String guildPinRemark = "";
    public String electrolyticPaper = "";
    public String electrolyticPaperSupplier = "";
    public String electrolyticPaperRemark = "";
    public String electrolyticPaper2 = "";
    public String electrolyticPaper2Supplier = "";
    public String electrolyticPaper2Remark = "";
    public String electrolyte = "";
    public String electrolyteSupplier = "";
    public String electrolyteRemark = "";
    public String rubberStopper = "";
    public String rubberStopperSupplier = "";
    public String rubberStopperRemark = "";
    public String aluminumShell = "";
    public String aluminumShellSupplier = "";
    public String aluminumShellRemark = "";
    public String casing = "";
    public String casingSupplier = "";
    public String casingRemark = "";

    public FlowCardData() {

    }

    public FlowCardData(String batchId, String productCode, String customerCode, String date, String specification, String size, String deviceId, String amount, String capacityRange, String positiveFoil, String positiveFoilModel, String positiveFoilSupplier, String positiveFoilSize, String negativeFoilModel, String negativeFoilSupplier, String negativeFoilSize, String guildPinModel, String guildPinSupplier, String guildPinRemark, String electrolyticPaper, String electrolyticPaperSupplier, String electrolyticPaperRemark, String electrolyticPaper2, String electrolyticPaper2Supplier, String electrolyticPaper2Remark, String electrolyte, String electrolyteSupplier, String electrolyteRemark, String rubberStopper, String rubberStopperSupplier, String rubberStopperRemark, String aluminumShell, String aluminumShellSupplier, String aluminumShellRemark, String casing, String casingSupplier, String casingRemark) {
        this.batchId = batchId;
        this.productCode = productCode;
        this.customerCode = customerCode;
        this.date = date;
        this.specification = specification;
        this.size = size;
        this.deviceId = deviceId;
        this.amount = amount;
        this.capacityRange = capacityRange;
        this.positiveFoil = positiveFoil;
        this.positiveFoilModel = positiveFoilModel;
        this.positiveFoilSupplier = positiveFoilSupplier;
        this.positiveFoilSize = positiveFoilSize;
        this.negativeFoilModel = negativeFoilModel;
        this.negativeFoilSupplier = negativeFoilSupplier;
        this.negativeFoilSize = negativeFoilSize;
        this.guildPinModel = guildPinModel;
        this.guildPinSupplier = guildPinSupplier;
        this.guildPinRemark = guildPinRemark;
        this.electrolyticPaper = electrolyticPaper;
        this.electrolyticPaperSupplier = electrolyticPaperSupplier;
        this.electrolyticPaperRemark = electrolyticPaperRemark;
        this.electrolyticPaper2 = electrolyticPaper2;
        this.electrolyticPaper2Supplier = electrolyticPaper2Supplier;
        this.electrolyticPaper2Remark = electrolyticPaper2Remark;
        this.electrolyte = electrolyte;
        this.electrolyteSupplier = electrolyteSupplier;
        this.electrolyteRemark = electrolyteRemark;
        this.rubberStopper = rubberStopper;
        this.rubberStopperSupplier = rubberStopperSupplier;
        this.rubberStopperRemark = rubberStopperRemark;
        this.aluminumShell = aluminumShell;
        this.aluminumShellSupplier = aluminumShellSupplier;
        this.aluminumShellRemark = aluminumShellRemark;
        this.casing = casing;
        this.casingSupplier = casingSupplier;
        this.casingRemark = casingRemark;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


    public String getCapacityRange() {
        return capacityRange;
    }

    public void setCapacityRange(String capacityRange) {
        this.capacityRange = capacityRange;
    }

    public String getPositiveFoil() {
        return positiveFoil;
    }

    public void setPositiveFoil(String positiveFoil) {
        this.positiveFoil = positiveFoil;
    }

    public String getPositiveFoilModel() {
        return positiveFoilModel;
    }

    public void setPositiveFoilModel(String positiveFoilModel) {
        this.positiveFoilModel = positiveFoilModel;
    }

    public String getPositiveFoilSupplier() {
        return positiveFoilSupplier;
    }

    public void setPositiveFoilSupplier(String positiveFoilSupplier) {
        this.positiveFoilSupplier = positiveFoilSupplier;
    }

    public String getPositiveFoilSize() {
        return positiveFoilSize;
    }

    public void setPositiveFoilSize(String positiveFoilSize) {
        this.positiveFoilSize = positiveFoilSize;
    }

    public String getNegativeFoilModel() {
        return negativeFoilModel;
    }

    public void setNegativeFoilModel(String negativeFoilModel) {
        this.negativeFoilModel = negativeFoilModel;
    }

    public String getNegativeFoilSupplier() {
        return negativeFoilSupplier;
    }

    public void setNegativeFoilSupplier(String negativeFoilSupplier) {
        this.negativeFoilSupplier = negativeFoilSupplier;
    }

    public String getNegativeFoilSize() {
        return negativeFoilSize;
    }

    public void setNegativeFoilSize(String negativeFoilSize) {
        this.negativeFoilSize = negativeFoilSize;
    }

    public String getGuildPinModel() {
        return guildPinModel;
    }

    public void setGuildPinModel(String guildPinModel) {
        this.guildPinModel = guildPinModel;
    }

    public String getGuildPinSupplier() {
        return guildPinSupplier;
    }

    public void setGuildPinSupplier(String guildPinSupplier) {
        this.guildPinSupplier = guildPinSupplier;
    }

    public String getGuildPinRemark() {
        return guildPinRemark;
    }

    public void setGuildPinRemark(String guildPinRemark) {
        this.guildPinRemark = guildPinRemark;
    }

    public String getElectrolyticPaper() {
        return electrolyticPaper;
    }

    public void setElectrolyticPaper(String electrolyticPaper) {
        this.electrolyticPaper = electrolyticPaper;
    }

    public String getElectrolyticPaperSupplier() {
        return electrolyticPaperSupplier;
    }

    public void setElectrolyticPaperSupplier(String electrolyticPaperSupplier) {
        this.electrolyticPaperSupplier = electrolyticPaperSupplier;
    }

    public String getElectrolyticPaperRemark() {
        return electrolyticPaperRemark;
    }

    public void setElectrolyticPaperRemark(String electrolyticPaperRemark) {
        this.electrolyticPaperRemark = electrolyticPaperRemark;
    }

    public String getElectrolyte() {
        return electrolyte;
    }

    public void setElectrolyte(String electrolyte) {
        this.electrolyte = electrolyte;
    }

    public String getElectrolyteSupplier() {
        return electrolyteSupplier;
    }

    public void setElectrolyteSupplier(String electrolyteSupplier) {
        this.electrolyteSupplier = electrolyteSupplier;
    }

    public String getElectrolyteRemark() {
        return electrolyteRemark;
    }

    public void setElectrolyteRemark(String electrolyteRemark) {
        this.electrolyteRemark = electrolyteRemark;
    }

    public String getRubberStopper() {
        return rubberStopper;
    }

    public void setRubberStopper(String rubberStopper) {
        this.rubberStopper = rubberStopper;
    }

    public String getRubberStopperSupplier() {
        return rubberStopperSupplier;
    }

    public void setRubberStopperSupplier(String rubberStopperSupplier) {
        this.rubberStopperSupplier = rubberStopperSupplier;
    }

    public String getRubberStopperRemark() {
        return rubberStopperRemark;
    }

    public void setRubberStopperRemark(String rubberStopperRemark) {
        this.rubberStopperRemark = rubberStopperRemark;
    }

    public String getAluminumShell() {
        return aluminumShell;
    }

    public void setAluminumShell(String aluminumShell) {
        this.aluminumShell = aluminumShell;
    }

    public String getAluminumShellSupplier() {
        return aluminumShellSupplier;
    }

    public void setAluminumShellSupplier(String aluminumShellSupplier) {
        this.aluminumShellSupplier = aluminumShellSupplier;
    }

    public String getAluminumShellRemark() {
        return aluminumShellRemark;
    }

    public void setAluminumShellRemark(String aluminumShellRemark) {
        this.aluminumShellRemark = aluminumShellRemark;
    }

    public String getCasing() {
        return casing;
    }

    public void setCasing(String casing) {
        this.casing = casing;
    }

    public String getCasingSupplier() {
        return casingSupplier;
    }

    public void setCasingSupplier(String casingSupplier) {
        this.casingSupplier = casingSupplier;
    }

    public String getCasingRemark() {
        return casingRemark;
    }

    public void setCasingRemark(String casingRemark) {
        this.casingRemark = casingRemark;
    }
}
