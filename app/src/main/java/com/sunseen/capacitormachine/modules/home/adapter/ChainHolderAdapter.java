package com.sunseen.capacitormachine.modules.home.adapter;


import android.view.View;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.modules.home.bean.HolderAdapterBean;

import java.util.List;


/**
 * @author zest
 */
public class ChainHolderAdapter extends BaseQuickAdapter<HolderAdapterBean, BaseViewHolder> {

    public ChainHolderAdapter(int layoutResId, @Nullable List<HolderAdapterBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HolderAdapterBean item) {
        helper.setText(R.id.tv_holder_name, String.format(helper.itemView.getContext().getString(R.string.holder_format), item.getHolderId()));
        helper.getView(R.id.img_warn).setVisibility(item.isNormal() ? View.GONE : View.VISIBLE);
    }
}
