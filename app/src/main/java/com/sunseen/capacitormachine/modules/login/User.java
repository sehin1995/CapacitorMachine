package com.sunseen.capacitormachine.modules.login;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

/**
 * @author zest
 */
@Entity
public class User {

    public static final int ADMIN = 0;
    public static final int PROFESSOR = 1;
    public static final int USER = 2;

    @IntDef({ADMIN, PROFESSOR, USER})
    public @interface UserLevel {
    }

    @Id
    public long id;

    private String name;

    private String password;

    @UserLevel
    private
    int level;

    public User(@NonNull String name, @NonNull String password, @UserLevel int level) {
        this.name = name;
        this.password = password;
        this.level = level;
    }


    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getPassword() {
        return password;
    }

    public void setPassword(@NonNull String password) {
        this.password = password;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
