package com.sunseen.capacitormachine.modules.login;

import android.widget.Toast;

import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.databinding.FragmentLoginBinding;
import com.sunseen.capacitormachine.MainFragment;
import com.sunseen.capacitormachine.common.BaseFragment;

import androidx.databinding.ViewDataBinding;

/**
 * @author zest
 * */
public class LoginFragment extends BaseFragment {
    @Override
    protected int setLayout() {
        return R.layout.fragment_login;
    }

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        final FragmentLoginBinding binding = (FragmentLoginBinding) viewDataBinding;
        LoginHandler loginHandler = new LoginHandler((boolean validState) -> {
            if (validState) {
                replaceFragment(new MainFragment(), false);
            } else {
                Toast.makeText(getContext(), "用户名或密码错误", Toast.LENGTH_LONG).show();
            }
        });
        binding.setUser(loginHandler.getLocalUser());
        binding.setLoginHandler(loginHandler);
    }
}
