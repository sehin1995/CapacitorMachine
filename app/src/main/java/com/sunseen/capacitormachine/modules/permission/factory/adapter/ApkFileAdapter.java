package com.sunseen.capacitormachine.modules.permission.factory.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sunseen.capacitormachine.R;

import java.io.File;

public class ApkFileAdapter extends BaseQuickAdapter<File, BaseViewHolder> {
    public ApkFileAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, File item) {
            helper.setText(R.id.tv_file_name,item.getName());
    }
}
