package com.sunseen.capacitormachine.commumication.tcp.event;

public class DetectedImplosionEvent {
    private int holderId;
    private int pos;

    public DetectedImplosionEvent(int holderId, int pos) {
        this.holderId = holderId;
        this.pos = pos;
    }

    public int getHolderId() {
        return holderId;
    }

    public void setHolderId(int holderId) {
        this.holderId = holderId;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }
}
