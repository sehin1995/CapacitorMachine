package com.sunseen.capacitormachine.commumication.serialport.event;

import android.util.SparseIntArray;

public class SurgeDataEvent {
    private String uid;
    /**
     * surgeType == 1 浪涌1
     * surgeType == 2 浪涌2
     */
    private int surgeType;

    /**
     * 浪涌结果 02通过，01失败，00接触不良或空位
     */
    private int surgeResult;

    //浪涌基准线
    private int baseVoltage;

    private SparseIntArray surgeDatas;

    public SurgeDataEvent(String uid, int baseVoltage, int surgeType, int surgeResult, SparseIntArray surgeDatas) {
        this.uid = uid;
        this.baseVoltage = baseVoltage;
        this.surgeType = surgeType;
        this.surgeResult = surgeResult;
        this.surgeDatas = surgeDatas;
    }

    public String getUid() {
        return uid;
    }

    public int getBaseVoltage() {
        return baseVoltage;
    }

    public int getSurgeType() {
        return surgeType;
    }

    public int getSurgeResult() {
        return surgeResult;
    }

    public SparseIntArray getSurgeDatas() {
        return surgeDatas;
    }
}
