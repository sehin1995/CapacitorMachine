package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class OpenCircuitJsonObj {
    public String type = "openCircuit";
    public int time;
    public CircuitData data;

    public OpenCircuitJsonObj(String type,int time, CircuitData data) {
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public CircuitData getData() {
        return data;
    }

    public void setData(CircuitData data) {
        this.data = data;
    }
}
