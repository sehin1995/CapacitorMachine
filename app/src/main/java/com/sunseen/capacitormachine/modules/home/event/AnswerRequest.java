package com.sunseen.capacitormachine.modules.home.event;

import com.sunseen.capacitormachine.modules.parameter.bean.ProcessParameter;

public class AnswerRequest {
    /**
     * start == true : 开始启动请求的无参数回复
     * start == false : 换料请求的无参数回复
     */
    private boolean start;

    /**
     * 是否有参数
     * true 有参数
     * false 无参数
     */
    private boolean hasParameter;

    private ProcessParameter parameter;

    public AnswerRequest(boolean start, boolean hasParameter, ProcessParameter parameter) {
        this.start = start;
        this.hasParameter = hasParameter;
        this.parameter = parameter;
    }

    public boolean isStart() {
        return start;
    }

    public boolean isHasParameter() {
        return hasParameter;
    }

    public ProcessParameter getParameter() {
        return parameter;
    }
}
