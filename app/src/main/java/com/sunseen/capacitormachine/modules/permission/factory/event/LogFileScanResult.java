package com.sunseen.capacitormachine.modules.permission.factory.event;

import java.io.File;

public class LogFileScanResult {
    private boolean hasFile;
    private File[] normalLogFiles;
    private File[] crashLogFiles;

    public LogFileScanResult(boolean hasFile) {
        this.hasFile = hasFile;
    }

    public LogFileScanResult(boolean hasFile, File[] normalLogFiles, File[] crashLogFiles) {
        this.hasFile = hasFile;
        this.normalLogFiles = normalLogFiles;
        this.crashLogFiles = crashLogFiles;
    }

    public boolean isHasFile() {
        return hasFile;
    }

    public File[] getNormalLogFiles() {
        return normalLogFiles;
    }

    public File[] getCrashLogFiles() {
        return crashLogFiles;
    }
}
