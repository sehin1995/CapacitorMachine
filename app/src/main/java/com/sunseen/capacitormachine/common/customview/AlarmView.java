package com.sunseen.capacitormachine.common.customview;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.commumication.serialport.PlcService;
import com.sunseen.capacitormachine.commumication.serialport.event.CmdEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class AlarmView {
    private WindowManager.LayoutParams mLayoutParams;
    private WindowManager mWindowManager;

    private LinearLayout linearLayout;
    private TextView titleView;
    private AlarmAdapter alarmAdapter;
    private AlarmAdapter offLineAlarmAdapter;

    public AlarmView(Context context) {
        initParams();
        linearLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.layout_alarm, null, false);
        titleView = linearLayout.findViewById(R.id.tv_title);
        RecyclerView offLineAlarmRv = linearLayout.findViewById(R.id.rv_off_line_alarm);
        offLineAlarmRv.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        offLineAlarmAdapter = new AlarmAdapter(R.layout.layout_item_alarm);
        offLineAlarmRv.setAdapter(offLineAlarmAdapter);
        RecyclerView alarmRv = linearLayout.findViewById(R.id.rv_alarm);
        alarmRv.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        alarmAdapter = new AlarmAdapter(R.layout.layout_item_alarm);
        alarmRv.setAdapter(alarmAdapter);
        Button confirmBtn = linearLayout.findViewById(R.id.btn_confirm);
        confirmBtn.setOnClickListener((View v) -> {
            EventBus.getDefault().post(new CmdEvent(PlcService.CLEAR_ALARM));
            hide();
        });
        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    }

    private void initParams() {
        mLayoutParams = new WindowManager.LayoutParams();
        mLayoutParams.flags = mLayoutParams.flags
                | WindowManager.LayoutParams.FLAG_FULLSCREEN;
        mLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        mLayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        mLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        mLayoutParams.gravity = Gravity.CENTER;
        mLayoutParams.format = PixelFormat.RGBA_8888;
    }

    private boolean showing = false;

    public void show() {
        if (!showing) {
            mWindowManager.addView(linearLayout, mLayoutParams);
            showing = true;
        }
    }

    public void hide() {
        if (showing) {
            mWindowManager.removeView(linearLayout);
            alarmAdapter.setNewData(new ArrayList<>());
            offLineAlarmAdapter.setNewData(new ArrayList<>());
            showing = false;
        }
    }

    public void tryHide() {
        if (showing) {
            if (alarmAdapter != null && alarmAdapter.getData().isEmpty()) {
                hide();
            }
        }
    }

    public boolean isShowing() {
        return showing;
    }

    public void setTitle(String str) {
        if (titleView != null) {
            titleView.setText(str);
        }
    }

    public void setAlarmInfo(List<String> alarmInfo) {
        alarmAdapter.setNewData(alarmInfo);
    }


    private boolean[] offLineFlags = new boolean[]{false, false, false, false, false};

    public boolean offLineFlagDifferent(boolean[] flags) {
        boolean diff = false;
        if (flags != null && flags.length == offLineFlags.length) {
            for (int i = 0, size = flags.length; i < size; i++) {
                if (flags[i] != offLineFlags[i]) {
                    diff = true;
                    offLineFlags[i] = flags[i];
                }
            }
        }
        return diff;
    }

    public void setOffLineInfo(List<String> offLineInfo) {
        offLineAlarmAdapter.setNewData(offLineInfo);
    }
}
