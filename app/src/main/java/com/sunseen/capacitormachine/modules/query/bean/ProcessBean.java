package com.sunseen.capacitormachine.modules.query.bean;

public class ProcessBean {

    /**
     * id : 1
     * batchId : 666666
     * deviceNo : 6812B066
     * V1 : 900
     * I1 : 102
     * V2 : 1000
     * I2 : 112
     * V3 : 1200
     * I3 : 114
     * V4 : 1300
     * I4 : 115
     * V5 : 1400
     * I5 : 117
     * V6 : 1600
     * I6 : 118
     * V7 : 1800
     * I7 : 120
     * V8 : 1900
     * I8 : 123
     * V9 : 2000
     * I9 : 127
     * V10 : 2200
     * I10 : 128
     * V11 : 2400
     * I11 : 139
     * V12 : 2600
     * I12 : 159
     * V13 : 2800
     * I13 : 168
     * V14 : 3000
     * I14 : 180
     * V15 : 3200
     * I15 : 200
     * V16 : 3400
     * I16 : 220
     * V17 : 3500
     * I17 : 230
     * V18 : 3600
     * I18 : 250
     * chargeV : 3800
     * surgeV : 4000
     * testV : 4200
     * shortCircuitV : 120
     * openCircuitV : 4000
     * leakageUpperLimit : 123
     * leakageLowerLimit : 143
     * capacitorNominal : 123
     * premiumLimit : 234
     * superiorLowerLimit : 132
     * goodProductUpperLimit : 234
     * goodProductLowerLimit : 345
     * unAgedVoltage : 456
     * lossAngelUpperLimit : 453
     * upperImpedanceLimit : 432
     * goodProduct2UpperLimit : 566
     * goodProduct2LowerLimit : 345
     * agingTime : 10
     * codingMark : JB888866
     * codingStartNumber : 00000000
     * surgeUpperLimit : 345
     * upperTemperatureLimit : 840
     * instrumentFrequency : 120
     * ovenTemperature : 840
     * implosionTime : 10
     * implosionVoltageUpperLimit : 234
     * implosionVoltageLowerLimit : 235
     * implosionCurrentUpperLimit : 345
     * implosionCurrentLowerLimit : 356
     * time : 1568706990
     */

    private String batchId;
    private String deviceNo;
    private String V1;
    private String I1;
    private String V2;
    private String I2;
    private String V3;
    private String I3;
    private String V4;
    private String I4;
    private String V5;
    private String I5;
    private String V6;
    private String I6;
    private String V7;
    private String I7;
    private String V8;
    private String I8;
    private String V9;
    private String I9;
    private String V10;
    private String I10;
    private String V11;
    private String I11;
    private String V12;
    private String I12;
    private String V13;
    private String I13;
    private String V14;
    private String I14;
    private String V15;
    private String I15;
    private String V16;
    private String I16;
    private String V17;
    private String I17;
    private String V18;
    private String I18;
    private String chargeV;
    private String chargeI;
    private String surgeV;
    private String surgeI;
    private String testV;
    private String testI;
    private String shortCircuitV;//短路电压
    private String openCircuitV;//开路电压
    private String leakageUpperLimit;//漏电上限
    private String leakageMiddleLimit;//漏电中限
    private String leakageLowerLimit;//漏电下限
    private String capacitorNominal;//电容标称
    private String premiumLimit;//
    private String superiorLowerLimit;
    private String goodProductUpperLimit;//良品上限
    private String goodProductLowerLimit;//良品下限
    private String unAgedVoltage;//未老化电压
    private String lossAngelUpperLimit;//损失上限
    private String upperImpedanceLimit;//阻抗上限
    private String goodProduct2UpperLimit;//良品2上限
    private String goodProduct2LowerLimit;//良品2下限
    private String agingTime;//老化时间
    private String codingMark;//编码标志
    private String codingStartNumber;//编码起始数
    private String surgeUpperLimit;//浪涌上限
    private String upperTemperatureLimit;//温度上限
    private String instrumentFrequency;//仪表频率
    private String ovenTemperature;//烤箱温度
    private String implosionTime;//内爆时间
    private String implosionVoltageUpperLimit;//内爆电压上限
    private String implosionVoltageLowerLimit;//内爆电压下限
    private String implosionCurrentUpperLimit;//内爆电流上限
    private String implosionCurrentLowerLimit;//内爆电压下限
    private String time;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getV1() {
        return V1;
    }

    public void setV1(String V1) {
        this.V1 = V1;
    }

    public String getI1() {
        return I1;
    }

    public void setI1(String I1) {
        this.I1 = I1;
    }

    public String getV2() {
        return V2;
    }

    public void setV2(String V2) {
        this.V2 = V2;
    }

    public String getI2() {
        return I2;
    }

    public void setI2(String I2) {
        this.I2 = I2;
    }

    public String getV3() {
        return V3;
    }

    public void setV3(String V3) {
        this.V3 = V3;
    }

    public String getI3() {
        return I3;
    }

    public void setI3(String I3) {
        this.I3 = I3;
    }

    public String getV4() {
        return V4;
    }

    public void setV4(String V4) {
        this.V4 = V4;
    }

    public String getI4() {
        return I4;
    }

    public void setI4(String I4) {
        this.I4 = I4;
    }

    public String getV5() {
        return V5;
    }

    public void setV5(String V5) {
        this.V5 = V5;
    }

    public String getI5() {
        return I5;
    }

    public void setI5(String I5) {
        this.I5 = I5;
    }

    public String getV6() {
        return V6;
    }

    public void setV6(String V6) {
        this.V6 = V6;
    }

    public String getI6() {
        return I6;
    }

    public void setI6(String I6) {
        this.I6 = I6;
    }

    public String getV7() {
        return V7;
    }

    public void setV7(String V7) {
        this.V7 = V7;
    }

    public String getI7() {
        return I7;
    }

    public void setI7(String I7) {
        this.I7 = I7;
    }

    public String getV8() {
        return V8;
    }

    public void setV8(String V8) {
        this.V8 = V8;
    }

    public String getI8() {
        return I8;
    }

    public void setI8(String I8) {
        this.I8 = I8;
    }

    public String getV9() {
        return V9;
    }

    public void setV9(String V9) {
        this.V9 = V9;
    }

    public String getI9() {
        return I9;
    }

    public void setI9(String I9) {
        this.I9 = I9;
    }

    public String getV10() {
        return V10;
    }

    public void setV10(String V10) {
        this.V10 = V10;
    }

    public String getI10() {
        return I10;
    }

    public void setI10(String I10) {
        this.I10 = I10;
    }

    public String getV11() {
        return V11;
    }

    public void setV11(String V11) {
        this.V11 = V11;
    }

    public String getI11() {
        return I11;
    }

    public void setI11(String I11) {
        this.I11 = I11;
    }

    public String getV12() {
        return V12;
    }

    public void setV12(String V12) {
        this.V12 = V12;
    }

    public String getI12() {
        return I12;
    }

    public void setI12(String I12) {
        this.I12 = I12;
    }

    public String getV13() {
        return V13;
    }

    public void setV13(String V13) {
        this.V13 = V13;
    }

    public String getI13() {
        return I13;
    }

    public void setI13(String I13) {
        this.I13 = I13;
    }

    public String getV14() {
        return V14;
    }

    public void setV14(String V14) {
        this.V14 = V14;
    }

    public String getI14() {
        return I14;
    }

    public void setI14(String I14) {
        this.I14 = I14;
    }

    public String getV15() {
        return V15;
    }

    public void setV15(String V15) {
        this.V15 = V15;
    }

    public String getI15() {
        return I15;
    }

    public void setI15(String I15) {
        this.I15 = I15;
    }

    public String getV16() {
        return V16;
    }

    public void setV16(String V16) {
        this.V16 = V16;
    }

    public String getI16() {
        return I16;
    }

    public void setI16(String I16) {
        this.I16 = I16;
    }

    public String getV17() {
        return V17;
    }

    public void setV17(String V17) {
        this.V17 = V17;
    }

    public String getI17() {
        return I17;
    }

    public void setI17(String I17) {
        this.I17 = I17;
    }

    public String getV18() {
        return V18;
    }

    public void setV18(String V18) {
        this.V18 = V18;
    }

    public String getI18() {
        return I18;
    }

    public void setI18(String I18) {
        this.I18 = I18;
    }

    public String getChargeV() {
        return chargeV;
    }

    public void setChargeV(String chargeV) {
        this.chargeV = chargeV;
    }

    public String getSurgeV() {
        return surgeV;
    }

    public void setSurgeV(String surgeV) {
        this.surgeV = surgeV;
    }

    public String getTestV() {
        return testV;
    }

    public void setTestV(String testV) {
        this.testV = testV;
    }

    public String getChargeI() {
        return chargeI;
    }

    public void setChargeI(String chargeI) {
        this.chargeI = chargeI;
    }

    public String getSurgeI() {
        return surgeI;
    }

    public void setSurgeI(String surgeI) {
        this.surgeI = surgeI;
    }

    public String getTestI() {
        return testI;
    }

    public void setTestI(String testI) {
        this.testI = testI;
    }

    public String getShortCircuitV() {
        return shortCircuitV;
    }

    public void setShortCircuitV(String shortCircuitV) {
        this.shortCircuitV = shortCircuitV;
    }

    public String getOpenCircuitV() {
        return openCircuitV;
    }

    public void setOpenCircuitV(String openCircuitV) {
        this.openCircuitV = openCircuitV;
    }

    public String getLeakageUpperLimit() {
        return leakageUpperLimit;
    }

    public void setLeakageUpperLimit(String leakageUpperLimit) {
        this.leakageUpperLimit = leakageUpperLimit;
    }

    public String getLeakageLowerLimit() {
        return leakageLowerLimit;
    }

    public void setLeakageLowerLimit(String leakageLowerLimit) {
        this.leakageLowerLimit = leakageLowerLimit;
    }

    public String getCapacitorNominal() {
        return capacitorNominal;
    }

    public void setCapacitorNominal(String capacitorNominal) {
        this.capacitorNominal = capacitorNominal;
    }

    public String getPremiumLimit() {
        return premiumLimit;
    }

    public void setPremiumLimit(String premiumLimit) {
        this.premiumLimit = premiumLimit;
    }

    public String getSuperiorLowerLimit() {
        return superiorLowerLimit;
    }

    public void setSuperiorLowerLimit(String superiorLowerLimit) {
        this.superiorLowerLimit = superiorLowerLimit;
    }

    public String getGoodProductUpperLimit() {
        return goodProductUpperLimit;
    }

    public void setGoodProductUpperLimit(String goodProductUpperLimit) {
        this.goodProductUpperLimit = goodProductUpperLimit;
    }

    public String getGoodProductLowerLimit() {
        return goodProductLowerLimit;
    }

    public void setGoodProductLowerLimit(String goodProductLowerLimit) {
        this.goodProductLowerLimit = goodProductLowerLimit;
    }

    public String getUnAgedVoltage() {
        return unAgedVoltage;
    }

    public void setUnAgedVoltage(String unAgedVoltage) {
        this.unAgedVoltage = unAgedVoltage;
    }

    public String getLossAngelUpperLimit() {
        return lossAngelUpperLimit;
    }

    public void setLossAngelUpperLimit(String lossAngelUpperLimit) {
        this.lossAngelUpperLimit = lossAngelUpperLimit;
    }

    public String getUpperImpedanceLimit() {
        return upperImpedanceLimit;
    }

    public void setUpperImpedanceLimit(String upperImpedanceLimit) {
        this.upperImpedanceLimit = upperImpedanceLimit;
    }

    public String getGoodProduct2UpperLimit() {
        return goodProduct2UpperLimit;
    }

    public void setGoodProduct2UpperLimit(String goodProduct2UpperLimit) {
        this.goodProduct2UpperLimit = goodProduct2UpperLimit;
    }

    public String getGoodProduct2LowerLimit() {
        return goodProduct2LowerLimit;
    }

    public void setGoodProduct2LowerLimit(String goodProduct2LowerLimit) {
        this.goodProduct2LowerLimit = goodProduct2LowerLimit;
    }

    public String getAgingTime() {
        return agingTime;
    }

    public void setAgingTime(String agingTime) {
        this.agingTime = agingTime;
    }

    public String getCodingMark() {
        return codingMark;
    }

    public void setCodingMark(String codingMark) {
        this.codingMark = codingMark;
    }

    public String getCodingStartNumber() {
        return codingStartNumber;
    }

    public void setCodingStartNumber(String codingStartNumber) {
        this.codingStartNumber = codingStartNumber;
    }

    public String getSurgeUpperLimit() {
        return surgeUpperLimit;
    }

    public void setSurgeUpperLimit(String surgeUpperLimit) {
        this.surgeUpperLimit = surgeUpperLimit;
    }

    public String getUpperTemperatureLimit() {
        return upperTemperatureLimit;
    }

    public void setUpperTemperatureLimit(String upperTemperatureLimit) {
        this.upperTemperatureLimit = upperTemperatureLimit;
    }

    public String getInstrumentFrequency() {
        return instrumentFrequency;
    }

    public void setInstrumentFrequency(String instrumentFrequency) {
        this.instrumentFrequency = instrumentFrequency;
    }

    public String getOvenTemperature() {
        return ovenTemperature;
    }

    public void setOvenTemperature(String ovenTemperature) {
        this.ovenTemperature = ovenTemperature;
    }

    public String getImplosionTime() {
        return implosionTime;
    }

    public void setImplosionTime(String implosionTime) {
        this.implosionTime = implosionTime;
    }

    public String getImplosionVoltageUpperLimit() {
        return implosionVoltageUpperLimit;
    }

    public void setImplosionVoltageUpperLimit(String implosionVoltageUpperLimit) {
        this.implosionVoltageUpperLimit = implosionVoltageUpperLimit;
    }

    public String getImplosionVoltageLowerLimit() {
        return implosionVoltageLowerLimit;
    }

    public void setImplosionVoltageLowerLimit(String implosionVoltageLowerLimit) {
        this.implosionVoltageLowerLimit = implosionVoltageLowerLimit;
    }

    public String getImplosionCurrentUpperLimit() {
        return implosionCurrentUpperLimit;
    }

    public void setImplosionCurrentUpperLimit(String implosionCurrentUpperLimit) {
        this.implosionCurrentUpperLimit = implosionCurrentUpperLimit;
    }

    public String getImplosionCurrentLowerLimit() {
        return implosionCurrentLowerLimit;
    }

    public void setImplosionCurrentLowerLimit(String implosionCurrentLowerLimit) {
        this.implosionCurrentLowerLimit = implosionCurrentLowerLimit;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLeakageMiddleLimit() {
        return leakageMiddleLimit;
    }

    public void setLeakageMiddleLimit(String leakageMiddleLimit) {
        this.leakageMiddleLimit = leakageMiddleLimit;
    }
}
