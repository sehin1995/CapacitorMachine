package com.sunseen.capacitormachine.modules.parameter.productivetask;

import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.databinding.ViewDataBinding;

import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.commumication.serialport.PlcService;
import com.sunseen.capacitormachine.commumication.serialport.event.CmdEvent;
import com.sunseen.capacitormachine.data.DataUtil;
import com.sunseen.capacitormachine.databinding.FragmentEditFlagBinding;
import com.sunseen.capacitormachine.modules.parameter.productivetask.event.FlagSavedEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class FlagSwitchFragment extends BaseFragment
        implements CompoundButton.OnCheckedChangeListener {

    @Override
    protected int setLayout() {
        return R.layout.fragment_edit_flag;
    }

    private FragmentEditFlagBinding binding;

    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        binding = (FragmentEditFlagBinding) viewDataBinding;
        setCheckBox();
        binding.shortCircuitCb.setOnCheckedChangeListener(this);
        binding.openCircuitCb.setOnCheckedChangeListener(this);
        binding.unAgedCb.setOnCheckedChangeListener(this);
        binding.leakCurrentCb.setOnCheckedChangeListener(this);
        binding.capacityCb.setOnCheckedChangeListener(this);
        binding.lossAngleCb.setOnCheckedChangeListener(this);
        binding.esrCb.setOnCheckedChangeListener(this);
        binding.surgeCb.setOnCheckedChangeListener(this);
        binding.implosionCb.setOnCheckedChangeListener(this);
        binding.powerSupplyCb.setOnCheckedChangeListener(this);
        binding.temperatureCb.setOnCheckedChangeListener(this);
        binding.camera1Cb.setOnCheckedChangeListener(this);
        binding.camera2Cb.setOnCheckedChangeListener(this);
        binding.camera3Cb.setOnCheckedChangeListener(this);
        binding.sprayCodeCb.setOnCheckedChangeListener(this);
        binding.machineRunModeRg.setOnCheckedChangeListener(
                (RadioGroup group, int checkedId) -> {
                    if (checkedId == R.id.auto_mode_rb) {
                        DataUtil.flagBean.setAutoModeEnable(true);
                    } else if (checkedId == R.id.sort_mode_rb) {
                        DataUtil.flagBean.setAutoModeEnable(false);
                    }
                });
        binding.saveFlagBtn.setOnCheckedChangeListener(this);
        EventBus.getDefault().register(this);
    }

    private void setCheckBox() {
        binding.shortCircuitCb.setChecked(DataUtil.flagBean.isShortCircuitEnable());
        binding.shortCircuitCb.setText(DataUtil.flagBean.isShortCircuitEnable() ? R.string.short_circuit_enable : R.string.short_circuit_disable);
        binding.openCircuitCb.setChecked(DataUtil.flagBean.isOpenCircuitEnable());
        binding.openCircuitCb.setText(DataUtil.flagBean.isOpenCircuitEnable() ? R.string.open_circuit_enable : R.string.open_circuit_disable);
        binding.unAgedCb.setChecked(DataUtil.flagBean.isUnAgeEnable());
        binding.unAgedCb.setText(DataUtil.flagBean.isUnAgeEnable() ? R.string.un_age_enable : R.string.un_age_disable);
        binding.leakCurrentCb.setChecked(DataUtil.flagBean.isLeakCurrentEnable());
        binding.leakCurrentCb.setText(DataUtil.flagBean.isLeakCurrentEnable() ? R.string.leak_current_enable : R.string.leak_current_disable);
        binding.capacityCb.setChecked(DataUtil.flagBean.isCapacityEnable());
        binding.capacityCb.setText(DataUtil.flagBean.isCapacityEnable() ? R.string.capacity_enable : R.string.capacity_disable);
        binding.lossAngleCb.setChecked(DataUtil.flagBean.isLossAngleEnable());
        binding.lossAngleCb.setText(DataUtil.flagBean.isLossAngleEnable() ? R.string.loss_angle_enable : R.string.loss_angle_disable);
        binding.esrCb.setChecked(DataUtil.flagBean.isEsrEnable());
        binding.esrCb.setText(DataUtil.flagBean.isEsrEnable() ? R.string.esr_enable : R.string.esr_disable);
        binding.surgeCb.setChecked(DataUtil.flagBean.isSurgeEnable());
        binding.surgeCb.setText(DataUtil.flagBean.isSurgeEnable() ? R.string.surge_enable : R.string.surge_disable);
        binding.implosionCb.setChecked(DataUtil.flagBean.isImplosionEnable());
        binding.implosionCb.setText(DataUtil.flagBean.isImplosionEnable() ? R.string.implosion_enable : R.string.implosion_disable);
        binding.powerSupplyCb.setChecked(DataUtil.flagBean.isPowerSupplyAlarmEnable());
        binding.powerSupplyCb.setText(DataUtil.flagBean.isPowerSupplyAlarmEnable() ? R.string.power_supply_alarm_enable : R.string.power_supply_alarm_disable);
        binding.temperatureCb.setChecked(DataUtil.flagBean.isTemperatureAlarmEnable());
        binding.temperatureCb.setText(DataUtil.flagBean.isTemperatureAlarmEnable() ? R.string.temperature_alarm_enable : R.string.temperature_alarm_disable);
        binding.camera1Cb.setChecked(DataUtil.flagBean.isCamera1Enable());
        binding.camera1Cb.setText(DataUtil.flagBean.isCamera1Enable() ? R.string.camera_1_enable : R.string.camera_1_disable);
        binding.camera2Cb.setChecked(DataUtil.flagBean.isCamera2Enable());
        binding.camera2Cb.setText(DataUtil.flagBean.isCamera2Enable() ? R.string.camera_2_enable : R.string.camera_2_disable);
        binding.camera3Cb.setChecked(DataUtil.flagBean.isCamera3Enable());
        binding.camera3Cb.setText(DataUtil.flagBean.isCamera3Enable() ? R.string.camera_3_enable : R.string.camera_3_disable);
        binding.sprayCodeCb.setChecked(DataUtil.flagBean.isSprayCodeEnable());
        binding.sprayCodeCb.setText(DataUtil.flagBean.isSprayCodeEnable() ? R.string.spray_code_enable : R.string.spray_code_disable);
        if (DataUtil.flagBean.isAutoModeEnable()) {
            binding.machineRunModeRg.check(R.id.auto_mode_rb);
        } else {
            binding.machineRunModeRg.check(R.id.sort_mode_rb);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.save_flag_btn: {
                binding.shortCircuitCb.setEnabled(isChecked);
                binding.openCircuitCb.setEnabled(isChecked);
                binding.unAgedCb.setEnabled(isChecked);
                binding.leakCurrentCb.setEnabled(isChecked);
                binding.capacityCb.setEnabled(isChecked);
                binding.lossAngleCb.setEnabled(isChecked);
                binding.esrCb.setEnabled(isChecked);
                binding.surgeCb.setEnabled(isChecked);
                binding.implosionCb.setEnabled(isChecked);
                binding.powerSupplyCb.setEnabled(isChecked);
                binding.temperatureCb.setEnabled(isChecked);
                binding.camera1Cb.setEnabled(isChecked);
                binding.camera2Cb.setEnabled(isChecked);
                binding.camera3Cb.setEnabled(isChecked);
                binding.sprayCodeCb.setEnabled(isChecked);
                binding.autoModeRb.setEnabled(isChecked);
                binding.sortModeRb.setEnabled(isChecked);
                if (!isChecked) {
                    EventBus.getDefault().post(new CmdEvent(PlcService.ENABLE_DETECT, DataUtil.flagBean));
                    DataUtil.saveDetectionFlagBean();
                    EventBus.getDefault().post(new FlagSavedEvent());
                }
            }
            break;
            case R.id.short_circuit_cb:
                DataUtil.flagBean.setShortCircuitEnable(isChecked);
                buttonView.setText(isChecked ? R.string.short_circuit_enable : R.string.short_circuit_disable);
                break;
            case R.id.open_circuit_cb:
                DataUtil.flagBean.setOpenCircuitEnable(isChecked);
                buttonView.setText(isChecked ? R.string.open_circuit_enable : R.string.open_circuit_disable);
                break;
            case R.id.un_aged_cb:
                DataUtil.flagBean.setUnAgeEnable(isChecked);
                buttonView.setText(isChecked ? R.string.un_age_enable : R.string.un_age_disable);
                break;
            case R.id.leak_current_cb:
                DataUtil.flagBean.setLeakCurrentEnable(isChecked);
                buttonView.setText(isChecked ? R.string.leak_current_enable : R.string.leak_current_disable);
                break;
            case R.id.capacity_cb:
                DataUtil.flagBean.setCapacityEnable(isChecked);
                buttonView.setText(isChecked ? R.string.capacity_enable : R.string.capacity_disable);
                break;
            case R.id.loss_angle_cb:
                DataUtil.flagBean.setLossAngleEnable(isChecked);
                buttonView.setText(isChecked ? R.string.loss_angle_enable : R.string.loss_angle_disable);
                break;
            case R.id.esr_cb:
                DataUtil.flagBean.setEsrEnable(isChecked);
                buttonView.setText(isChecked ? R.string.esr_enable : R.string.esr_disable);
                break;
            case R.id.surge_cb:
                DataUtil.flagBean.setSurgeEnable(isChecked);
                buttonView.setText(isChecked ? R.string.surge_enable : R.string.surge_disable);
                break;
            case R.id.implosion_cb:
                DataUtil.flagBean.setImplosionEnable(isChecked);
                buttonView.setText(isChecked ? R.string.implosion_enable : R.string.implosion_disable);
                break;
            case R.id.power_supply_cb:
                DataUtil.flagBean.setPowerSupplyAlarmEnable(isChecked);
                buttonView.setText(isChecked ? R.string.power_supply_alarm_enable : R.string.power_supply_alarm_disable);
                break;
            case R.id.temperature_cb:
                DataUtil.flagBean.setTemperatureAlarmEnable(isChecked);
                buttonView.setText(isChecked ? R.string.temperature_alarm_enable : R.string.temperature_alarm_disable);
                break;
            case R.id.camera_1_cb:
                DataUtil.flagBean.setCamera1Enable(isChecked);
                buttonView.setText(isChecked ? R.string.camera_1_enable : R.string.camera_1_disable);
                break;
            case R.id.camera_2_cb:
                DataUtil.flagBean.setCamera2Enable(isChecked);
                buttonView.setText(isChecked ? R.string.camera_2_enable : R.string.camera_2_disable);
                break;
            case R.id.camera_3_cb:
                DataUtil.flagBean.setCamera3Enable(isChecked);
                buttonView.setText(isChecked ? R.string.camera_3_enable : R.string.camera_3_disable);
                break;
            case R.id.spray_code_cb:
                DataUtil.flagBean.setSprayCodeEnable(isChecked);
                buttonView.setText(isChecked ? R.string.spray_code_enable : R.string.spray_code_disable);
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFlagSaved(FlagSavedEvent event) {
        Toast.makeText(_mActivity, "检测开关参数已保存并下发", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }
}
