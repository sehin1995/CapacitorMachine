package com.sunseen.capacitormachine.modules.permission.factory.event;

import java.io.File;

public class UpdateFileScanResult {
    private int result; // 0 :未扫描到U盘 ，1 ：未扫描到apk文件 2：扫描到了apk文件
    private File[] files;

    public UpdateFileScanResult(int result) {
        this.result = result;
    }

    public UpdateFileScanResult(int result, File[] files) {
        this.result = result;
        this.files = files;
    }

    public int getResult() {
        return result;
    }

    public File[] getFiles() {
        return files;
    }
}
