package com.sunseen.capacitormachine.modules.query.bean;

public class IVBean {
    private int i;
    private int v;

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }
}
