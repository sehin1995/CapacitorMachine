package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class OvenCapacitorData {
    public String uid;
    /**
     * 电源号
     */
    public int p;
    /**
     * 电流值
     */
    public int i;
    /**
     * 电压值
     */
    public int v;

    public OvenCapacitorData() {

    }

    public OvenCapacitorData(String uid, int p, int i, int v) {
        this.uid = uid;
        this.p = p;
        this.i = i;
        this.v = v;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }
}
