package com.sunseen.capacitormachine.commumication.mqtt.bean;

import com.dslplatform.json.CompiledJson;

@CompiledJson(onUnknown = CompiledJson.Behavior.IGNORE)
public class LeakCurrentJsonObj {
    public String type;
    public int time;
    public LeakCurrentData data;

    public LeakCurrentJsonObj(String type, int time, LeakCurrentData data) {
        this.type = type;
        this.time = time;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public LeakCurrentData getData() {
        return data;
    }

    public void setData(LeakCurrentData data) {
        this.data = data;
    }
}
