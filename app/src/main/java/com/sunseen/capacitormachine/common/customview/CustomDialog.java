package com.sunseen.capacitormachine.common.customview;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.MethodUtil;

public class CustomDialog extends Dialog {

    private TextView titleTv;
    private TextView contentTv;
    private Button cancelBtn;
    private Button confirmBtn;

    public CustomDialog(@NonNull Context context) {
        this(context, -1, -1);
    }

    public CustomDialog(@NonNull Context context, int width, int height) {
        super(context, R.style.customDialog);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_custom_dialog, null);
        super.setContentView(view);
        titleTv = view.findViewById(R.id.tip_title);
        contentTv = view.findViewById(R.id.tv_content);
        cancelBtn = view.findViewById(R.id.btn_cancel);
        confirmBtn = view.findViewById(R.id.btn_confirm);
        if (width < 0) {
            width = (MethodUtil.getScreenWidth(context) / 5 * 3);
        }
        if (height < 0) {
            height = MethodUtil.getScreenHeight(context) / 8 * 3;
        }
        getWindow().setLayout(width, height);
        setCanceledOnTouchOutside(false);
    }

    public void setTitle(CharSequence title) {
        titleTv.setText(title);
    }

    public void setContent(CharSequence content) {
        contentTv.setText(content);
    }

    public void setCancelClickListener(View.OnClickListener listener) {
        cancelBtn.setOnClickListener(listener);
    }

    public void setConfirmClickListener(View.OnClickListener listener) {
        confirmBtn.setOnClickListener(listener);
    }
}
