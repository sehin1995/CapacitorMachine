package com.sunseen.capacitormachine.modules.realtime;

import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.ViewDataBinding;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bin.david.form.core.SmartTable;
import com.bin.david.form.data.table.FormTableData;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.sunseen.capacitormachine.CapacitorApp;
import com.sunseen.capacitormachine.R;
import com.sunseen.capacitormachine.common.SharePreferenceUtil;
import com.sunseen.capacitormachine.commumication.http.HttpUtil;
import com.sunseen.capacitormachine.commumication.http.RestClient;
import com.sunseen.capacitormachine.commumication.mqtt.bean.BatchStatisticsData;
import com.sunseen.capacitormachine.commumication.mqtt.bean.BatchStatisticsJsonObj;
import com.sunseen.capacitormachine.commumication.mqtt.event.SendJsonObjEvent;
import com.sunseen.capacitormachine.commumication.serialport.event.ImplosionCountEvent;
import com.sunseen.capacitormachine.commumication.serialport.event.RealTimeVoltageEvent;
import com.sunseen.capacitormachine.commumication.serialport.event.SurgeCurveEvent;
import com.sunseen.capacitormachine.data.DataUtil;
import com.sunseen.capacitormachine.databinding.FragmentRealTimeDataBinding;
import com.sunseen.capacitormachine.common.BaseFragment;
import com.sunseen.capacitormachine.common.Form;
import com.sunseen.capacitormachine.modules.parameter.bean.BatchBean;
import com.sunseen.capacitormachine.modules.parameter.bean.FlowCardBean;
import com.sunseen.capacitormachine.modules.realtime.event.CountInfo;
import com.sunseen.capacitormachine.modules.realtime.event.CountUpdateEvent;
import com.sunseen.capacitormachine.modules.realtime.event.SetSurgeLineEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zest
 */
public class RealTimeDataFragment extends BaseFragment implements View.OnLongClickListener {
    @Override
    protected int setLayout() {
        return R.layout.fragment_real_time_data;
    }

    private FragmentRealTimeDataBinding binding = null;

    private SmartTable<Form> valueTable;
    private SmartTable<Form> feedInCountTable;
    private SmartTable<Form> dischargeCountTable;
    private SmartTable<Form> dischargeCountTable2;
    private CountInfo lastCountInfo = null;
    private CountInfo newCountInfo = null;
    @Override
    protected void onBindView(ViewDataBinding viewDataBinding) {
        EventBus.getDefault().register(this);
        binding = (FragmentRealTimeDataBinding) viewDataBinding;
        initTables();
        initLineChart1();
        initLineChart2();
        setLongClickListener();

        lastCountInfo = new CountInfo(0,0,0,0,
                0,0,0,0,0,0,0,
                0,0,0);
        thread.start();
    }
    private static boolean flag = true;
    private Thread thread = new Thread(){
        @Override
        public void run() {
            while (true) {
                if(flag){
                    flag = false;
                    if(newCountInfo != null){
                        final CountInfo copyNewCountInfo = newCountInfo;
                        if(lastCountInfo == null){
                            //queryBatchStatusTics(DataUtil.FlowCardBatchIdProducing);//查询当前生产批次的数据，用于统计
                            Log.e("xiaochunhui","lastCountInfo is null");
                        } else {
                            Log.e("xiaochunhui12_copy->",copyNewCountInfo.toStrings());
                            Log.e("xiaochunhui12_last->",lastCountInfo.toStrings());
                            if(copyNewCountInfo.getGood() > lastCountInfo.getGood()){
                                //良
                                goodProductCount = goodProductCount + (copyNewCountInfo.getGood() - lastCountInfo.getGood());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.GOOD_PRODUCT_COUNT, goodProductCount);
                            }else if(copyNewCountInfo.getGood() < lastCountInfo.getGood()){
                                goodProductCount = goodProductCount + (copyNewCountInfo.getGood() + 256 - lastCountInfo.getGood());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.GOOD_PRODUCT_COUNT, goodProductCount);
                            }
                            if(copyNewCountInfo.getSuperior() > lastCountInfo.getSuperior()){
                                //优品计数
                                excellentProductCount = excellentProductCount + (copyNewCountInfo.getSuperior() - lastCountInfo.getSuperior());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.EXCELLENT_PRODUCT_COUNT, excellentProductCount);
                            }else if(copyNewCountInfo.getSuperior() < lastCountInfo.getSuperior()){
                                excellentProductCount = excellentProductCount + (copyNewCountInfo.getSuperior() + 256 - lastCountInfo.getSuperior());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.EXCELLENT_PRODUCT_COUNT, excellentProductCount);
                            }
                            if(copyNewCountInfo.getHighCapacity() > lastCountInfo.getHighCapacity()){
                                //高容
                                highCapacityCount = highCapacityCount + (copyNewCountInfo.getHighCapacity() - lastCountInfo.getHighCapacity());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.HIGH_CAPACITY_COUNT, highCapacityCount);

                            }else if(copyNewCountInfo.getHighCapacity() < lastCountInfo.getHighCapacity()){
                                highCapacityCount = highCapacityCount + (copyNewCountInfo.getHighCapacity() + 256 - lastCountInfo.getHighCapacity());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.HIGH_CAPACITY_COUNT, highCapacityCount);
                            }
                            if(copyNewCountInfo.getLowCapacity() > lastCountInfo.getLowCapacity()){
                                //低容
                                lowCapacityCount = lowCapacityCount + (copyNewCountInfo.getLowCapacity() - lastCountInfo.getLowCapacity());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.LOW_CAPACITY_COUNT, lowCapacityCount);
                            }else if(copyNewCountInfo.getLowCapacity() < lastCountInfo.getLowCapacity()){
                                lowCapacityCount = lowCapacityCount + (copyNewCountInfo.getLowCapacity() + 256 - lastCountInfo.getLowCapacity());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.LOW_CAPACITY_COUNT, lowCapacityCount);
                            }
                            if(copyNewCountInfo.getImpedance() > lastCountInfo.getImpedance()){
                                //阻抗
                                esrCount = esrCount + (copyNewCountInfo.getImpedance() - lastCountInfo.getImpedance());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.ESR_COUNT, esrCount);
                            }else if(copyNewCountInfo.getImpedance() < lastCountInfo.getImpedance()){
                                esrCount = esrCount + (copyNewCountInfo.getImpedance() + 256 - lastCountInfo.getImpedance());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.ESR_COUNT, esrCount);
                            }
                            if(copyNewCountInfo.getReTest() > lastCountInfo.getReTest()){
                                //重测
                                reTestCount = reTestCount + (copyNewCountInfo.getReTest() - lastCountInfo.getReTest());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.RE_TEST_COUNT, reTestCount);
                            }else if(copyNewCountInfo.getReTest() < lastCountInfo.getReTest()){
                                reTestCount = reTestCount + (copyNewCountInfo.getReTest() + 256 - lastCountInfo.getReTest());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.RE_TEST_COUNT, reTestCount);
                            }
                            if(copyNewCountInfo.getLossAngel() > lastCountInfo.getLossAngel()){
                                //损失
                                lossAngleCount = lossAngleCount + (copyNewCountInfo.getLossAngel() - lastCountInfo.getLossAngel());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.LOSS_ANGLE_COUNT, lossAngleCount);
                            }else if(copyNewCountInfo.getLossAngel() < lastCountInfo.getLossAngel()){
                                lossAngleCount = lossAngleCount + (copyNewCountInfo.getLossAngel() + 256 - lastCountInfo.getLossAngel());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.LOSS_ANGLE_COUNT, lossAngleCount);
                            }
                            if(copyNewCountInfo.getLeakCurrent() > lastCountInfo.getLeakCurrent()){
                                //漏电
                                leakCurrentCount = leakCurrentCount + (copyNewCountInfo.getLeakCurrent() - lastCountInfo.getLeakCurrent());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.LEAK_CURRENT_COUNT, leakCurrentCount);
                            }else if(copyNewCountInfo.getLeakCurrent() < lastCountInfo.getLeakCurrent()){
                                leakCurrentCount = leakCurrentCount + (copyNewCountInfo.getLeakCurrent() + 256 - lastCountInfo.getLeakCurrent());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.LEAK_CURRENT_COUNT, leakCurrentCount);
                            }
                            if(copyNewCountInfo.getConvex() > lastCountInfo.getConvex()){
                                //凸顶
                                convexCount = convexCount + (copyNewCountInfo.getConvex() - lastCountInfo.getConvex());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.CONVEX_COUNT, convexCount);
                            }else if(copyNewCountInfo.getConvex() < lastCountInfo.getConvex()){
                                convexCount = convexCount + (copyNewCountInfo.getConvex() + 256 - lastCountInfo.getConvex());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.CONVEX_COUNT, convexCount);
                            }
                            if(copyNewCountInfo.getUnAge() > lastCountInfo.getUnAge()){
                                //未老化
                                unAgeCount = unAgeCount + (copyNewCountInfo.getUnAge() - lastCountInfo.getUnAge());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.UN_AGE_COUNT, unAgeCount);
                            }else if(copyNewCountInfo.getUnAge() < lastCountInfo.getUnAge()){
                                unAgeCount = unAgeCount + (copyNewCountInfo.getUnAge() + 256 - lastCountInfo.getUnAge());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.UN_AGE_COUNT, unAgeCount);
                            }
                            if(copyNewCountInfo.getSurge() > lastCountInfo.getSurge()){
                                //浪涌
                                surgeCount = surgeCount + (copyNewCountInfo.getSurge() - lastCountInfo.getSurge());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.SURGE_COUNT, surgeCount);
                            }else if(copyNewCountInfo.getSurge() < lastCountInfo.getSurge()){
                                surgeCount = surgeCount + (copyNewCountInfo.getSurge() + 256 - lastCountInfo.getSurge());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.SURGE_COUNT, surgeCount);
                            }
                            if(DataUtil.flagBean.isCamera3Enable()){
                                if(copyNewCountInfo.getQrCodeErrorCount() > lastCountInfo.getQrCodeErrorCount()){
                                    //二维码
                                    qrCodeErrorCount = qrCodeErrorCount + (copyNewCountInfo.getQrCodeErrorCount() - lastCountInfo.getQrCodeErrorCount());
                                    SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                            .putInt(SharePreferenceUtil.QR_CODE_ERROR_COUNT, qrCodeErrorCount);
                                }else if(copyNewCountInfo.getQrCodeErrorCount() < lastCountInfo.getQrCodeErrorCount()){
                                    qrCodeErrorCount = qrCodeErrorCount + (copyNewCountInfo.getQrCodeErrorCount() + 256 - lastCountInfo.getQrCodeErrorCount());
                                    SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                            .putInt(SharePreferenceUtil.QR_CODE_ERROR_COUNT, qrCodeErrorCount);
                                }
                            }
                            if(copyNewCountInfo.getOpenCircuit() > lastCountInfo.getOpenCircuit()){
                                //开路
                                openCircuitCount = openCircuitCount + (copyNewCountInfo.getOpenCircuit() - lastCountInfo.getOpenCircuit());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.OPEN_CIRCUIT_COUNT, openCircuitCount);
                            }else if(copyNewCountInfo.getOpenCircuit() < lastCountInfo.getOpenCircuit()){
                                openCircuitCount = openCircuitCount + (copyNewCountInfo.getOpenCircuit() + 256 - lastCountInfo.getOpenCircuit());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.OPEN_CIRCUIT_COUNT, openCircuitCount);
                            }
                            if(copyNewCountInfo.getShortCircuit() > lastCountInfo.getShortCircuit()){
                                //短路
                                shortCircuitCount = shortCircuitCount + (copyNewCountInfo.getShortCircuit() - lastCountInfo.getShortCircuit());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.SHORT_CIRCUIT_COUNT, shortCircuitCount);
                            }else if(copyNewCountInfo.getShortCircuit() < lastCountInfo.getShortCircuit()){
                                shortCircuitCount = shortCircuitCount + (copyNewCountInfo.getShortCircuit() + 256 - lastCountInfo.getShortCircuit());
                                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                                        .putInt(SharePreferenceUtil.SHORT_CIRCUIT_COUNT, shortCircuitCount);
                            }
                        }
                        produceCount = feedCount + shortCircuitCount + openCircuitCount
                                + acuteMaladyCount + unAgeCount + convexCount
                                + surgeCount + excellentProductCount + goodProductCount
                                + highCapacityCount + lowCapacityCount + lossAngleCount
                                + leakCurrentCount + esrCount + reTestCount + qrCodeErrorCount;
                        lastCountInfo = copyNewCountInfo;
                    }

                    final BatchStatisticsData batchStatisticsData = new BatchStatisticsData(
                            DataUtil.FlowCardBatchIdProducing,//单号
                            feedCount,//进料统计
                            shortCircuitCount,//短路计数
                            openCircuitCount,
                            unAgeCount,
                            leakCurrentCount,
                            highCapacityCount,
                            lowCapacityCount,
                            implosionCount,
                            convexCount,
                            surgeCount,
                            lossAngleCount,
                            excellentProductCount,
                            goodProductCount,
                            reTestCount,
                            produceCount,
                            acuteMaladyCount,
                            qrCodeErrorCount);
                    final BatchStatisticsJsonObj jsonObj = new BatchStatisticsJsonObj("",
                            (int) (System.currentTimeMillis() / 1000L),
                            batchStatisticsData);

                    EventBus.getDefault().post(new SendJsonObjEvent(jsonObj));
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    flag = true;
                }

            }

        }
    };

    private void setLongClickListener() {
        binding.btnClearAll.setOnLongClickListener(this);
        binding.btnFeedInClear.setOnLongClickListener(this);
        binding.btnClearCount1.setOnLongClickListener(this);
        binding.btnClearCount2.setOnLongClickListener(this);
        binding.btnClearCount3.setOnLongClickListener(this);
        binding.btnClearCount4.setOnLongClickListener(this);
        binding.btnClearCount4.setVisibility(View.INVISIBLE);//去掉极性不良计数
        binding.btnDischargeClear.setOnLongClickListener(this);
        binding.btnDischargeClear1.setOnLongClickListener(this);
        binding.btnDischargeClear2.setOnLongClickListener(this);
        binding.btnDischargeClear3.setOnLongClickListener(this);
        binding.btnDischargeClear4.setOnLongClickListener(this);
        binding.btnDischargeClear5.setOnLongClickListener(this);
        binding.btnDischargeClear6.setOnLongClickListener(this);
        binding.btnDischargeClear7.setOnLongClickListener(this);
        binding.btnDischargeClear11.setOnLongClickListener(this);
        binding.btnDischargeClear12.setOnLongClickListener(this);
        binding.btnDischargeClear13.setOnLongClickListener(this);
        binding.btnDischargeClear14.setOnLongClickListener(this);
        binding.btnDischargeClear15.setOnLongClickListener(this);
        binding.btnDischargeClear16.setOnLongClickListener(this);
        binding.btnDischargeClear17.setOnLongClickListener(this);
    }

    private Form[][] valueForm;
    private Form[][] feedInCountForm;
    private Form[][] dischargeForm;
    private Form[][] dischargeForm2;

    private void initForms() {
        feedInCountForm = new Form[][]{
                {
                        new Form("进料"),
                        new Form("     0     "),
                },
                {

                        new Form("短路"),
                        new Form("     0     "),

                },
                {
                        new Form("开路"),
                        new Form("     0     "),
                },
//                {
//                        new Form("极性不良"),
//                        new Form("     0     "),
//                },

        };
        valueForm = new Form[][]{
                {
                        new Form(getString(R.string.temp_title)),
                        new Form("000.0℃"),
                        new Form(getString(R.string.test_power_supply_voltage)),
                        new Form("000.0V"),
                        new Form(getString(R.string.surge_power_supply_voltage)),
                        new Form("000.0V"),
                        new Form(getString(R.string.unage_power_supply_voltage)),
                        new Form("000.0V"),
                },
                {
                        new Form(getString(R.string.leakage_current_1)),
                        new Form("0000μA"),
//                        new Form(getString(R.string.capacity_1)),
//                        new Form("0000μF"),
//                        new Form(getString(R.string.loss_angle_1)),
//                        new Form("00.00%"),
//                        new Form(getString(R.string.impedance_1)),
//                        new Form("0000mΩ"),
                        new Form(getString(R.string.capacity)),
                        new Form("0000μF"),
                        new Form(getString(R.string.loss_angle)),
                        new Form("00.00%"),
                        new Form(getString(R.string.impedance)),
                        new Form("0000mΩ"),
                },
                {
                        new Form(getString(R.string.leakage_current_2)),
                        new Form("0000μA"),
//                        new Form(getString(R.string.capacity)),
//                        new Form("0000μF"),
//                        new Form(getString(R.string.loss_angle)),
//                        new Form("00.00%"),
//                        new Form(getString(R.string.impedance)),
//                        new Form("0000mΩ"),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                        new Form(""),
                },

        };
        dischargeForm = new Form[][]{
                {
                        new Form("未老化"),
                        new Form("漏电"),
                        new Form("高容"),
                        new Form("阻抗"),
                        new Form("优品"),
                        new Form("良品率"),
                        new Form("内爆"),
                },
                {
                        new Form("     0     "),
                        new Form("     0     "),
                        new Form("     0     "),
                        new Form("     0     "),
                        new Form("     0     "),
                        new Form("00.00%"),
                        new Form("     0     "),
                }
        };
        dischargeForm2 = new Form[][]{
                {
                        new Form("凸顶"),
                        new Form("浪涌"),
                        new Form("低容"),
                        new Form("损失"),
                        new Form("良品"),
                        new Form("重测"),
                        new Form("打码不良"),
                },
                {
                        new Form("     0     "),
                        new Form("     0     "),
                        new Form("     0     "),
                        new Form("     0     "),
                        new Form("     0     "),
                        new Form("     0     "),
                        new Form("     0     "),
                }
        };
    }

    private void configTables(SmartTable<Form> table, int spanSize, Form[][] forms, boolean vertical) {
        FormTableData<Form> formTableData = FormTableData.create(table,
                "", spanSize, forms);
        formTableData.setFormat((t) -> {
            if (t != null) {
                return t.getContent();
            } else {
                return "";
            }
        });
        table.getConfig().setShowXSequence(false);
        table.getConfig().setShowYSequence(false);
        table.getConfig().getContentGridStyle().setColor(Color.parseColor("#898989"));
        table.getConfig().getContentStyle().setTextColor(Color.parseColor("#222222"));
        table.setTableData(formTableData);
    }

    private void initTables() {
        initForms();

        feedInCountTable = binding.tableFeedInCount;
        configTables(feedInCountTable, 2, feedInCountForm, true);

        valueTable = binding.tableValue;
        configTables(valueTable, 8, valueForm, true);

        dischargeCountTable = binding.tableDischargeCount;
        configTables(dischargeCountTable, 7, dischargeForm, false);

        dischargeCountTable2 = binding.tableDischargeCount2;
        configTables(dischargeCountTable2, 7, dischargeForm2, false);
        //queryBatchStatusTics(DataUtil.FlowCardBatchIdProducing);//查询当前生产批次的数据，用于统计
    }

    private LineChart lineChart1;

    private void initLineChart1() {
        lineChart1 = binding.lineChart1;
        setLineChartParameter(lineChart1);
        initLineData(lineChart1, "浪涌1", 450);
    }

    private LineChart lineChart2;

    private void initLineChart2() {
        lineChart2 = binding.lineChart2;
        setLineChartParameter(lineChart2);
        initLineData(lineChart2, "浪涌2", 450);
    }

    private void initLineData(LineChart lineChart, String labelName, int baseValue) {

        LineDataSet baseLineSet = new LineDataSet(new ArrayList<>(), getString(R.string.surging_upper_limit));
//        baseLineSet.addEntry(new Entry(0, baseValue));
//        baseLineSet.addEntry(new Entry(220, baseValue));
        baseLineSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        baseLineSet.setMode(LineDataSet.Mode.LINEAR);
        baseLineSet.setLineWidth(2f);
        baseLineSet.setCircleRadius(2f);
        baseLineSet.setFillAlpha(65);
        final int orangeColor = getResources().getColor(R.color.orange);
        baseLineSet.setColor(orangeColor);
        baseLineSet.setCircleColor(orangeColor);
        baseLineSet.setFillColor(orangeColor);
        baseLineSet.setHighLightColor(Color.rgb(244, 177, 177));
        baseLineSet.setDrawCircleHole(false);

        LineDataSet set = new LineDataSet(new ArrayList<>(), labelName);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setMode(LineDataSet.Mode.LINEAR);
        set.setLineWidth(2f);
        set.setCircleRadius(2f);
        set.setFillAlpha(65);
        final int redColor = getResources().getColor(R.color.red_normal);
        set.setColor(redColor);
        set.setCircleColor(redColor);
        set.setFillColor(redColor);

        set.setHighLightColor(Color.rgb(244, 177, 177));
        set.setDrawCircleHole(false);

        LineData lineData = new LineData(baseLineSet, set);
        lineData.setValueTextColor(Color.BLACK);
        lineData.setValueTextSize(12f);
        lineChart.setData(lineData);
    }

    private void setLineChartParameter(LineChart lineChart) {
        lineChart.getDescription().setEnabled(false);
        lineChart.setPinchZoom(true);
        lineChart.setBackgroundColor(Color.TRANSPARENT);

        Legend legend = lineChart.getLegend();
        legend.setForm(Legend.LegendForm.SQUARE);
        legend.setTextSize(12f);
        legend.setTextColor(Color.BLACK);
        legend.setMaxSizePercent(0.9f);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setAxisLineColor(getResources().getColor(R.color.colorAccent));
        xAxis.setAxisLineWidth(2f);
        xAxis.setTextSize(12f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawGridLines(false);
        xAxis.setAxisMinimum(0f);
        xAxis.setAxisMaximum(220f);
        xAxis.setLabelCount(10);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                if (value != 0f) {
                    return String.valueOf((int) value);
                } else {
                    return "";
                }
            }
        });

        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setAxisLineColor(getResources().getColor(R.color.colorAccent));
        leftAxis.setTextSize(12f);
        leftAxis.setAxisLineWidth(2f);
        leftAxis.setAxisMaximum(600f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(false);
        leftAxis.setGranularityEnabled(false);

        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setDrawZeroLine(false);
        rightAxis.setGranularityEnabled(false);
        //不画刻度数字
        rightAxis.setDrawLabels(false);
        //不画竖线
        rightAxis.setDrawAxisLine(false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetSurgeData(SurgeCurveEvent event) {
        if (event.getSurgeType() == 1) {
            //浪涌1的曲线数据
            LineData lineData = lineChart1.getLineData();
            ILineDataSet set = lineData.getDataSets().get(1);
            set.clear();
            for (int i = 1; i < event.getSurgeDatas().size(); i++) {
                set.addEntry(new Entry(i, event.getSurgeDatas().get(i)));
            }
            lineChart1.notifyDataSetChanged();
            lineChart1.invalidate();
            binding.tvSurge1Result.setText(getSurgeResultStr(event.getSurgeResult()));
        } else if (event.getSurgeType() == 2) {
            LineData lineData = lineChart2.getLineData();
            ILineDataSet set = lineData.getDataSets().get(1);
            set.clear();
            for (int i = 1; i < event.getSurgeDatas().size(); i++) {
                set.addEntry(new Entry(i, event.getSurgeDatas().get(i)));
            }
            lineChart2.notifyDataSetChanged();
            lineChart2.invalidate();
            binding.tvSurge2Result.setText(getSurgeResultStr(event.getSurgeResult()));
        }
    }

    private String getSurgeResultStr(int result) {
        switch (result) {
            case 2:
                return getString(R.string.pass);
            case 1:
                return getString(R.string.no_pass);
            case 0:
                return getString(R.string.poor_contact_or_vacancy);
            default:
                return getString(R.string.unknown);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGotVoltage(RealTimeVoltageEvent event) {
        valueForm[0][3].setContent(String.format(getString(R.string.voltage_format),
                event.getSurgeVoltage() / 10.0f));
        valueForm[0][5].setContent(String.format(getString(R.string.voltage_format),
                event.getTestVoltage() / 10.0f));
        valueForm[0][7].setContent(String.format(getString(R.string.voltage_format),
                event.getUnAgeVoltage() / 10.0f));
        valueTable.invalidate();
    }

    /**
     * 进料计数
     */
    private int feedCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.FEED_COUNT,0);
    /**
     * 短路计数
     */
    private int shortCircuitCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.SHORT_CIRCUIT_COUNT,0);
    /**
     * 开路计数
     */
    private int openCircuitCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.OPEN_CIRCUIT_COUNT,0);
    /**
     * 极性不良计数
     */
    private int acuteMaladyCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.ACUTE_MALADY_COUNT,0);

    /**
     * 未老化计数
     */
    private int unAgeCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.UN_AGE_COUNT,0);
    /**
     * 凸顶计数
     */
    private int convexCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.CONVEX_COUNT,0);
    /**
     * 漏电计数
     */
    private int leakCurrentCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.LEAK_CURRENT_COUNT,0);
    /**
     * 浪涌计数
     */
    private int surgeCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.SURGE_COUNT,0);
    /**
     * 高容计数
     */
    private int highCapacityCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.HIGH_CAPACITY_COUNT,0);

    /**
     * 低容计数
     */
    private int lowCapacityCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.LOW_CAPACITY_COUNT,0);
    /**
     * 阻抗计数
     */
    private int esrCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.ESR_COUNT,0);
    /**
     * 损失计数
     */
    private int lossAngleCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.LOSS_ANGLE_COUNT,0);
    /**
     * 优品计数
     */
    private int excellentProductCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.EXCELLENT_PRODUCT_COUNT,0);
    /**
     * 良品计数
     */
    private int goodProductCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.GOOD_PRODUCT_COUNT,0);


    /**
     * 重测计数
     */
    private int reTestCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.RE_TEST_COUNT,0);

    /**
     * 内爆计数
     */
    private int implosionCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.IMPLOSION_COUNT,0);

    /**
     * 二维码
     */
    private int qrCodeErrorCount = SharePreferenceUtil.getInstance(getContext())
            .getInt(SharePreferenceUtil.QR_CODE_ERROR_COUNT,0);

    /**
     * 出料总数
     */
    private int produceCount = 0;


    private DecimalFormat df = new DecimalFormat("0.00");

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCountUpdateEvent(CountUpdateEvent event) {
        Log.e("xiaochunhui   ", event.toStrings());
        if (event.isFeed()) {
            feedInCountForm[0][1].setContent(String.valueOf(++feedCount));
        }
        newCountInfo = event.getCountInfo();
        binding.dischargeCountTv.setText(String.valueOf(produceCount));
        feedInCountForm[1][1].setContent(String.valueOf(shortCircuitCount));
        feedInCountForm[2][1].setContent(String.valueOf(openCircuitCount));
        dischargeForm[1][0].setContent(String.valueOf(unAgeCount));
        dischargeForm2[1][0].setContent(String.valueOf(convexCount));
        dischargeForm2[1][1].setContent(String.valueOf(surgeCount));
        dischargeForm2[1][1].setContent(String.valueOf(surgeCount));
        binding.dischargeCountTv.setText(String.valueOf(produceCount));
        dischargeForm[1][4].setContent(String.valueOf(excellentProductCount));
        dischargeForm2[1][4].setContent(String.valueOf(goodProductCount));
        dischargeForm[1][2].setContent(String.valueOf(highCapacityCount));
        dischargeForm2[1][2].setContent(String.valueOf(lowCapacityCount));
        dischargeForm2[1][3].setContent(String.valueOf(lossAngleCount));
        dischargeForm[1][1].setContent(String.valueOf(leakCurrentCount));
        dischargeForm[1][3].setContent(String.valueOf(esrCount));
        dischargeForm2[1][5].setContent(String.valueOf(reTestCount));
        dischargeForm2[1][6].setContent(String.valueOf(qrCodeErrorCount));

        if (produceCount > 0) {
            //良品率 = （优品 + 良品）/出料总数
            dischargeForm[1][5].setContent(df.format((excellentProductCount + goodProductCount) * 100 / produceCount) + "%");
        }


        valueForm[0][1].setContent(String.format(getString(R.string.temperature_format),
                event.getTemperature() / 10.0));
        valueForm[1][1].setContent(String.format(getString(R.string.leak_current_format),
                event.getLeakCurrentValue1()));
//        valueForm[1][3].setContent(String.format(getString(R.string.capacity_format),
//                event.getCapacity1()));
//        valueForm[1][5].setContent(event.getLossAngle1() + "%");
//        valueForm[1][7].setContent(String.format(getString(R.string.impedance_format),
//                event.getImpedance1()));
        valueForm[2][1].setContent(String.format(getString(R.string.leak_current_format),
                event.getLeakCurrentValue2()));
//        valueForm[2][3].setContent(String.format(getString(R.string.capacity_format),
//                event.getCapacity2()));
//        valueForm[2][5].setContent(event.getLossAngle2() + "%");
//        valueForm[2][7].setContent(String.format(getString(R.string.impedance_format),
//                event.getImpedance2()));
        valueForm[1][3].setContent(String.format(getString(R.string.capacity_format),
                event.getCapacity2()));
        valueForm[1][5].setContent(event.getLossAngle2() + "%");
        valueForm[1][7].setContent(String.format(getString(R.string.impedance_format),
                event.getImpedance2()));

        valueTable.invalidate();
        feedInCountTable.invalidate();
        dischargeCountTable.invalidate();
        dischargeCountTable2.invalidate();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSurgeParamter(SetSurgeLineEvent event) {
        LineData lineData = lineChart1.getLineData();
        ILineDataSet set = lineData.getDataSets().get(0);
        set.clear();
        set.addEntry(new Entry(0, event.getSurgeUpperLimit()));
        set.addEntry(new Entry(220, event.getSurgeUpperLimit()));
        lineChart1.notifyDataSetChanged();
        lineChart1.invalidate();

        LineData lineData2 = lineChart2.getLineData();
        ILineDataSet set2 = lineData2.getDataSets().get(0);
        set2.clear();
        set2.addEntry(new Entry(0, event.getSurgeUpperLimit()));
        set2.addEntry(new Entry(220, event.getSurgeUpperLimit()));
        lineChart2.notifyDataSetChanged();
        lineChart2.invalidate();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.btn_feed_in_clear: {
                feedCount = 0;
                shortCircuitCount = 0;
                openCircuitCount = 0;
                acuteMaladyCount = 0;
                feedInCountForm[0][1].setContent(getString(R.string.zero));
                feedInCountForm[1][1].setContent(getString(R.string.zero));
                feedInCountForm[2][1].setContent(getString(R.string.zero));
//                feedInCountForm[3][1].setContent(getString(R.string.zero));
                feedInCountTable.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.FEED_COUNT, feedCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.SHORT_CIRCUIT_COUNT, shortCircuitCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.OPEN_CIRCUIT_COUNT, openCircuitCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.ACUTE_MALADY_COUNT, acuteMaladyCount);
                toastClear("进料统计已清零");
            }
            break;
            case R.id.btn_clear_count_1: {
                feedCount = 0;
                feedInCountForm[0][1].setContent(getString(R.string.zero));
                feedInCountTable.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.FEED_COUNT, feedCount);
                toastClear("进料计数已清零");
            }
            break;
            case R.id.btn_clear_count_2: {
                shortCircuitCount = 0;
                feedInCountForm[1][1].setContent(getString(R.string.zero));
                feedInCountTable.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.SHORT_CIRCUIT_COUNT, shortCircuitCount);
                toastClear("短路计数已清零");
            }
            break;
            case R.id.btn_clear_count_3: {
                openCircuitCount = 0;
                feedInCountForm[2][1].setContent(getString(R.string.zero));
                feedInCountTable.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.OPEN_CIRCUIT_COUNT, openCircuitCount);
                toastClear("开路计数已清零");
            }
            break;
            case R.id.btn_clear_count_4: {
                acuteMaladyCount = 0;
//                feedInCountForm[3][1].setContent(getString(R.string.zero));
                feedInCountTable.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.ACUTE_MALADY_COUNT, acuteMaladyCount);
                toastClear("极性不良已清零");
            }
            break;
            case R.id.btn_discharge_clear: {
                produceCount = 0;
                binding.dischargeCountTv.setText(getString(R.string.zero));
                unAgeCount = 0;
                leakCurrentCount = 0;
                highCapacityCount = 0;
                esrCount = 0;
                excellentProductCount = 0;
                implosionCount = 0;
                dischargeForm[1][0].setContent(getString(R.string.zero));
                dischargeForm[1][1].setContent(getString(R.string.zero));
                dischargeForm[1][2].setContent(getString(R.string.zero));
                dischargeForm[1][3].setContent(getString(R.string.zero));
                dischargeForm[1][4].setContent(getString(R.string.zero));
                dischargeForm[1][5].setContent(getString(R.string.rate));
                dischargeForm[1][6].setContent(getString(R.string.zero));
                dischargeCountTable.invalidate();
                convexCount = 0;
                surgeCount = 0;
                lowCapacityCount = 0;
                lossAngleCount = 0;
                goodProductCount = 0;
                reTestCount = 0;
                qrCodeErrorCount = 0;
                dischargeForm2[1][0].setContent(getString(R.string.zero));
                dischargeForm2[1][1].setContent(getString(R.string.zero));
                dischargeForm2[1][2].setContent(getString(R.string.zero));
                dischargeForm2[1][3].setContent(getString(R.string.zero));
                dischargeForm2[1][4].setContent(getString(R.string.zero));
                dischargeForm2[1][5].setContent(getString(R.string.zero));
                dischargeForm2[1][6].setContent(getString(R.string.zero));
                dischargeCountTable2.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.UN_AGE_COUNT, unAgeCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.LEAK_CURRENT_COUNT, leakCurrentCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.HIGH_CAPACITY_COUNT, highCapacityCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.ESR_COUNT, esrCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.EXCELLENT_PRODUCT_COUNT, excellentProductCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.IMPLOSION_COUNT, implosionCount);
                toastClear("出料统计已清零");
            }
            break;
            case R.id.btn_discharge_clear_1: {
                unAgeCount = 0;
                dischargeForm[1][0].setContent(getString(R.string.zero));
                dischargeCountTable.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.UN_AGE_COUNT, unAgeCount);
                toastClear("未老化计数已清零");
            }
            break;
            case R.id.btn_discharge_clear_2: {
                leakCurrentCount = 0;
                dischargeForm[1][1].setContent(getString(R.string.zero));
                dischargeCountTable.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.LEAK_CURRENT_COUNT, leakCurrentCount);
                toastClear("漏电计数已清零");
            }
            break;
            case R.id.btn_discharge_clear_3: {
                highCapacityCount = 0;
                dischargeForm[1][2].setContent(getString(R.string.zero));
                dischargeCountTable.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.HIGH_CAPACITY_COUNT, highCapacityCount);
                toastClear("高容计数已清零");
            }
            break;
            case R.id.btn_discharge_clear_4: {
                esrCount = 0;
                dischargeForm[1][3].setContent(getString(R.string.zero));
                dischargeCountTable.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.ESR_COUNT, esrCount);
                toastClear("阻抗计数已清零");
            }
            break;
            case R.id.btn_discharge_clear_5: {
                excellentProductCount = 0;
                dischargeForm[1][4].setContent(getString(R.string.zero));
                dischargeCountTable.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.EXCELLENT_PRODUCT_COUNT, excellentProductCount);
                toastClear("优品计数已清零");
            }
            break;
            case R.id.btn_discharge_clear_6: {
                dischargeForm[1][5].setContent(getString(R.string.rate));
                dischargeCountTable.invalidate();
                toastClear("良品率已清零");
            }
            break;
            case R.id.btn_discharge_clear_7: {
                implosionCount = 0;
                dischargeForm[1][6].setContent(getString(R.string.zero));
                dischargeCountTable.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.IMPLOSION_COUNT, implosionCount);
                toastClear("内爆计数已清零");
            }
            break;
            case R.id.btn_discharge_clear_11: {
                convexCount = 0;
                dischargeForm2[1][0].setContent(getString(R.string.zero));
                dischargeCountTable2.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.CONVEX_COUNT, convexCount);
                toastClear("凸顶计数已清零");
            }
            break;
            case R.id.btn_discharge_clear_12: {
                surgeCount = 0;
                dischargeForm2[1][1].setContent(getString(R.string.zero));
                dischargeCountTable2.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.SURGE_COUNT, surgeCount);
                toastClear("浪涌计数已清零");
            }
            break;
            case R.id.btn_discharge_clear_13: {
                lowCapacityCount = 0;
                dischargeForm2[1][2].setContent(getString(R.string.zero));
                dischargeCountTable2.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.LOW_CAPACITY_COUNT, lowCapacityCount);
                toastClear("低容计数已清零");
            }
            break;
            case R.id.btn_discharge_clear_14: {
                lossAngleCount = 0;
                dischargeForm2[1][3].setContent(getString(R.string.zero));
                dischargeCountTable2.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.LOSS_ANGLE_COUNT, lossAngleCount);
                toastClear("损失计数已清零");
            }
            break;
            case R.id.btn_discharge_clear_15: {
                goodProductCount = 0;
                dischargeForm2[1][4].setContent(getString(R.string.zero));
                dischargeCountTable2.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.GOOD_PRODUCT_COUNT, goodProductCount);
                toastClear("良品计数已清零");
            }
            break;
            case R.id.btn_discharge_clear_16: {
                reTestCount = 0;
                dischargeForm2[1][5].setContent(getString(R.string.zero));
                dischargeCountTable2.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.RE_TEST_COUNT, reTestCount);
                toastClear("重测计数已清零");
            }
            break;
            case R.id.btn_discharge_clear_17: {
                qrCodeErrorCount = 0;
                dischargeForm2[1][6].setContent(getString(R.string.zero));
                dischargeCountTable2.invalidate();
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.QR_CODE_ERROR_COUNT, qrCodeErrorCount);
                toastClear("打码不良计数已清零");
            }
            break;
            case R.id.btn_clear_all: {
                feedCount = 0;
                shortCircuitCount = 0;
                openCircuitCount = 0;
                acuteMaladyCount = 0;
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.FEED_COUNT, feedCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.SHORT_CIRCUIT_COUNT, shortCircuitCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.OPEN_CIRCUIT_COUNT, openCircuitCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.ACUTE_MALADY_COUNT, acuteMaladyCount);
                feedInCountForm[0][1].setContent(getString(R.string.zero));
                feedInCountForm[1][1].setContent(getString(R.string.zero));
                feedInCountForm[2][1].setContent(getString(R.string.zero));
//                feedInCountForm[3][1].setContent(getString(R.string.zero));
                feedInCountTable.invalidate();
                unAgeCount = 0;
                leakCurrentCount = 0;
                highCapacityCount = 0;
                esrCount = 0;
                excellentProductCount = 0;
                implosionCount = 0;
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.UN_AGE_COUNT, unAgeCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.LEAK_CURRENT_COUNT, leakCurrentCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.HIGH_CAPACITY_COUNT, highCapacityCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.ESR_COUNT, esrCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.EXCELLENT_PRODUCT_COUNT, excellentProductCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.IMPLOSION_COUNT, implosionCount);
                dischargeForm[1][0].setContent(getString(R.string.zero));
                dischargeForm[1][1].setContent(getString(R.string.zero));
                dischargeForm[1][2].setContent(getString(R.string.zero));
                dischargeForm[1][3].setContent(getString(R.string.zero));
                dischargeForm[1][4].setContent(getString(R.string.zero));
                dischargeForm[1][5].setContent(getString(R.string.rate));
                dischargeForm[1][6].setContent(getString(R.string.zero));
                dischargeCountTable.invalidate();
                convexCount = 0;
                surgeCount = 0;
                lowCapacityCount = 0;
                lossAngleCount = 0;
                goodProductCount = 0;
                reTestCount = 0;
                qrCodeErrorCount = 0;
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.CONVEX_COUNT, convexCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.SURGE_COUNT, surgeCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.LOW_CAPACITY_COUNT, lowCapacityCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.LOSS_ANGLE_COUNT, lossAngleCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.GOOD_PRODUCT_COUNT, goodProductCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.RE_TEST_COUNT, reTestCount);
                SharePreferenceUtil.getInstance(CapacitorApp.getApplication())
                        .putInt(SharePreferenceUtil.QR_CODE_ERROR_COUNT, qrCodeErrorCount);
                dischargeForm2[1][0].setContent(getString(R.string.zero));
                dischargeForm2[1][1].setContent(getString(R.string.zero));
                dischargeForm2[1][2].setContent(getString(R.string.zero));
                dischargeForm2[1][3].setContent(getString(R.string.zero));
                dischargeForm2[1][4].setContent(getString(R.string.zero));
                dischargeForm2[1][5].setContent(getString(R.string.zero));
                dischargeForm2[1][6].setContent(getString(R.string.zero));
                dischargeCountTable2.invalidate();
                produceCount = 0;
                binding.dischargeCountTv.setText(getString(R.string.zero));
                toastClear("全部计数已清零");
            }
            break;
        }
        return false;
    }

    private void toastClear(CharSequence content) {
        Toast.makeText(_mActivity, content, Toast.LENGTH_SHORT).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onImplosionCount(ImplosionCountEvent event) {
        implosionCount++;
        dischargeForm[1][6].setContent(Integer.toString(implosionCount));
        dischargeCountTable.invalidate();
    }

//    private void queryBatchStatusTics(String batchId) {
//        if(TextUtils.isEmpty(batchId))
//        {
//            return;
//        }
//        RestClient.builder()
//                .url(HttpUtil.batchStatistics)
//                .params("batchId ", batchId)
//                .success((String respond) -> {
//                    JSONObject rootObj = JSON.parseObject(respond);
//                    if (rootObj != null) {
//                        int status = rootObj.getIntValue("status");
//                        if (status == 1) {
//                            JSONObject dataObj = null;
//                            try {
//                                dataObj = rootObj.getJSONObject("data");
//                                if (dataObj != null) {
//                                    BatchStatisticsData bean = dataObj.toJavaObject(BatchStatisticsData.class);
//                                    if (bean != null) {
//                                        bean.toString1();
//                                        updateForm(bean);
//                                    } else {
//                                        updateForm(null);
//                                    }
//                                } else {
//                                    updateForm(null);
//                                }
//                            } catch (ClassCastException e){
//                                Log.e("xiaochunhui",e.toString());
//                            }
//
//                        } else {
//                            updateForm(null);
//                        }
//                    } else {
//                        updateForm(null);
//                    }
//                })
//                .failure(() -> {
//                    updateForm(null);
//                })
//                .error((code, msg) -> {
//                    updateForm(null);
//                })
//                .build()
//                .get();
//    }

    private void updateForm(BatchStatisticsData bean) {
        if (bean != null) {
            feedCount = bean.getFeedIn();
            shortCircuitCount = bean.getShortCircuit();
            openCircuitCount = bean.getOpenCircuit();
            acuteMaladyCount = bean.getAcuteMaladyCount();
            unAgeCount = bean.getOpenCircuit();
            convexCount = bean.getOpenCircuit();
            surgeCount = bean.getOpenCircuit();
            //produceCount = bean.getOpenCircuit();
            excellentProductCount = bean.getOpenCircuit();
            goodProductCount = bean.getOpenCircuit();
            highCapacityCount = bean.getOpenCircuit();
            lowCapacityCount = bean.getOpenCircuit();
            lossAngleCount = bean.getOpenCircuit();
            leakCurrentCount = bean.getOpenCircuit();
            esrCount = bean.getOpenCircuit();
            reTestCount = bean.getOpenCircuit();
            qrCodeErrorCount = bean.getOpenCircuit();
            produceCount = feedCount + shortCircuitCount + openCircuitCount
                    + acuteMaladyCount + unAgeCount + convexCount
                    + surgeCount + excellentProductCount + goodProductCount
                    + highCapacityCount + lowCapacityCount + lossAngleCount
                    + leakCurrentCount + esrCount + reTestCount + qrCodeErrorCount;
        }
        feedInCountForm[0][1].setContent(String.valueOf(feedCount));
        feedInCountForm[1][1].setContent(String.valueOf(shortCircuitCount));
        feedInCountForm[2][1].setContent(String.valueOf(openCircuitCount));
//        feedInCountForm[3][1].setContent(String.valueOf(acuteMaladyCount));
        dischargeForm[1][0].setContent(String.valueOf(unAgeCount));
        dischargeForm2[1][0].setContent(String.valueOf(convexCount));
        dischargeForm2[1][1].setContent(String.valueOf(surgeCount));
        binding.dischargeCountTv.setText(String.valueOf(produceCount));
        dischargeForm[1][4].setContent(String.valueOf(excellentProductCount));
        dischargeForm2[1][4].setContent(String.valueOf(goodProductCount));
        dischargeForm[1][2].setContent(String.valueOf(highCapacityCount));
        dischargeForm2[1][2].setContent(String.valueOf(lowCapacityCount));
        dischargeForm2[1][3].setContent(String.valueOf(lossAngleCount));
        dischargeForm[1][1].setContent(String.valueOf(leakCurrentCount));
        dischargeForm[1][3].setContent(String.valueOf(esrCount));
        dischargeForm2[1][5].setContent(String.valueOf(reTestCount));
        dischargeForm2[1][6].setContent(String.valueOf(qrCodeErrorCount));
        if(produceCount > 0) {
            dischargeForm[1][5].setContent(df.format((excellentProductCount + goodProductCount) * 100 / produceCount) + "%");
        }

        feedInCountTable.invalidate();
        dischargeCountTable.invalidate();
        dischargeCountTable2.invalidate();
    }
}
